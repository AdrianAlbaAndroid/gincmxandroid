package mx.ginc

import androidx.multidex.MultiDexApplication
import mx.ginc.data.AppPreferenceManager

class BaseApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
    }

}