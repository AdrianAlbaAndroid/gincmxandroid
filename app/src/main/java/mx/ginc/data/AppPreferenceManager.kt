package mx.ginc.data

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class AppPreferenceManager(context : Context) {

    private var sharedPreferences: SharedPreferences? = null

    init {
        this.sharedPreferences = context.getSharedPreferences(AppUtil.Constants.PREFERENCE_SETTINGS, Context.MODE_PRIVATE)
    }

    fun saveToken(token : String) {
        sharedPreferences!!.edit().putString(AppUtil.Constants.ACCESS_TOKEN, token).commit()
    }

    fun getToken() : String? {
        val access = sharedPreferences!!.getString(AppUtil.Constants.ACCESS_TOKEN,"")
        return sharedPreferences!!.getString(AppUtil.Constants.ACCESS_TOKEN,"")
    }

    fun removeToken() {
         sharedPreferences!!.edit().remove(AppUtil.Constants.ACCESS_TOKEN).commit()
    }

    fun saveFullName(name : String) {
        sharedPreferences!!.edit().putString(AppUtil.Constants.FULL_NAME, name).commit()
    }

    fun getFullName() : String? {
        return sharedPreferences!!.getString(AppUtil.Constants.FULL_NAME,"")
    }

    fun removeFullName() {
        sharedPreferences!!.edit().remove(AppUtil.Constants.FULL_NAME).commit()
    }

    fun saveProfile(profile : String) {
        sharedPreferences!!.edit().putString(AppUtil.Constants.PROFILE, profile).commit()
    }

    fun getProfile(): String?   {
        return sharedPreferences!!.getString(AppUtil.Constants.PROFILE,"")
    }

    fun setProductCount(count : Int) {
        sharedPreferences!!.edit().putInt(AppUtil.Constants.COUNT_PRODUCT, count).commit()
    }

    fun getProductCount(): Int?   {
        return sharedPreferences!!.getInt(AppUtil.Constants.COUNT_PRODUCT,0)
    }

    fun getArrayList(key: String): ArrayList<String?>? {
        val gson = Gson()
        val json = sharedPreferences!!.getString(key, null)
        val type = object : TypeToken<ArrayList<String?>?>() {}.type
        return gson.fromJson(json, type)
    }

    fun saveArrayList(list: ArrayList<String?>?, key: String) {
        val editor = sharedPreferences!!.edit()
        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(key, json)
        editor.apply()
    }



}