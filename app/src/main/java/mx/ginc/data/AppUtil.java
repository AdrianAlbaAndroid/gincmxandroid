package mx.ginc.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.ginc.data.entities.Colonia;

public class AppUtil {
    private static AppPreferenceManager pref;
    public static class Constants {
        public static final String ACCESS_TOKEN = "access_token";
        public static final String PROFILE = "profile";
        public static final String COUNT_PRODUCT = "count_product";
        public static final String FULL_NAME = "name";
        public static final String ID_CUSTOMIZER = "idCustomizer";
        public static final String PAGELOAD = "pageLoad";
        public static final String PREFERENCE_SETTINGS = "PREFERENCE_SETTINGS";
        public static final int PRECIO_CAMISA = 3800;
        public static final int VERSION_DATA_BASE = 3;
        public static final int ID_MANGA_LARGA = 3;
        public static final int ID_MANGA_CORTA = 36;
        public static final String MANGA_LARGA = "Con mangas";
        public static final String MANGA_CORTA = "Sin mangas";
        public static final String TOKEN_USER = "Bearer ";
        public Constants() {
        }
    }

    public static class Urls {
        public static final String BASE_URL = "https://ginc.smart-develop.mx/";
        public static final String URL_COMO_FUNCIONA =  BASE_URL + "assets/apartados/comofunciona/Hola-mundo.pdf";
        public static final String VIDEO_COMO_MEDIRME = BASE_URL + "assets/apartados/comomedirme/video.mp4";
        public static final String URL_TERMINOS =  BASE_URL + "assets/apartados/terminosycondiciones/OPBTC-01.PDF";
        public static final String URL_GUIA_TALLAS =  BASE_URL + "assets/apartados/guias/tallas/guia-tallas.pdf";
        public static final String URL_IMAGES =  BASE_URL + "publico/catalogos/rest/tipo_customizacion_test/";

        public Urls() {
        }
    }

    public static String customFormat(String pattern, double value ) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        System.out.println(value + "  " + pattern + "  " + output);
        return output;
    }

    public static  String setIds(ArrayList<String> mCustomizerIds){
        String ids = "";
        for(String id :  mCustomizerIds){
            ids = ids + id + ",";
        }
        ids = ids.substring(0, ids.length()-1);
        return ids;
    }

    public static List<String> getListColonia(List<Colonia> estado){
        List<String> coloniaList = new ArrayList<>();
        for(Colonia col : estado){
            coloniaList.add(col.getNombreColonia());
        }
        return coloniaList;
    }

     public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }

    public static String convertDateTime(String sDate){
        Date date = null;
        SimpleDateFormat format = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(sDate);
            format = new SimpleDateFormat("dd 'de' MMMM yyyy - hh:mm:ss a");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format.format(date);
    }

    public static String convertDate(String sDate){
        Date date = null;
        SimpleDateFormat format = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(sDate);
            format = new SimpleDateFormat("dd 'de' MMMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format.format(date);
    }

    public static String Token(Context context){
        pref = new AppPreferenceManager(context);
        return AppUtil.Constants.TOKEN_USER  + pref.getToken();
    }

    public static class TipoProducto {
        public static final String CATALOGO = "CATALOGO";
        public static final String PERSONALIZADO = "PERSONALIZADO";

        public TipoProducto() {
        }
    }


}
