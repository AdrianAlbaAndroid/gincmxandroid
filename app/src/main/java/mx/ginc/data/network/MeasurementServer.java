package mx.ginc.data.network;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Belly;
import mx.ginc.data.entities.Medidas;
import mx.ginc.views.AddCartDesignActivity;
import mx.ginc.views.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MeasurementServer {

    private static MeasurementServer instance = null;
    private MutableLiveData<List<Belly>>  mMeasurementList = new MutableLiveData<>();
    private MutableLiveData<List<Belly>>  mBellyList = new MutableLiveData<>();
    private MutableLiveData<List<Belly>>  mChestList = new MutableLiveData<>();
    private MutableLiveData<List<Medidas>> mMisMedidasList = new MutableLiveData<>();
    private Context context;

    public static MeasurementServer getInstance(Context context) {
        if (instance == null)
            instance = new MeasurementServer(context);
        return instance;
    }

    public MeasurementServer(Context context){
        this.context = context;
    }

    private MeasurementServer.MeasurementCallBack measurementCallBack;
    private MeasurementServer.MedidasCallBack medidasCallBack;

    public interface MeasurementCallBack {
        void onSuccess(String message);
        void onFailure(String message);
    }

    public interface MedidasCallBack {
        void onSuccess(Medidas medida);
        void onEmpty();
        void onFailure(JSONObject jsonObject);
    }



    public LiveData<List<Belly>> getFitPreference()  {
        Call<JsonObject> client = RetrofitService.getInstance().getFitPreference();
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setFit(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        return mMeasurementList;
    }

    public LiveData<List<Belly>> getBellyPreference()  {
        Call<JsonObject> client = RetrofitService.getInstance().getBellyPreference();
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getBelly(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        return mBellyList;
    }

    public LiveData<List<Belly>> getChestPreference()  {
        Call<JsonObject> client = RetrofitService.getInstance().getChestPreference();
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getChest(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

        return mChestList;
    }

    public void deleteMedida(int id, MeasurementCallBack measurementCallBack)  {
        this.measurementCallBack = measurementCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().deleteMedida(AppUtil.Token(context), id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getMedidas(response.body());
                    }
                } else {
                    measurementCallBack.onFailure("Hubo un error al intentar borrar la medida");
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void addMedida(JsonObject jsonObject, MeasurementCallBack measurementCallBack)  {
        this.measurementCallBack = measurementCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().addMedida(AppUtil.Token(context),jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getMedidas(response.body());
                    }
                } else {
                    getBodyError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void actualizarMedidas(int id, JsonObject jsonObject, MeasurementCallBack measurementCallBack)  {
        this.measurementCallBack = measurementCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().actualizarMedida(AppUtil.Token(context),jsonObject, id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getMedidas(response.body());
                    }
                } else {
                    getBodyError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public void getMedida(MedidasCallBack medidasCallBack)  {
        this.medidasCallBack = medidasCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getMedida(AppUtil.Token(context));
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getMedida(response.body());
                    }
                } else {
                    getBodyMedidaError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
    }

    public LiveData<List<Medidas>> getMedidasAll()  {
        Call<JsonObject> client = RetrofitService.getInstance().getMisMedidas(AppUtil.Token(context));
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getMedidasAll(response.body());
                    }
                } else {
                    setLogin(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mMisMedidasList;
    }

    private void getBodyError(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            measurementCallBack.onFailure(jObjError.getString("mensaje"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getBodyMedidaError(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            medidasCallBack.onFailure(jObjError);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void setLogin(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            if(jObjError.getString("mensaje").equals(context.getString(R.string.token_requerido)) ||
                    jObjError.getString("mensaje").equals(context.getString(R.string.token_noexiste))
                    || jObjError.getString("mensaje").equals(context.getString(R.string.token_expirado))){
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);
            }else{
                createDialog("Alerta", jObjError.getString("mensaje"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    private void setFit(JsonObject object) {
        List<Belly> mStringList = new ArrayList();
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            for (int j = 0; j < arrayTitulo.size(); j++) {
                JsonObject json = arrayTitulo.get(j).getAsJsonObject();
                mStringList.add(new Belly(json.get("id").getAsInt(), json.get("etiqueta").getAsString(),""));
            }
            mMeasurementList.setValue(mStringList);
        }
    }


    private void getMedidas(JsonObject object) {
        if (object != null) {
            String mensaje = object.get("mensaje").getAsString();
            measurementCallBack.onSuccess(mensaje);
        }
    }

    private void getMedida(JsonObject object) {
        if (object != null) {
            JsonObject cuerpo = object.get("cuerpo").getAsJsonObject();
            String nombre = cuerpo.get("nombre").getAsString();
            if(nombre.equals("No tienes medidas agregadas")){
                medidasCallBack.onEmpty();
            }else{
                medidasCallBack.onSuccess(medidasObject(cuerpo));
            }
        }
    }

    private void getMedidasAll(JsonObject object) {
        List<Medidas> listado = new ArrayList<>();
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            for (int j = 0; j < arrayTitulo.size(); j++) {
                JsonObject objectMedida = arrayTitulo.get(j).getAsJsonObject();
                listado.add(medidasObject(objectMedida));
            }
        }
        mMisMedidasList.setValue(listado);
    }


    private void getBelly(JsonObject object) {
        List<Belly> mStringList = new ArrayList();
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            for (int j = 0; j < arrayTitulo.size(); j++) {
                JsonObject json = arrayTitulo.get(j).getAsJsonObject();
                mStringList.add(bellyObject(json));
            }
            mBellyList.setValue(mStringList);
        }
    }

    private void getChest(JsonObject object) {
        List<Belly> mStringList = new ArrayList();
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            for (int j = 0; j < arrayTitulo.size(); j++) {
                JsonObject json = arrayTitulo.get(j).getAsJsonObject();
                mStringList.add(bellyObject(json));
            }
            mChestList.setValue(mStringList);
        }
    }

    private Medidas medidasObject(JsonObject objectMedida){
        return new Medidas(objectMedida.get("id").getAsString(),"Mis Medidas",
                    objectMedida.get("altura").getAsString(),
                    objectMedida.get("peso").getAsString(),
                    objectMedida.get("tipoVientre").getAsJsonObject().get("etiqueta").getAsString(),
                    objectMedida.get("tipoPecho").getAsJsonObject().get("etiqueta").getAsString(),
                    objectMedida.get("edad").getAsString(),
                    (objectMedida.get("tipoCamisaMedida").isJsonNull() ?  null : objectMedida.get("tipoCamisaMedida").getAsJsonObject().get("etiqueta").getAsString()),
                    objectMedida.get("cuello").getAsString(),
                    objectMedida.get("pecho").getAsString(),
                    objectMedida.get("cintura").getAsString(),
                    objectMedida.get("cadera").getAsString(),
                    objectMedida.get("largoManga").getAsString(),
                    objectMedida.get("largoMuneca").getAsString(),
                    objectMedida.get("munecaIzquierda").getAsString(),
                    objectMedida.get("munecaDerecha").getAsString(),
                    objectMedida.get("biceps").getAsString(),
                    objectMedida.get("espalda").getAsString(),
                    objectMedida.get("aida").getAsString(),
                    objectMedida.get("largo").getAsString(),
                    objectMedida.get("principal").getAsBoolean());
    }

    private Belly bellyObject(JsonObject object){
        return new Belly(object.get("id").getAsInt(),
                object.get("etiqueta").getAsString(),
                object.get("imagen").getAsString());
    }
}
