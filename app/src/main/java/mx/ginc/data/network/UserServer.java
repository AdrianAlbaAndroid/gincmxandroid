package mx.ginc.data.network;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.User;
import mx.ginc.data.entities.UserDetail;
import mx.ginc.data.entities.UserRegister;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserServer {
    private static UserServer instance = null;
    private Context context;
    private List<Customizer> mCustomizerList = new ArrayList<>();

    public static UserServer getInstance(Context context) {
        if (instance == null)
            instance = new UserServer(context);
        return instance;
    }

    public UserServer(Context context) {
        this.context = context;
    }

    public interface UserCallBack {
        void onSuccess(User user);
        void onFailure(String mensaje);
    }

    public interface UserDetailCallBack {
        void onSuccess(UserDetail userDetail);
        void onFailure(JSONObject jsonObject);
    }

    public interface MessageCallBack {
        void onSuccess(String mensaje);
    }

    private UserServer.UserCallBack userCallBack;
    private UserServer.MessageCallBack messageCallBack;
    private UserServer.UserDetailCallBack userDetailCallBack;

    public void setUserLogin(JsonObject object, UserCallBack userCallBack)  {
        this.userCallBack = userCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setUser(object);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        System.out.println(response.body().toString());
                        setUser(response.body());
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String userMessage = jObjError.getString("mensaje");
                        userCallBack.onFailure(userMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void getUser(UserDetailCallBack userDetailCallBack)  {
        this.userDetailCallBack = userDetailCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getUser(AppUtil.Token(context));
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setUserComplete(response.body());
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        userDetailCallBack.onFailure(jObjError);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void setUserRegister(JsonObject jsonObject, UserCallBack userCallBack)  {
        this.userCallBack = userCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setUserRegister(jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setUser(response.body());
                    }
                } else {
                    getBodyError(response);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void getBodyError(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            JSONArray keys = jObjError.getJSONObject("cuerpo").names();
            StringBuilder str = new StringBuilder();
            for(int i=0;i<keys.length();i++) {
                String current_key = keys.get(i).toString();
                String nombre = jObjError.getJSONObject("cuerpo").getJSONArray(current_key).getString(0);
                str.append(nombre + "\n");
            }
            userCallBack.onFailure(str.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setMessageContact(JsonObject jsonObject, MessageCallBack messageCallBack)  {
        this.messageCallBack = messageCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setMessageContact(jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setMessage(response.body());
                    } else {
                    }
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String userMessage = jObjError.getString("mensaje");
                        messageCallBack.onSuccess(userMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void setUserProvider(JsonObject object, String provider, UserCallBack userCallBack)  {
        this.userCallBack = userCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setUserProveedor(object,provider);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setUser(response.body());
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String userMessage = jObjError.getString("mensaje");
                        userCallBack.onFailure(userMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void setUserForgotten(JsonObject object, UserCallBack userCallBack)  {
        this.userCallBack = userCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setUserForgotten(object);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setUser(response.body());
                    }
                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        String userMessage = jObjError.getString("mensaje");
                        userCallBack.onFailure(userMessage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void setUser(JsonObject object) {
        if (object != null) {
            JsonObject jsonObject = object.get("cuerpo").getAsJsonObject();
            userCallBack.onSuccess(setUserToken(jsonObject));
        }
    }

    private void setUserComplete(JsonObject object) {
        if (object != null) {
            JsonObject jsonObject = object.get("cuerpo").getAsJsonObject();
            userDetailCallBack.onSuccess(setUserDetail(jsonObject));
        }
    }

    private void setMessage(JsonObject object) {
        if (object != null) {
            String message = object.get("mensaje").getAsString();
            messageCallBack.onSuccess(message);
        }
    }

    public User setUserToken(JsonObject objectJson) {
        return new User(objectJson.get("access_token").getAsString(),
                objectJson.get("token_type").getAsString(),
                objectJson.get("user").getAsString(),
                objectJson.get("nombre").getAsString());
    }

    public UserRegister setUserRegister(JsonObject objectJson) {
        return new UserRegister(objectJson.get("nombre").getAsString(),
                (objectJson.get("email").isJsonNull() ?  null : objectJson.get("email").getAsString()),
                (objectJson.get("apellidoPaterno").isJsonNull() ?  null : objectJson.get("apellidoPaterno").getAsString()),
                (objectJson.get("apellidoMaterno").isJsonNull() ?  null : objectJson.get("apellidoMaterno").getAsString()),
                (objectJson.get("password").isJsonNull() ?  null : objectJson.get("password").getAsString()));
    }

    public UserDetail setUserDetail(JsonObject objectJson) {
        return new UserDetail(objectJson.get("usuario").getAsString(),
                objectJson.get("nombre").getAsString(),
                (objectJson.get("email").isJsonNull() ?  null : objectJson.get("email").getAsString()),
                (objectJson.get("apellidoPaterno").isJsonNull() ?  null : objectJson.get("apellidoPaterno").getAsString()),
                (objectJson.get("apellidoMaterno").isJsonNull() ?  null : objectJson.get("apellidoMaterno").getAsString()),
                (objectJson.get("fechaAlta").isJsonNull() ?  null : objectJson.get("fechaAlta").getAsString()));
    }
}
