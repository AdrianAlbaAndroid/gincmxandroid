package mx.ginc.data.network;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.R;
import mx.ginc.data.entities.Colonia;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.Especificaciones;
import mx.ginc.data.entities.FinalPrice;
import mx.ginc.data.entities.Product;
import mx.ginc.data.entities.ProductInventory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductServer {

    private static ProductServer instance = null;
    private  List<Customizer> mCustomizerList = new ArrayList<>();
    private String urlImage = "";

    public static ProductServer getInstance() {
        if (instance == null)
            instance = new ProductServer();
        return instance;
    }

    public ProductServer() {
    }

    public interface CustomizerCallBack {
        void onSuccess(List<Customizer> mListCustomizer);
    }

    public interface CustomizerMenuCallBack {
        void onSuccess(List<Customizer> mListCustomizer, String tipoManga);
    }

    public interface PriceCallBack {
        void onSuccess(FinalPrice finalPrice);
        void onFailure(Double price);
    }
    public interface ImageCallBack {
        void onUrl(String url);
    }

    public interface ImageCustomizerCallBack {
        void onSuccesCustomizer(String imageString);
    }


    private MutableLiveData<List<Customizer>> mCustomizerMutableList = new MutableLiveData<>();
    private MutableLiveData<List<Product>> mProductMutableList = new MutableLiveData<>();
    private MutableLiveData<List<String>> mImageList = new MutableLiveData<>();
    private MutableLiveData<List<Customizer>> mCategoriasList = new MutableLiveData<>();
    private MutableLiveData<List<Especificaciones>> mEspecificacionesList = new MutableLiveData<>();

    private CustomizerCallBack customizerCallBack;
    private CustomizerMenuCallBack customizerMenuCallBack;
    private PriceCallBack priceCallBack;
    private ImageCallBack imageCallBack;
    private ImageCustomizerCallBack imageCustomizerCallBack;

    public void getMenuItemFirst(int id, CustomizerCallBack customizerCallBack) {
        this.customizerCallBack = customizerCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getCustomizer(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        mCustomizerList = setCustomizerFirst(response.body(), false);
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void getCustomizerMenu(int id, String tipoManga, CustomizerMenuCallBack customizerMenuCallBack) {
        this.customizerMenuCallBack = customizerMenuCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getCustomizer(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        mCustomizerList = setTipoManga(response.body(), tipoManga);
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public LiveData<List<Customizer>> getCatalog(int id)  {
        Call<JsonObject> client = RetrofitService.getInstance().getCustomizer(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setCustomizerFirst(response.body(),true);
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mCustomizerMutableList;
    }

    public LiveData<List<Product>> getProductFilter(JsonObject jsonObject) {
        Call<JsonObject> client = RetrofitService.getInstance().getProductFilter(jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        System.out.println(response.body().toString());
                        setProduct(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mProductMutableList;
    }

    public LiveData<List<String>> getImagesCatalog(int id) {
        Call<JsonObject> client = RetrofitService.getInstance().getImageProduct(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setImageJson(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mImageList;
    }

    public void getFinalPrice(int id, PriceCallBack priceCallBack)  {
        this.priceCallBack = priceCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getFinalPrice(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setFinalPrice(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }




    private List<Customizer> setCustomizerFirst(JsonObject object, boolean catalog) {
        List<Customizer> mCustomizerList = new ArrayList();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {
                for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                    JsonArray arrayTitulo = object.getAsJsonArray("cuerpo").get(i).getAsJsonObject()
                            .getAsJsonArray("hijos");
                    for (int j = 0; j < arrayTitulo.size(); j++) {
                        JsonObject jsonObject = arrayTitulo.get(j).getAsJsonObject();
                        mCustomizerList.add(setCustomizer(j,jsonObject));
                    }

                }
                if(catalog){
                    mCustomizerMutableList.setValue(mCustomizerList);
                }else{
                    customizerCallBack.onSuccess(mCustomizerList);
                }

            }
        }
        return mCustomizerList;
    }

    private List<Customizer> setTipoManga(JsonObject object, String tipoManga) {
        List<Customizer> mCustomizerList = new ArrayList();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {
                for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                    JsonArray arrayTitulo = object.getAsJsonArray("cuerpo").get(i).getAsJsonObject()
                            .getAsJsonArray("hijos");
                    for (int j = 0; j < arrayTitulo.size(); j++) {
                        JsonObject jsonObject = arrayTitulo.get(j).getAsJsonObject();
                        mCustomizerList.add(setCustomizer(j,jsonObject));
                    }

                }
                customizerMenuCallBack.onSuccess(mCustomizerList, tipoManga);
            }
        }
        return mCustomizerList;
    }

    public void getImageCustomizer(String urlIds, ImageCustomizerCallBack imageCustomizerCallBack)  {
        this.imageCustomizerCallBack = imageCustomizerCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getImageCustomizer(urlIds);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setImageJsonObject(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage().toString());
            }
        });
    }

    public void setImage(int id, ImageCallBack imageCallBack)  {
        this.imageCallBack = imageCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getImageProduct(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setImageJson(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


    private List<Product> setProduct(JsonObject object) {
        List<Product> mProductList = new ArrayList();
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonObject("cuerpo").getAsJsonArray("data");
            if (arrayTitulo.size() != 0) {
                for (int i = 0; i < arrayTitulo.size(); i++) {
                    JsonObject jsonObject = arrayTitulo.get(i).getAsJsonObject();
                    mProductList.add(setProductDetail(jsonObject));
                }
                mProductMutableList.setValue(mProductList);
            }
        }
        return mProductList;
    }

    public LiveData<List<Customizer>> getCategorias()  {
        Call<JsonObject> client = RetrofitService.getInstance().getCateorias();
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setCategoriasObject(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mCategoriasList;
    }

    public LiveData<List<Especificaciones>> getEspecificaciones(int idProducto)  {
        Call<JsonObject> client = RetrofitService.getInstance().getEspecificaciones(idProducto);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setEspecificacion(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mEspecificacionesList;
    }


    private void setImageJsonObject(JsonObject object) {
        if (object != null) {
            String arrayTitulo = object.get("cuerpo").getAsString();
            imageCustomizerCallBack.onSuccesCustomizer(arrayTitulo);
        }
    }

    private void setImageJson(JsonObject object) {
        List<String> urlImage = new ArrayList();
        if (object != null) {
            for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                String url = String.valueOf(object.getAsJsonArray("cuerpo").get(i).getAsJsonObject().get("url"));
                url = url.replace("\"","");
                urlImage.add(url);
            }

        }
        mImageList.setValue(urlImage);
    }

    private void setEspecificacion(JsonObject object) {
        List<Especificaciones> especificacionesList = new ArrayList();
        if (object != null) {
            JsonArray arrayList = object.getAsJsonArray("cuerpo");
            for (int i = 0; i < arrayList.size(); i++) {
                int id = arrayList.get(i).getAsJsonObject().get("id").getAsInt();
                especificacionesList.add(setEspecifications(id,
                        arrayList.get(i).getAsJsonObject().get("tipoValor").getAsJsonObject(),
                        arrayList.get(i).getAsJsonObject()));
                }
            }
        mEspecificacionesList.setValue(especificacionesList);
    }

    private void setCategoriasObject(JsonObject object){
        List<Customizer> listado = new ArrayList();
        JsonArray arrayCamisas = null;
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            for (int j = 0; j < arrayTitulo.size(); j++) {
                JsonArray json = arrayTitulo.get(0).getAsJsonObject().getAsJsonArray("hijos");
                for (int l = 0; l < json.size(); l++) {
                    arrayCamisas = json.get(l).getAsJsonObject().getAsJsonArray("hijos");
                }
            }
            for (int t = 0; t < arrayCamisas.size(); t++) {
                listado.add(setCustomizerProduct(arrayCamisas.get(t).getAsJsonObject()));
            }
        }
        mCategoriasList.setValue(listado);
    }



    private void setFinalPrice(JsonObject object) {
        if (object != null) {
            JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
            if(arrayTitulo.size() != 0){
                for (int j = 0; j < arrayTitulo.size(); j++) {
                    JsonObject json = arrayTitulo.get(j).getAsJsonObject();
                    System.out.println(json);
                    priceCallBack.onSuccess(setPrices(json));
                }
            }else{
                priceCallBack.onFailure(0.0);
            }
        }
    }

    public Customizer setCustomizer(int index, JsonObject objectJson) {
        String icon = null;
        JsonObject json = null;
        json = (objectJson.get("iconDTO").isJsonNull() ?  null : objectJson.get("iconDTO").getAsJsonObject());
        if(json == null){
            icon= null;
        }else{
            icon = (objectJson.get("iconDTO").getAsJsonObject().get("url").isJsonNull() ?  null : objectJson.get("iconDTO").getAsJsonObject().get("url").getAsString());
        }
        return new Customizer(objectJson.get("id").getAsInt(),
                index,
                objectJson.get("etiqueta").getAsString(),
                objectJson.get("esTextura").getAsBoolean(),
                icon,
                R.drawable.ginc_logo,
                (objectJson.get("esUltimo").isJsonNull() ?  null : objectJson.get("esUltimo").getAsBoolean()),
                objectJson.get("nivel").getAsString(),
                false);
    }

    public Especificaciones setEspecifications(int id, JsonObject objectJson, JsonObject jsonObject) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return new Especificaciones(id,
                objectJson.get("valor").getAsString(),
                objectJson.get("tipoAtributo").getAsJsonObject().get("etiqueta").getAsString(),
                gson.toJson(jsonObject));
    }

    public Customizer setCustomizerProduct(JsonObject objectJson) {
        return new Customizer(objectJson.get("id").getAsInt(),
                0,
                objectJson.get("etiqueta").getAsString(),
                true,
               null,
                R.drawable.ginc_logo,
                null,
                null,
                false);
    }

    public Product setProductDetail(JsonObject objectJson) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return new Product(objectJson.get("id").getAsInt(),
                objectJson.get("etiqueta").getAsString(),
                objectJson.get("descripcion").getAsString(),
                objectJson.get("url").getAsString(),
                gson.toJson(objectJson));
    }

    public FinalPrice setPrices(JsonObject objectJson) {
        return new FinalPrice(objectJson.get("id").getAsInt(),
                (objectJson.get("costoBase").isJsonNull() ?  null : objectJson.get("costoBase").getAsDouble()),
                (objectJson.get("costoOfertaFinal").isJsonNull() ?  null :objectJson.get("costoOfertaFinal").getAsDouble()),
                (objectJson.get("costoPorcentajeFinal").isJsonNull() ?  null :objectJson.get("costoPorcentajeFinal").getAsDouble()),
                (objectJson.get("porcentajeFinal").isJsonNull() ?  null :objectJson.get("porcentajeFinal").getAsDouble()),
                (objectJson.get("costoFinal").isJsonNull() ?  null :objectJson.get("costoFinal").getAsDouble()));
    }
}
