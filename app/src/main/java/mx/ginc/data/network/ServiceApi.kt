package mx.ginc.data.network

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.*


interface ServiceApi {

    @GET("/publico/catalogos/rest/tipo_customizacion/ids/{id}/")
    fun getTipoCustomizacion(@Path("id") tipo_id: Int): Call<JsonObject>

    @POST("/publico/rest/productos/filtro")
    fun getProductFilter(@Body body : JsonObject): Call<JsonObject>

    @GET("/publico/rest/productos/precios_finales/{idProducto}/")
    fun getPreciosFinales(@Path("idProducto") tipo_id: Int): Call<JsonObject>

    @POST("/oauth/token")
    fun setUser(@Body body : JsonObject): Call<JsonObject>

    @GET("/publico/rest/colonias/{codigoPostal}")
    fun getColonia(@Path("codigoPostal") codigo_postal: String): Call<JsonObject>

    @POST("/publico/rest/direccion_entrega")
    fun setDirection(@Header("Authorization") token: String, @Body body : JsonObject): Call<JsonObject>

    @PUT("/publico/rest/direccion_entrega")
    fun editDirection(@Header("Authorization") token: String, @Body body : JsonObject): Call<JsonObject>

    @POST("/publico/rest/direccion_entrega/{id}")
    fun getDeleteById(@Header("Authorization") token: String, @Path("id") tipo_id: Int): Call<JsonObject>

    @GET("/publico/rest/direccion_entrega")
    fun getDirectionAll(@Header("Authorization") token: String): Call<JsonObject>

    @GET("/publico/rest/productos/imagen_producto/{idProducto}")
    fun getImageProduct(@Path("idProducto") tipo_id: Int): Call<JsonObject>

    @GET
    fun getImageCustomizer(@Url url: String?): Call<JsonObject?>?

    @POST("/publico/rest/solicitud/compra/checkout")
    fun setCheckout(@Header("Authorization") token: String, @Body body : JsonObject): Call<JsonObject>

    @GET
    fun getUrlPay(@Url url: String?): Call<JsonObject?>?

    @GET("/publico/me")
    fun getUser(@Header("Authorization") token: String) : Call<JsonObject?>?

    @POST("/publico/rest/contacto")
    fun setMessageContact(@Body body : JsonObject): Call<JsonObject>

    @POST("/publico/rest/solicitud/compra/filtro")
    fun getMisPedidos(@Header("Authorization") token: String, @Body body : JsonObject) : Call<JsonObject?>

    @GET("/publico/medidas/rest/tipocamisamedida")
    fun getFitPreference() : Call<JsonObject?>?

    @GET("/publico/medidas/rest/tipovientre")
    fun getBellyPreference() : Call<JsonObject?>?

    @GET("/publico/medidas/rest/tipopecho")
    fun getChestPreference() : Call<JsonObject?>?

    @GET("/publico/medidas/rest/mimedida")
    fun getMedida(@Header("Authorization") token: String) : Call<JsonObject?>?

    @POST("/publico/medidas/rest/mismedidas")
    fun addMedida(@Header("Authorization") token: String, @Body body : JsonObject) : Call<JsonObject?>?

    @GET("/publico/medidas/rest/mismedidas")
    fun getMisMedidas(@Header("Authorization") token: String) : Call<JsonObject?>

    @POST("/publico/medidas/rest/mismedidas/{id}")
    fun deleteMedida(@Header("Authorization") token: String, @Path("id") id: Int) : Call<JsonObject?>

    @PATCH("/publico/medidas/rest/mismedidas/{id}")
    fun updateMedida(@Header("Authorization") token: String, @Body body : JsonObject,  @Path("id") id: Int) : Call<JsonObject?>

    @POST("/publico/registro")
    fun setUserRegister(@Body body : JsonObject): Call<JsonObject>

    @GET("/publico/catalogos/rest/categorias/lista")
    fun getCategorias() : Call<JsonObject?>?

    @POST("/oauth/token/{provider}")
    fun setUserProveedor(@Body body : JsonObject, @Path("provider") provider: String) : Call<JsonObject?>?

    @POST("/oauth/forgotten/password")
    fun setUserForgotten(@Body body : JsonObject) : Call<JsonObject?>?

    @GET("/publico/rest/productos/especificaciones/{idProducto}")
    fun getEspecificaciones(@Path("idProducto") idProducto: Int) : Call<JsonObject?>?

}