package mx.ginc.data.network;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Colonia;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.views.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectionServer {

    private static DirectionServer instance = null;
    private Context context;

    public static DirectionServer getInstance(Context context) {
        if (instance == null)
            instance = new DirectionServer(context);
        return instance;
    }

    public DirectionServer(Context context) {
        this.context = context;
    }

    public MutableLiveData<List<DirectionAll>> mDirectionList = new MutableLiveData<>();
    private ColoniaCallBack coloniaCallBack;
    private DirectionCallBack directionCallBack;
    private DirectionAllCallBack directionAllCallBack;
    private DirectionDeleteCallBack directionDeleteCallBack;

    public interface ColoniaCallBack {
        void onSuccess(List<Colonia> estado);
        void onFailure(String message);
    }

    public interface DirectionCallBack {
        void onSuccess(String message);
        void onFailure(String message);
    }

    public interface DirectionAllCallBack {
        void onSuccess( List<DirectionAll> listaDireccion);
    }

    public interface DirectionDeleteCallBack {
        void isDelete(Boolean isDelete, String message);
    }

    public void getColonia(String id, DirectionServer.ColoniaCallBack coloniaCallBack) {
        this.coloniaCallBack = coloniaCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getColonia(id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getColonia(response.body());
                    }
                } else {
                    System.out.println(response.errorBody().toString());
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void getDeleteById(int id, DirectionServer.DirectionDeleteCallBack directionDeleteCallBack) {
        this.directionDeleteCallBack = directionDeleteCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getDeleteById(AppUtil.Token(context),id);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getDeleteDirection(response.body());
                    }
                } else {
                    if(response.code() == 401) {
                        abrirLogin(context);
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public LiveData<List<DirectionAll>> getDirectionAll() {
        Call<JsonObject> client = RetrofitService.getInstance().getDirectionAll(AppUtil.Token(context));
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        System.out.println(response.body().toString());
                        getDirectionAll(response.body());
                    }
                } else {
                    if(response.code() == 401) {
                        abrirLogin(context);
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
        return mDirectionList;
    }

    public void getDirectionFirst(DirectionServer.DirectionAllCallBack directionCallBack) {
        this.directionAllCallBack = directionCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getDirectionAll(AppUtil.Token(context));
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getDirectionFirst(response.body());
                    }
                } else {
                    if(response.code() == 401) {
                        abrirLogin(context);
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void setDirection(JsonObject jsonObject, DirectionServer.DirectionCallBack directionCallBack) {
        this.directionCallBack = directionCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().setDirection(AppUtil.Token(context), jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setDirectionValidate(response.body());
                    }
                } else {
                    if(response.code() == 401) {
                        abrirLogin(context);
                    }else{
                        getBodyError(response);
                       // directionCallBack.onSuccess("Favor de validar la información ingresada");
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void editDirection(JsonObject jsonObject, DirectionServer.DirectionCallBack directionCallBack) {
        this.directionCallBack = directionCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().editDirection(AppUtil.Token(context), jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setDirectionValidate(response.body());
                    }
                } else {
                    if(response.code() == 401) {
                        abrirLogin(context);
                    }else{
                        getBodyError(response);
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }


    private void getColonia(JsonObject object) {
        List<Colonia> listColonia = new ArrayList<>();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {
                JsonArray arrayTitulo = object.getAsJsonArray("cuerpo");
                if(arrayTitulo.size() != 0){
                    for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                        JsonObject jsonObject = arrayTitulo.get(i).getAsJsonObject();
                        String colonia = jsonObject.get("etiqueta").getAsString();
                        String codigoPostal = jsonObject.get("codigoPostal").getAsString();
                        String municipio = jsonObject.get("tipoMunicipio").getAsJsonObject().get("etiqueta").getAsString();
                        String entidad = jsonObject.get("tipoMunicipio").getAsJsonObject().get("tipoEntidad").getAsJsonObject().get("etiqueta").getAsString();
                        listColonia.add(new Colonia(codigoPostal,colonia,municipio,entidad, jsonObject));
                    }
                    coloniaCallBack.onSuccess(listColonia);
                }
            }
            else{
                coloniaCallBack.onFailure("No se obtuvieron datos");
            }
        }
    }

    private void getDeleteDirection(JsonObject object) {
        if (object != null) {
            Boolean cuerpo = object.get("cuerpo").getAsBoolean();
            String mensaje = object.get("mensaje").getAsString();
            directionDeleteCallBack.isDelete(cuerpo,mensaje);
        }
    }

    private void getDirectionAll(JsonObject object) {
        List<DirectionAll> mDirectionAllList = new ArrayList();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {
                for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                    JsonObject jsonObjectBody = object.getAsJsonArray("cuerpo").get(i).getAsJsonObject();
                    mDirectionAllList.add(setDirection(jsonObjectBody));
                }
            }
        }
        mDirectionList.setValue(mDirectionAllList);
    }

    private void getDirectionFirst(JsonObject object) {
        List<DirectionAll> mDirectionAllList = new ArrayList();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {
                for (int i = 0; i < object.getAsJsonArray("cuerpo").size(); i++) {
                    JsonObject jsonObjectBody = object.getAsJsonArray("cuerpo").get(i).getAsJsonObject();
                    mDirectionAllList.add(setDirection(jsonObjectBody));
                }
            }
        }
        directionAllCallBack.onSuccess(mDirectionAllList);
    }

    private void getBodyError(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            JSONArray keys = jObjError.getJSONObject("cuerpo").names();
            StringBuilder str = new StringBuilder();
            for(int i=0;i<keys.length();i++) {
                String current_key = keys.get(i).toString();
                String nombre = jObjError.getJSONObject("cuerpo").getJSONArray(current_key).getString(0);
                str.append(nombre + "\n");
            }
            directionCallBack.onFailure(str.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getDirectionError(JsonObject object) {
        List<DirectionAll> mDirectionAllList = new ArrayList();
        if (object != null) {
            if (object.getAsJsonArray("cuerpo").size() != 0) {

            }
        }
        mDirectionList.setValue(mDirectionAllList);
    }

    private void setDirectionValidate(JsonObject object) {
        if (object != null) {
            String message = object.get("mensaje").getAsString();
            directionCallBack.onSuccess(message);
        }
    }

    public void abrirLogin(Context context){
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("activity","Direction");
        context.startActivity(intent);
    }

    private DirectionAll setDirection(JsonObject object) {
        Gson gson = new GsonBuilder().serializeNulls().create();
       return new DirectionAll(object.get("id").getAsInt(),
               object.get("quienRecibe").getAsString(),
               object.get("telefono").getAsString(),
               object.get("tipoColonia").getAsJsonObject().get("etiqueta").getAsString(),
               object.get("tipoColonia").getAsJsonObject().get("codigoPostal").getAsString(),
               object.get("tipoColonia").getAsJsonObject().get("tipoMunicipio").getAsJsonObject().get("etiqueta").getAsString(),
               object.get("tipoColonia").getAsJsonObject().get("tipoMunicipio").getAsJsonObject().get("tipoEntidad").getAsJsonObject().get("etiqueta").getAsString(),
               object.get("callePrincipal").getAsString(),
               object.get("calle1").getAsString(),
               object.get("calle2").getAsString(),
               object.get("numeroExterior").getAsString(),
               object.get("numeroInteriorDepto").getAsString(),
               object.get("indicacionesAdicionales").getAsString(),
               object.get("principal").getAsBoolean(),
               gson.toJson(object));
    }



}
