package mx.ginc.data.network;

import android.content.Context;
import android.content.Intent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.ParentPedido;
import mx.ginc.data.entities.Pedido;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.views.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseServer {
    private static PurchaseServer instance = null;
    private Context context;
    private MutableLiveData<List<ParentPedido>> mPedidosList = new MutableLiveData<>();
    private String TOKEN_USER;
    private AppPreferenceManager pref;

    public static PurchaseServer getInstance(Context context) {
        if (instance == null)
            instance = new PurchaseServer(context);
        return instance;
    }

    public PurchaseServer(Context context) {
        this.context = context;
        pref = new AppPreferenceManager(this.context);
        TOKEN_USER = AppUtil.Constants.TOKEN_USER  + pref.getToken();
    }

    public interface PurchaseCallBack {
        void onSuccess(String paymentUrl, String invoiceNumber);
        void onFailure(JSONObject jsonObject);
    }

    public interface PayCallBack {
        void onSuccess(String mensaje);
    }

    private PurchaseServer.PurchaseCallBack purchaseCallBack;
    private PurchaseServer.PayCallBack payCallBack;

    public void setCheckout(JsonObject jsonObject, PurchaseServer.PurchaseCallBack purchaseCallBack) {
        this.purchaseCallBack = purchaseCallBack;
        Gson gson = new GsonBuilder().serializeNulls().create();
        Call<JsonObject> client = RetrofitService.getInstance().setCheckout(AppUtil.Token(context),jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setCheckoutBody(response.body());
                    }
                } else {
                    if(response.code() == 500){
                        DialogAlerta.createDialogOK("Alerta", context.getString(R.string.error_servidor),context);
                    }else{
                        getBodyError(response);
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public void setUrlPayment(String url, PurchaseServer.PayCallBack payCallBack) {
        this.payCallBack = payCallBack;
        Call<JsonObject> client = RetrofitService.getInstance().getViewPayPal(url);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        getPayMessage(response.body());
                    }
                } else {
                    if(response.code() == 500){
                        DialogAlerta.createDialogOK("Alerta", context.getString(R.string.error_servidor), context);
                    }else{
                        getBodyError(response);
                    }

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    public LiveData<List<ParentPedido>> getMisPedidos(JsonObject jsonObject)
    {
        Call<JsonObject> client = RetrofitService.getInstance().getMisPedidos(AppUtil.Token(context),jsonObject);
        client.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        setMisPedidos(response.body());
                    }
                } else {
                    if(response.code() == 500){
                        DialogAlerta.createDialogOK("Alerta", context.getString(R.string.error_servidor),context);
                    }else{
                        getBodyErroPedidos(response);
                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                System.out.println(t.getMessage());
            }
        });
        return mPedidosList;
    }

    private void setMisPedidos(JsonObject object) {
        List<ParentPedido> mPedidoParentList = new ArrayList<>();
        List<Pedido> mPedidoList = new ArrayList<>();
        List<String> mNoPedidoList = new ArrayList<>();
        if (object != null) {
            JsonObject objectCuerpo = object.getAsJsonObject("cuerpo");
            JsonArray arrayTitulo = objectCuerpo.getAsJsonArray("data");
            for (int i = 0; i < arrayTitulo.size(); i++) {

                JsonObject jsonObject = arrayTitulo.get(i).getAsJsonObject();
                String invoceNumber = jsonObject.get("invoiceNumber").getAsString();
                JsonArray jsonArray = jsonObject.get("itemsDeCompraCatalogo").getAsJsonArray();
                if(jsonArray.size() != 0){
                    for (int j = 0; j < jsonArray.size(); j++) {
                        String cadenaAtributos = "";
                        String etiqueta = jsonArray.get(j).getAsJsonObject().get("producto").getAsJsonObject().get("etiqueta").getAsString();
                        JsonObject jsonProducto = jsonArray.get(j).getAsJsonObject().get("producto").getAsJsonObject();
                        String fechaAlta = jsonArray.get(j).getAsJsonObject().get("fechaAlta").getAsString();
                        int costo = jsonProducto.get("inventarioProducto").getAsJsonObject().get("costo").getAsInt();
                        String urlProducto = jsonArray.get(j).getAsJsonObject().get("producto").getAsJsonObject().get("url").getAsString();
                        int cantidad = jsonArray.get(j).getAsJsonObject().get("cantidad").getAsInt();
                        JsonArray especificaciones = jsonArray.get(j).getAsJsonObject().get("especificaciones").getAsJsonArray();
                        for (int t = 0; t < especificaciones.size(); t++) {
                            cadenaAtributos = cadenaAtributos + "\n" + especificaciones.get(t).getAsJsonObject().get("tipoAtributo").getAsString() + " : " + especificaciones.get(t).getAsJsonObject().get("tipoValor").getAsString();
                        }
                        mPedidoList.add(setPedido(invoceNumber,etiqueta, urlProducto,fechaAlta, costo, cantidad,AppUtil.TipoProducto.CATALOGO, cadenaAtributos));
                        mNoPedidoList.add(invoceNumber);
                    }
                }
                JsonArray jsonArrayPersonaliza = jsonObject.get("itemsDeCompraPersonaliza").getAsJsonArray();
                if(jsonArrayPersonaliza.size() != 0){
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int j = 0; j < jsonArrayPersonaliza.size(); j++) {
                        String customizaciones = "";
                        String imagenUrl = jsonArrayPersonaliza.get(j).getAsJsonObject().get("imagenUrl").getAsString();
                        String fechaAlta = jsonArrayPersonaliza.get(j).getAsJsonObject().get("fechaAlta").getAsString();
                        int cantidad = jsonArrayPersonaliza.get(j).getAsJsonObject().get("cantidad").getAsInt();
                        JsonArray especificaciones = jsonArrayPersonaliza.get(j).getAsJsonObject().get("customizaciones").getAsJsonArray();
                        for (int t = 0; t < especificaciones.size(); t++) {
                            customizaciones = customizaciones + "\n" + especificaciones.get(t).getAsJsonObject().get("tipoCustomizacionString").getAsString();
                        }
                        mPedidoList.add(setPedido(invoceNumber,"Camisa Customizada", imagenUrl,fechaAlta, AppUtil.Constants.PRECIO_CAMISA, cantidad, AppUtil.TipoProducto.PERSONALIZADO, customizaciones));
                        mNoPedidoList.add(invoceNumber);
                    }

                }
            }
            List<String> listaNoPedido = filtrarNoPedido(mNoPedidoList);
            for (int n = 0; n < listaNoPedido.size(); n++) {
                List<Pedido> listaPedido = filtrarMisPedidos(mPedidoList,listaNoPedido.get(n));
                mPedidoParentList.add(setParentPedido(listaNoPedido.get(n), listaPedido.get(0).getFechaCompra(), listaPedido));
            }
            mPedidosList.setValue(mPedidoParentList);
        }
    }

    private List<String> filtrarNoPedido(List<String> mNoPedidoList){
        return  mNoPedidoList.stream()
                .distinct()
                .collect(Collectors.toList());
    }

    private List<Pedido> filtrarMisPedidos(List<Pedido> misPedidos, String invoceNumber) {
        return  misPedidos.stream()
                .filter(c -> c.getInvoiceNumber().equals(invoceNumber))
                .collect(Collectors.toList());
    }

    public Pedido setPedido(String invoceNumber, String etiqueta, String urlProducto, String fechaAlta, int costo, int cantidad, String tipo, String especificaciones) {
        return new Pedido(invoceNumber,
                etiqueta,
                urlProducto,
                costo,
                cantidad,
                fechaAlta,
                tipo,
                especificaciones);
    }

    private ParentPedido setParentPedido(String invoceNumber, String fechaCompra, List<Pedido> listPedido){
        return new ParentPedido(invoceNumber, fechaCompra,
                listPedido);
    }

    private void getPayMessage(JsonObject object) {
        if (object != null) {
            String mensaje = object.get("mensaje").toString().replace("\"","");
            payCallBack.onSuccess(mensaje);
        }
    }

    private void setCheckoutBody(JsonObject object) {
        if (object != null) {
            JsonObject objectCuerpo = object.getAsJsonObject("cuerpo");
            String paymentUrl = objectCuerpo.get("urlPayment").getAsString();
            String invoiceNumber = objectCuerpo.get("invoiceNumber").getAsString();
            purchaseCallBack.onSuccess(paymentUrl,invoiceNumber);
        }
    }

    private void getBodyError(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            purchaseCallBack.onFailure(jObjError);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void getBodyErroPedidos(Response<JsonObject> response){
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            if (jObjError.getInt("http") == 401) {
                Intent intent = new Intent(context,  LoginActivity.class);
                context.startActivity(intent);
            } else {
                DialogAlerta.createDialogOK("Alerta", jObjError.getString("mensaje"), context);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
