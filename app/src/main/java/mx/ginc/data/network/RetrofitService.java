package mx.ginc.data.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mx.ginc.data.AppUtil;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    private Retrofit retrofit;

    private static RetrofitService instance = null;

    public static RetrofitService getInstance() {
        if (instance == null)
            instance = new RetrofitService();

        return instance;
    }

    public RetrofitService() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client =
                new OkHttpClient.Builder().
                        addInterceptor(interceptor)
                        .connectTimeout(1, TimeUnit.MINUTES)
                        .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(AppUtil.Urls.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

    }

    public Call<JsonObject> getCustomizer(int id) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getTipoCustomizacion(id);
    }

    public Call<JsonObject> getProductFilter(JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getProductFilter(jsonObject);
    }

    public Call<JsonObject> getFinalPrice(int id) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getPreciosFinales(id);
    }

    public Call<JsonObject> setUser(JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setUser(jsonObject);
    }

    public Call<JsonObject> getUser(String token) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getUser(token);
    }

    public Call<JsonObject> getColonia(String codigo_postal) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getColonia(codigo_postal);
    }

    public Call<JsonObject> setDirection(String token, JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setDirection(token,jsonObject);
    }

    public Call<JsonObject> editDirection(String token, JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.editDirection(token,jsonObject);
    }

    public Call<JsonObject> getDirectionAll(String token) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getDirectionAll(token);
    }

    public Call<JsonObject> getDeleteById(String token, int id) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getDeleteById(token, id);
    }

    public Call<JsonObject> getImageProduct(int id) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getImageProduct(id);
    }

    public Call<JsonObject> setCheckout(String token, JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setCheckout(token, jsonObject);
    }

    public Call<JsonObject> getViewPayPal(String paymentUrl) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getUrlPay(paymentUrl);
    }

    public Call<JsonObject> getImageCustomizer(String urlIds) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getImageCustomizer(urlIds);
    }

    public Call<JsonObject> setMessageContact(JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setMessageContact(jsonObject);
    }

    public Call<JsonObject> getMisPedidos(String token,JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getMisPedidos(token,jsonObject);
    }

    public Call<JsonObject> getFitPreference() {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getFitPreference();
    }

    public Call<JsonObject> getBellyPreference() {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getBellyPreference();
    }

    public Call<JsonObject> getChestPreference() {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getChestPreference();
    }

    public Call<JsonObject> addMedida(String token,JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.addMedida(token, jsonObject);
    }

    public Call<JsonObject> getMisMedidas(String token) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getMisMedidas(token);
    }

    public Call<JsonObject> deleteMedida(String token, int id) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.deleteMedida(token, id);
    }

    public Call<JsonObject> setUserRegister(JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setUserRegister(jsonObject);
    }

    public Call<JsonObject> getCateorias() {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getCategorias();
    }

    public Call<JsonObject> setUserProveedor(JsonObject jsonObject, String proveedor) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setUserProveedor(jsonObject, proveedor);
    }

    public Call<JsonObject> setUserForgotten(JsonObject jsonObject) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.setUserForgotten(jsonObject);
    }

    public Call<JsonObject> getEspecificaciones(int idProducto) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getEspecificaciones(idProducto);
    }

    public Call<JsonObject> actualizarMedida(String token, JsonObject jsonObject, int idProducto) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.updateMedida(token, jsonObject, idProducto);
    }

    public Call<JsonObject> getMedida(String token) {
        ServiceApi api = retrofit.create(ServiceApi.class);
        return api.getMedida(token);
    }
}
