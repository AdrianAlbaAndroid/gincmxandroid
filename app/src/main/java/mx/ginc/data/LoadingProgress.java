package mx.ginc.data;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import mx.ginc.R;

public class LoadingProgress {
    Activity mActivity;
    Context mContext;
    AlertDialog dialog;

    public LoadingProgress(Activity activity, Context context){
        mActivity = activity;
        mContext = context;
    }

    public void showDialog(String texto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = mActivity.getLayoutInflater();
        View view = inflater.inflate(R.layout.progress_layout, null);
        builder.setView(view);
        TextView loading = (TextView) view.findViewById(R.id.Loading);
        loading.setText(texto);
        dialog = builder.create();
        dialog.show();
    }

    public void dimissbar(){
        dialog.hide();
    }
}
