package mx.ginc.data.entities

data class FinalPrice(
        val id : Int,
        val costoBase : Double? = null,
        val costoOfertaFinal : Double? = null,
        val costoPorcentajeFinal : Double? = null,
        val porcentajeFinal : Double? = null,
        val costoFinal : Double ? = null
)
