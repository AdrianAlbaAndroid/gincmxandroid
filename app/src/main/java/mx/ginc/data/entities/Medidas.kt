package mx.ginc.data.entities

data class Medidas (
        val id : String,
        val nombre : String? = null,
        val altura : String? = null,
        val peso : String? = null,
        val tipoVientre : String? = null,
        val tipoPecho : String ? = null,
        val edad : String ? = null,
        val tipoCamisaMedida : String ? = null,
        val cuello : String ? = null,
        val pecho : String ? = null,
        val cintura : String ? = null,
        val cadera : String ? = null,
        val largoManga : String ? = null,
        val largoMuneca : String ? = null,
        val munecaIzquierda : String ? = null,
        val munecaDerecha : String ? = null,
        val biceps : String ? = null,
        val espalda : String ? = null,
        val aida : String ? = null,
        val largo : String ? = null,
        val principal : Boolean ? = null
){}