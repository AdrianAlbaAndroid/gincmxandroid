package mx.ginc.data.entities

import com.google.gson.JsonObject

data class DirectionEntry (val id: Int, val quienRecibe: String, val telefono: String, val tipoColonia: JsonObject, val callePrincipal: String,
                           val calle1: String,val calle2: String,val numeroExterior: String,val numeroInteriorDepto: String,
                           val indicacionesAdicionales: String, val principal: Boolean){
}