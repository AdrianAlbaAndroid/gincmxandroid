package mx.ginc.data.entities

data class Customizer(val id: Int, val index: Int, val etiqueta: String, val esTextura: Boolean, val urlIcon: String ? = null, val image: Int, val ultimo: Boolean? = null, val nivel: String? = null, val selected: Boolean)