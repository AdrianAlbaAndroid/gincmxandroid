package mx.ginc.data.entities

import com.google.gson.JsonObject

data class Colonia (val codigoPostal: String, val nombreColonia: String, val municipio: String, val entidad: String, val jsonObject: JsonObject){
}