package mx.ginc.data.entities;

public class Item {
    private int id;
    private String name;
    private int image;

    public Item(int id, String name, int image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

    public Item(int image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
