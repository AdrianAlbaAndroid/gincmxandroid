package mx.ginc.data.entities;

public class Menu {
    private String image;
    private String name;
    private int fondo;
    private int color;

    public Menu(String image, String name, int fondo, int color) {
        this.image = image;
        this.name = name;
        this.fondo = fondo;
        this.color = color;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFondo() {
        return fondo;
    }

    public void setFondo(int fondo) {
        this.fondo = fondo;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
