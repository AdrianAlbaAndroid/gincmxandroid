package mx.ginc.data.entities

import com.google.gson.JsonArray
import com.google.gson.JsonObject

data class Checkout (var direccionEntrega : JsonObject, var itemsDeCarritoCatalogo : JsonArray, var itemsDeCarritoPersonaliza : JsonArray? = null, var cantidadTotalDeItems : Int){
}