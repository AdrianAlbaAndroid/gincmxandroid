package mx.ginc.data.entities;

import java.io.Serializable;

public class Direccion implements Serializable {
    private int id;
    private String quienRecibe;
    private String telefono;
    private String colonia;
    private String codigoPostal;
    private String municipio;
    private String entidad;
    private String callePrincipal;
    private String calle1;
    private String calle2;
    private String numeroExterior;
    private String numeroInteriorDepto;
    private String indicacionesAdicionales;
    private Boolean principal;
    private String jsonObject;

    public Direccion(int id, String quienRecibe, String telefono, String colonia, String codigoPostal, String municipio, String entidad, String callePrincipal, String calle1, String calle2, String numeroExterior, String numeroInteriorDepto, String indicacionesAdicionales, Boolean principal, String jsonObject) {
        this.id = id;
        this.quienRecibe = quienRecibe;
        this.telefono = telefono;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
        this.municipio = municipio;
        this.entidad = entidad;
        this.callePrincipal = callePrincipal;
        this.calle1 = calle1;
        this.calle2 = calle2;
        this.numeroExterior = numeroExterior;
        this.numeroInteriorDepto = numeroInteriorDepto;
        this.indicacionesAdicionales = indicacionesAdicionales;
        this.principal = principal;
        this.jsonObject = jsonObject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuienRecibe() {
        return quienRecibe;
    }

    public void setQuienRecibe(String quienRecibe) {
        this.quienRecibe = quienRecibe;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getCallePrincipal() {
        return callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCalle1() {
        return calle1;
    }

    public void setCalle1(String calle1) {
        this.calle1 = calle1;
    }

    public String getCalle2() {
        return calle2;
    }

    public void setCalle2(String calle2) {
        this.calle2 = calle2;
    }

    public String getNumeroExterior() {
        return numeroExterior;
    }

    public void setNumeroExterior(String numeroExterior) {
        this.numeroExterior = numeroExterior;
    }

    public String getNumeroInteriorDepto() {
        return numeroInteriorDepto;
    }

    public void setNumeroInteriorDepto(String numeroInteriorDepto) {
        this.numeroInteriorDepto = numeroInteriorDepto;
    }

    public String getIndicacionesAdicionales() {
        return indicacionesAdicionales;
    }

    public void setIndicacionesAdicionales(String indicacionesAdicionales) {
        this.indicacionesAdicionales = indicacionesAdicionales;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    public String getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(String jsonObject) {
        this.jsonObject = jsonObject;
    }
}
