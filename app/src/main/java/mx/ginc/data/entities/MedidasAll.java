package mx.ginc.data.entities;

import java.io.Serializable;

public class MedidasAll  implements Serializable {
    private String id;
    private String nombre;
    private String altura;
    private String peso;
    private String tipoVientre;
    private String tipoPecho;
    private String edad;
    private String tipoCamisaMedida;
    private String cuello;
    private String pecho;
    private String cintura;
    private String cadera;
    private String largoManga;
    private String largoMuneca;
    private String munecaIzquierda;
    private String munecaDerecha;
    private String biceps;
    private String espalda;
    private String aida;
    private String largo;
    private Boolean principal;

    public MedidasAll(String id, String nombre, String altura, String peso, String tipoVientre, String tipoPecho, String edad, String tipoCamisaMedida, String cuello, String pecho, String cintura, String cadera, String largoManga, String largoMuneca, String munecaIzquierda, String munecaDerecha, String biceps, String espalda, String aida, String largo, Boolean principal) {
        this.id = id;
        this.nombre = nombre;
        this.altura = altura;
        this.peso = peso;
        this.tipoVientre = tipoVientre;
        this.tipoPecho = tipoPecho;
        this.edad = edad;
        this.tipoCamisaMedida = tipoCamisaMedida;
        this.cuello = cuello;
        this.pecho = pecho;
        this.cintura = cintura;
        this.cadera = cadera;
        this.largoManga = largoManga;
        this.largoMuneca = largoMuneca;
        this.munecaIzquierda = munecaIzquierda;
        this.munecaDerecha = munecaDerecha;
        this.biceps = biceps;
        this.espalda = espalda;
        this.aida = aida;
        this.largo = largo;
        this.principal = principal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getTipoVientre() {
        return tipoVientre;
    }

    public void setTipoVientre(String tipoVientre) {
        this.tipoVientre = tipoVientre;
    }

    public String getTipoPecho() {
        return tipoPecho;
    }

    public void setTipoPecho(String tipoPecho) {
        this.tipoPecho = tipoPecho;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getTipoCamisaMedida() {
        return tipoCamisaMedida;
    }

    public void setTipoCamisaMedida(String tipoCamisaMedida) {
        this.tipoCamisaMedida = tipoCamisaMedida;
    }

    public String getCuello() {
        return cuello;
    }

    public void setCuello(String cuello) {
        this.cuello = cuello;
    }

    public String getPecho() {
        return pecho;
    }

    public void setPecho(String pecho) {
        this.pecho = pecho;
    }

    public String getCintura() {
        return cintura;
    }

    public void setCintura(String cintura) {
        this.cintura = cintura;
    }

    public String getCadera() {
        return cadera;
    }

    public void setCadera(String cadera) {
        this.cadera = cadera;
    }

    public String getLargoManga() {
        return largoManga;
    }

    public void setLargoManga(String largoManga) {
        this.largoManga = largoManga;
    }

    public String getLargoMuneca() {
        return largoMuneca;
    }

    public void setLargoMuneca(String largoMuneca) {
        this.largoMuneca = largoMuneca;
    }

    public String getBiceps() {
        return biceps;
    }

    public void setBiceps(String biceps) {
        this.biceps = biceps;
    }

    public String getEspalda() {
        return espalda;
    }

    public void setEspalda(String espalda) {
        this.espalda = espalda;
    }

    public String getAida() {
        return aida;
    }

    public void setAida(String aida) {
        this.aida = aida;
    }

    public String getLargo() {
        return largo;
    }

    public void setLargo(String largo) {
        this.largo = largo;
    }

    public Boolean getPrincipal() {
        return principal;
    }

    public void setPrincipal(Boolean principal) {
        this.principal = principal;
    }

    public String getMunecaIzquierda() {
        return munecaIzquierda;
    }

    public void setMunecaIzquierda(String munecaIzquierda) {
        this.munecaIzquierda = munecaIzquierda;
    }

    public String getMunecaDerecha() {
        return munecaDerecha;
    }

    public void setMunecaDerecha(String munecaDerecha) {
        this.munecaDerecha = munecaDerecha;
    }
}
