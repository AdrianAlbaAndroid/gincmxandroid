package mx.ginc.data.entities

data class User( val access_token : String,
                 val token_type : String,
                 val user : String,
                 val nombre : String)
