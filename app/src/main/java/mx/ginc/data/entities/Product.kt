package mx.ginc.data.entities

import com.google.gson.JsonObject

data class Product(
        val id : Int,
        val etiqueta: String,
        val descripcion : String,
        val url : String,
        val product : String
)

data class Dimension(
        val alto: Int,
        val ancho: Int,
        val largo: Int,
)

data class ProductInventory(
        val costo : Int,
        val cantidad : Int,
        val requiereEnvio: Boolean
)