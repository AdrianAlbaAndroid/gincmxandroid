package mx.ginc.data.entities




data class DirectionAll (val id: Int, val quienRecibe: String, val telefono: String, val colonia: String, val codigoPostal: String, val municipio : String,
                         val entidad : String, val callePrincipal: String, val calle1: String, val calle2: String, val numeroExterior: String,
                         val numeroInteriorDepto: String, val indicacionesAdicionales: String, val principal: Boolean, val jsonObject: String){
}