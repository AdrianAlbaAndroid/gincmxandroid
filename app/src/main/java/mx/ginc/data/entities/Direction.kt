package mx.ginc.data.entities

data class Direction (val nombre: String, val direccion: String, val calleRferencia: String, val telefono: String) {
}