package mx.ginc.data.entities;

public class Cart {
    private int id;
    private String name;
    private String price;
    private int image;
    private String size;


    public Cart(int id, String name, String price, int image, String size) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
