package mx.ginc.data.entities

data class Pedido (val invoiceNumber: String, val etiqueta: String, val image: String, val costo: Int, val cantidad: Int, val fechaCompra: String, val tipo:String, val especificaciones:String){
}