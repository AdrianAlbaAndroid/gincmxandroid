package mx.ginc.data.entities

data class ParentPedido (
    val section : String,
    val fechaCompra: String,
    val list : List<Pedido>
){}