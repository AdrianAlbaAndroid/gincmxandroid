package mx.ginc.data.entities;

public class ShirtProduct {
    private String name;
    private int image;
    private String price;
    private boolean save;


    public ShirtProduct(String name, int image, String price, boolean save) {
        this.name = name;
        this.image = image;
        this.price = price;
        this.save = save;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isSave() {
        return save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }
}
