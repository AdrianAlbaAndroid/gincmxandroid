package mx.ginc.data.entities

data class UserRegister (val name : String, val email : String? = null, val aPaterno : String? = null, val aMaterno : String? = null, val password : String? = null)