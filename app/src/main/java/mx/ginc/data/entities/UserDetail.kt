package mx.ginc.data.entities

data class UserDetail (val usuario : String, val name : String, val email : String? = null, val aPaterno : String? = null, val aMaterno : String? = null, val fechaAlta : String? = null,)