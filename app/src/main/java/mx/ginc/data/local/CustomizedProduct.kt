package mx.ginc.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customized_product")
 class CustomizedProduct{
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int? = null
        @ColumnInfo(name = "idProduct")
        var idProduct: Int? = null
        @ColumnInfo(name = "CustomizedName")
        var customizedName: String? = null
        @ColumnInfo(name = "indexProduct")
        var indexProduct: Int? = null
        @ColumnInfo(name = "selected")
        var selected: Boolean = false
        @ColumnInfo(name = "urlIcon")
        var urlIcon: String? = null
        @ColumnInfo(name = "itemSelected")
        var itemSelected: String? = null
        @ColumnInfo(name = "tipoManga")
        var tipoManga: String? = null
 }
