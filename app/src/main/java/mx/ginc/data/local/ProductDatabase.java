package mx.ginc.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import mx.ginc.data.AppUtil;

@Database(entities = { ProductSection.class, CustomizedProduct.class }, version = 3)
public abstract class ProductDatabase extends RoomDatabase {
    private static ProductDatabase instance;

    static final Migration MIGRATION_1_2 = new Migration(2,3) {
        @Override
        public void migrate (SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE CustomizedProduct ADD COLUMN tipoManga TEXT");
            // Como no modificamos la tabla, no hay nada más que hacer aquí.

            //database.execSQL("ALTER TABLE transaccion ADD COLUMN userType TEXT")
            //database.execSQL("ALTER TABLE transaccion ADD COLUMN refqr TEXT")
        }
    };

    public static synchronized ProductDatabase getInstance(Context context){
        if(instance ==null){
            instance = Room.databaseBuilder(context.getApplicationContext(), ProductDatabase.class, "ginc_db")
                    .addMigrations(MIGRATION_1_2)
                    //.fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract ProductDao productDao();

    public abstract CustomizedDao customizerDao();
}
