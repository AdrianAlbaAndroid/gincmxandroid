package mx.ginc.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customizer")
data class Customizer (
        @PrimaryKey
        @ColumnInfo(name = "idCustomizer")
        val idCustomizer: Int,
        @ColumnInfo(name = "idCatalogo")
        val idCatalogo: Int,
        @ColumnInfo(name = "idCatalogoBase")
        val idCatalogoBase: Int,
        @ColumnInfo(name = "nivel1")
        val nivel1: Int,
        @ColumnInfo(name = "nivel2")
        val nivel2: Int,
        @ColumnInfo(name = "nivel3")
        val nivel3: Int,
)