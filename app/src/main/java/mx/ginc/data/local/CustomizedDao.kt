package mx.ginc.data.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CustomizedDao {
    @Insert
    fun insertCustomized(customizedProduct: CustomizedProduct?)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCustomized(customizedProduct: CustomizedProduct)

    @Query("UPDATE customized_product SET selected = :selected")
    fun updateSelected(selected: Boolean)

    @Query("SELECT * FROM customized_product")
    fun getCustomizedAll(): List<CustomizedProduct>

    @Query("SELECT * FROM customized_product")
    fun getCustomizedMenu():  LiveData<List<CustomizedProduct>>

    @Query("SELECT * FROM customized_product WHERE itemSelected = :name")
    fun getCustomizedByName(name: String): List<CustomizedProduct>

    @Query("DELETE FROM customized_product")
    fun deleteAll()
}