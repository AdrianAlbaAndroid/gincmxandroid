package mx.ginc.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_section")
class ProductSection {
    @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var idCart: Int? = null
        @ColumnInfo(name = "idProducto")
        var idProducto: Int? = null
        @ColumnInfo(name = "description")
        var description: String? = null
        @ColumnInfo(name = "tipo")
        var tipo: String? = null
        @ColumnInfo(name = "sexo")
        var sexo: String? = null
        @ColumnInfo(name = "price_base")
        var price_base: Double? = null
        @ColumnInfo(name = "price_final")
        var price_final: Double? = null
        @ColumnInfo(name = "amount")
        var amount: Int? = null
        @ColumnInfo(name = "idCategoria")
        var idCategoria: Int? = null
        @ColumnInfo(name = "size")
        var size: String? = null
        @ColumnInfo(name = "imgUrl")
        var imgUrl: String? = null
        @ColumnInfo(name = "objProduct")
        var objProduct: String? = null
        @ColumnInfo(name = "objEspecificaciones")
        var objEspecificaciones: String? = null
        @ColumnInfo(name = "imgCustomizer")
        var imgCustomizer: String? = null

}
