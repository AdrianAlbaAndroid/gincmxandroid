package mx.ginc.data.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ProductDao {
    @Insert
    fun insertProduct(productSection: ProductSection?)

    @Query("DELETE FROM product_section WHERE id = :userId")
    fun deleteByUserId(userId: Int)

    @Query("DELETE FROM product_section")
    fun deleteAll()

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateProduct(productSection: ProductSection)

    @Query("SELECT * FROM product_section")
    fun getAllProduct(): LiveData<List<ProductSection>>

    @Query("SELECT * FROM product_section")
    fun getAll(): List<ProductSection>

    @Query("SELECT COUNT(*) FROM product_section")
    fun getCount(): LiveData<Int>
}