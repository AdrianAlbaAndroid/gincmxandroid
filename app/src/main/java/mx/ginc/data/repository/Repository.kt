package mx.ginc.data.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.google.gson.JsonObject
import mx.ginc.data.entities.*
import mx.ginc.data.network.DirectionServer
import mx.ginc.data.network.MeasurementServer
import mx.ginc.data.network.ProductServer
import mx.ginc.data.network.PurchaseServer

object Repository {

    fun getCatalog(id: Int) : LiveData<List<Customizer>> {
        return ProductServer.getInstance().getCatalog(id)
    }

    fun getSubCatalog(jsonObject: JsonObject) : LiveData<List<Product>> {
        return ProductServer.getInstance().getProductFilter(jsonObject)
    }
    fun getDirectionAll(context: Context) : LiveData<List<DirectionAll>> {
        return DirectionServer.getInstance(context).getDirectionAll();
    }

    fun getImageCatalog(id: Int) : LiveData<List<String>> {
        return ProductServer.getInstance().getImagesCatalog(id);
    }

    fun getMisPedidos(jsonObject: JsonObject, context: Context) : LiveData<List<ParentPedido>> {
        return PurchaseServer.getInstance(context).getMisPedidos(jsonObject);
    }

    fun getFitPref(context: Context) : LiveData<List<Belly>> {
        return MeasurementServer.getInstance(context).getFitPreference();
    }

    fun getBelly(context: Context) : LiveData<List<Belly>> {
        return MeasurementServer.getInstance(context).getBellyPreference();
    }

    fun getChest(context: Context) : LiveData<List<Belly>> {
        return MeasurementServer.getInstance(context).getChestPreference();
    }

    fun getMiMedida(context: Context) : LiveData<List<Medidas>> {
        return MeasurementServer.getInstance(context).getMedidasAll();
    }

    fun getCategorias() : LiveData<List<Customizer>> {
        return ProductServer.getInstance().getCategorias();
    }

    fun getEspecificaciones(idProducto: Int) : LiveData<List<Especificaciones>> {
        return ProductServer.getInstance().getEspecificaciones(idProducto);
    }

}