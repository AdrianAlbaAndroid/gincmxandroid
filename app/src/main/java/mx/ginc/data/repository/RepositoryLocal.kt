package mx.ginc.data.repository

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mx.ginc.data.local.*

class RepositoryLocal(private val viewModel : AndroidViewModel) {
    private var databaseDao : ProductDao = ProductDatabase.getInstance(viewModel.getApplication()).productDao()
    private var customizerDao : CustomizedDao = ProductDatabase.getInstance(viewModel.getApplication()).customizerDao()

    interface ProductsAllCallBack {
        fun setProducts(products : List<ProductSection>)
    }

    interface CustomizedAllCallBack {
        fun setCustomized(products : List<CustomizedProduct>)
    }

    var listener: ProductsAllCallBack? = null
    var listenerCustomized: CustomizedAllCallBack? = null

    fun insertProduct(productSection : ProductSection) = viewModel.viewModelScope.launch(Dispatchers.IO) {
        databaseDao.insertProduct(productSection)
    }

    fun insertCustomizedProduct(customizedProduct : CustomizedProduct) = viewModel.viewModelScope.launch(Dispatchers.IO) {
        customizerDao.insertCustomized(customizedProduct)
    }

    fun updateCustomizedProduct(customizedProduct : CustomizedProduct) = viewModel.viewModelScope.launch(Dispatchers.IO) {
        customizerDao.updateCustomized(customizedProduct)
    }

    fun getAll() : LiveData<List<ProductSection>>{
        return databaseDao.getAllProduct()
    }

    fun getProductsAll(callBack: ProductsAllCallBack) {
        this.listener = callBack
        this.listener!!.setProducts(databaseDao.getAll())
    }

    fun getCustomizedAll(callBackCustomized: CustomizedAllCallBack) {
        this.listenerCustomized = callBackCustomized
        this.listenerCustomized!!.setCustomized(customizerDao.getCustomizedAll())
    }

    fun getCustomizedMenu() : LiveData<List<CustomizedProduct>> {
        return customizerDao.getCustomizedMenu()
    }

    fun updateProduct(productSection : ProductSection) = viewModel.viewModelScope.launch(Dispatchers.IO){
        databaseDao.updateProduct(productSection)
    }

    fun getCount() : LiveData<Int> {
       return databaseDao.getCount()
    }

    fun deleteProduct(userId : Int)= viewModel.viewModelScope.launch(Dispatchers.IO) {
        databaseDao.deleteByUserId(userId)
    }

    fun deleteAll()= viewModel.viewModelScope.launch(Dispatchers.IO) {
        databaseDao.deleteAll()
    }

    fun deleteCustmizedAll()= viewModel.viewModelScope.launch(Dispatchers.IO) {
        customizerDao.deleteAll()
    }

    fun updateSelectedAll(selected : Boolean)= viewModel.viewModelScope.launch(Dispatchers.IO) {
        customizerDao.updateSelected(selected)
    }
}