package mx.ginc.data

enum class ProviderType {
    BASIC, GOOGLE, FACEBOOK
}