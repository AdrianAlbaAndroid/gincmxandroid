package mx.ginc.adapter;

import static com.squareup.picasso.MemoryPolicy.NO_CACHE;
import static com.squareup.picasso.MemoryPolicy.NO_STORE;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Customizer;
import mx.ginc.databinding.ItemMenuSubitemBinding;

public class SubItemAdapter extends RecyclerView.Adapter<SubItemAdapter.PersonalViewHolder> {

    private List<Customizer> mPersonalList;
    public Context mContext;
    public Activity mActivity;
    private int checkedPosition = 0;
    private SubItemAdapter.PersonalViewHolder viewHolder;
    private int image;
    private Boolean item;

    public SubItemAdapter(Activity mActivity, Context context, List<Customizer> mPersonalList, SubItemAdapter.SubItemIdCallBack subItemIdCallBack, Boolean item) {
        this.mContext = context;
        this.subItemIdCallBack = subItemIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
        this.item = item;
    }

    public interface SubItemIdCallBack {
        void onSelectedSubItem(Customizer subItem);
    }

    private SubItemAdapter.SubItemIdCallBack subItemIdCallBack;

    @NonNull
    @Override
    public SubItemAdapter.PersonalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMenuSubitemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_menu_subitem, parent, false);
        return new SubItemAdapter.PersonalViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull SubItemAdapter.PersonalViewHolder holder, int position) {
        viewHolder = (SubItemAdapter.PersonalViewHolder) holder;
        String listado = mPersonalList.get(position).getUrlIcon();
        if(mPersonalList.get(position).getUrlIcon() != null){
            Picasso.with(mContext).load(mPersonalList.get(position).getUrlIcon()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuPrBinding.imgPri);
        }else{
            viewHolder.itemMenuPrBinding.imgPri.setImageResource(R.drawable.ginc_logo);
        }
        viewHolder.itemMenuPrBinding.txtNameSubItem.setText(mPersonalList.get(position).getEtiqueta());
        if(item){
            viewHolder.itemMenuPrBinding.crdSubItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPersonalList.get(position) != null) {
                        subItemIdCallBack.onSelectedSubItem(mPersonalList.get(position));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class PersonalViewHolder extends RecyclerView.ViewHolder {
        ItemMenuSubitemBinding itemMenuPrBinding;

        public PersonalViewHolder(@NonNull ItemMenuSubitemBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuPrBinding = itemView;
        }
    }
}