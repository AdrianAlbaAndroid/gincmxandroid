package mx.ginc.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.databinding.ItemImageCatalogBinding;

public class CatalogImageAdapter extends RecyclerView.Adapter<CatalogImageAdapter.CatalogImageViewHolder> {

    private List<String> mCatalogImageList;
    public Context mContext;
    public Activity mActivity;

    public CatalogImageAdapter(Activity mActivity, Context context, List<String> mCatalogImageList) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mCatalogImageList = mCatalogImageList;
    }

    @NonNull
    @Override
    public CatalogImageAdapter.CatalogImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemImageCatalogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_image_catalog, parent, false);
        return new CatalogImageAdapter.CatalogImageViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull CatalogImageViewHolder holder, int position) {
        CatalogImageAdapter.CatalogImageViewHolder viewHolder = (CatalogImageAdapter.CatalogImageViewHolder) holder;
        Picasso.with(mContext).load(mCatalogImageList.get(position)).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemImageCatalogBinding.imgCatalogDetail);
    }

    @Override
    public int getItemCount() {
        if (mCatalogImageList != null)
            return mCatalogImageList.size();
        else return 0;
    }

    public static class CatalogImageViewHolder extends RecyclerView.ViewHolder {
        final ItemImageCatalogBinding itemImageCatalogBinding;

        public CatalogImageViewHolder(@NonNull ItemImageCatalogBinding itemView) {
            super(itemView.getRoot());
            this.itemImageCatalogBinding = itemView;
        }
    }
}