package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.data.local.ProductSection;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ItemConfirmationBinding;

public class ConfirmationAdapter extends RecyclerView.Adapter<ConfirmationAdapter.ConfirmationViewHolder> {

    private List<ProductSection> mCartList;
    public Context mContext;
    public Activity mActivity;

    public ConfirmationAdapter(Activity mActivity, Context context, List<ProductSection> mCartList) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mCartList = mCartList;
    }

    @NonNull
    @Override
    public ConfirmationAdapter.ConfirmationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemConfirmationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_confirmation, parent, false);
        return new ConfirmationAdapter.ConfirmationViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull ConfirmationViewHolder holder, int position) {
        ConfirmationAdapter.ConfirmationViewHolder viewHolder = (ConfirmationAdapter.ConfirmationViewHolder) holder;
        if(mCartList.get(position).getTipo().equals(AppUtil.TipoProducto.CATALOGO)){
            if(!mCartList.get(position).getImgUrl().equals("")){
                Picasso.with(mContext).load(mCartList.get(position).getImgUrl()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemConfirmationBinding.imageCart);
                viewHolder.itemConfirmationBinding.webviewImage.setVisibility(View.GONE);
                viewHolder.itemConfirmationBinding.imageCart.setVisibility(View.VISIBLE);
            }
        }else if(mCartList.get(position).getTipo().equals(AppUtil.TipoProducto.PERSONALIZADO)){
            if(!mCartList.get(position).getObjProduct().equals("")) {
                String url = AppUtil.Urls.URL_IMAGES + mCartList.get(position).getObjProduct();
                viewHolder.itemConfirmationBinding.webviewImage.loadUrl(url);
                viewHolder.itemConfirmationBinding.webviewImage.loadUrl(url);
                viewHolder.itemConfirmationBinding.webviewImage.clearCache(true);
                viewHolder.itemConfirmationBinding.webviewImage.getSettings().setJavaScriptEnabled(true);
                viewHolder.itemConfirmationBinding.webviewImage.getSettings().setDomStorageEnabled(true);
                viewHolder.itemConfirmationBinding.webviewImage.getSettings().setBlockNetworkImage(true);
                viewHolder.itemConfirmationBinding.webviewImage.getSettings().setBuiltInZoomControls(true);
                viewHolder.itemConfirmationBinding.webviewImage.setVisibility(View.VISIBLE);
                viewHolder.itemConfirmationBinding.imageCart.setVisibility(View.GONE);
            }
        }
        viewHolder.itemConfirmationBinding.nombreProducto.setText(mCartList.get(position).getDescription());
        viewHolder.itemConfirmationBinding.cantidad.setText("Cantidad: " + mCartList.get(position).getAmount());
        viewHolder.itemConfirmationBinding.tamano.setText(mCartList.get(position).getSize());
        if(mCartList.get(position).getPrice_final() == 0.0) {
            viewHolder.itemConfirmationBinding.precio.setText("$ 0.00");
        }else{
            viewHolder.itemConfirmationBinding.precio.setText(AppUtil.customFormat("$ ###,###.00", mCartList.get(position).getPrice_final()));
        }
        viewHolder.itemConfirmationBinding.precio.setTextColor(Color.RED);
    }

    @Override
    public int getItemCount() {
        if (mCartList != null)
            return mCartList.size();
        else return 0;
    }

    public static class ConfirmationViewHolder extends RecyclerView.ViewHolder {
        final ItemConfirmationBinding itemConfirmationBinding;


        public ConfirmationViewHolder(@NonNull ItemConfirmationBinding itemView) {
            super(itemView.getRoot());
            this.itemConfirmationBinding = itemView;
        }
    }
}