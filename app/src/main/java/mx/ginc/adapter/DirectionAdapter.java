package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Direccion;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.network.DirectionServer;
import mx.ginc.databinding.ItemDirectionBinding;

public class DirectionAdapter extends RecyclerView.Adapter<DirectionAdapter.DirectionViewHolder> {

    private List<DirectionAll> mDirectionAll;
    public Context mContext;
    public Activity mActivity;
    private Direccion direccion;

    public DirectionAdapter(Activity mActivity, Context context, List<DirectionAll> mDirectionAll, DirectionAdapter.DirectionDeleteCallBack directionDeleteCallBack, DirectionAdapter.DirectionEditCallBack directionEditCallBack) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mDirectionAll = mDirectionAll;
        this.directionDeleteCallBack = directionDeleteCallBack;
        this.directionEditCallBack = directionEditCallBack;

    }

    private DirectionAdapter.DirectionDeleteCallBack directionDeleteCallBack;
    private DirectionAdapter.DirectionEditCallBack directionEditCallBack;

    public interface DirectionDeleteCallBack {
        void onSelectedDeleted(int id, int position);
    }

    public interface DirectionEditCallBack {
        void onSelected(Direccion direction);
    }

    @NonNull
    @Override
    public DirectionAdapter.DirectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemDirectionBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_direction, parent, false);
        return new DirectionAdapter.DirectionViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull DirectionViewHolder holder, int position) {
        DirectionAdapter.DirectionViewHolder viewHolder = (DirectionAdapter.DirectionViewHolder) holder;
        if(mDirectionAll.get(position).getPrincipal()){
            viewHolder.itemDirectionBinding.predetermiando.setVisibility(View.VISIBLE);
        }else{
            viewHolder.itemDirectionBinding.predetermiando.setVisibility(View.GONE);
        }
        viewHolder.itemDirectionBinding.Nombre.setText(mDirectionAll.get(position).getQuienRecibe());
        viewHolder.itemDirectionBinding.Direccion.setText(mDirectionAll.get(position).getCallePrincipal() + ", " +
                mDirectionAll.get(position).getNumeroExterior() + ", " +
                mDirectionAll.get(position).getNumeroInteriorDepto() + ", " +
                mDirectionAll.get(position).getMunicipio() + ", " +
                mDirectionAll.get(position).getColonia() + ", " +
                mDirectionAll.get(position).getCodigoPostal() + ", " +
                mDirectionAll.get(position).getEntidad());
        viewHolder.itemDirectionBinding.ReferenciasCalles.setText(mDirectionAll.get(position).getCalle1() + " y " +
                mDirectionAll.get(position).getCalle2());
        viewHolder.itemDirectionBinding.TelefonoRegistro.setText(mDirectionAll.get(position).getTelefono());
        if(mDirectionAll.get(position).getPrincipal()){
            viewHolder.itemDirectionBinding.Preferente.setVisibility(View.VISIBLE);
        }else{
            viewHolder.itemDirectionBinding.Preferente.setVisibility(View.GONE);
        }

        viewHolder.itemDirectionBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directionDeleteCallBack.onSelectedDeleted(mDirectionAll.get(position).getId(), position);
                notifyDataSetChanged();
            }
        });
        viewHolder.itemDirectionBinding.Editar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                direccion = new Direccion(mDirectionAll.get(position).getId(),
                        mDirectionAll.get(position).getQuienRecibe(),
                        mDirectionAll.get(position).getTelefono(),
                        mDirectionAll.get(position).getColonia(),
                        mDirectionAll.get(position).getCodigoPostal(),
                        mDirectionAll.get(position).getMunicipio(),
                        mDirectionAll.get(position).getEntidad(),
                        mDirectionAll.get(position).getCallePrincipal(),
                        mDirectionAll.get(position).getCalle1(),
                        mDirectionAll.get(position).getCalle2(),
                        mDirectionAll.get(position).getNumeroExterior(),
                        mDirectionAll.get(position).getNumeroInteriorDepto(),
                        mDirectionAll.get(position).getIndicacionesAdicionales(),
                        mDirectionAll.get(position).getPrincipal(),
                        mDirectionAll.get(position).getJsonObject());
                System.out.println("Direcccion------------------" + direccion);
                directionEditCallBack.onSelected(direccion);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mDirectionAll != null)
            return mDirectionAll.size();
        else return 0;
    }

    public static class DirectionViewHolder extends RecyclerView.ViewHolder {
        final ItemDirectionBinding itemDirectionBinding;

        public DirectionViewHolder(@NonNull ItemDirectionBinding itemView) {
            super(itemView.getRoot());
            itemDirectionBinding = itemView;

        }
    }
}