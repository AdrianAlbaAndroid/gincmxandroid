package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.databinding.ItemMenuPrBinding;

public class ItemMainAdapter  extends RecyclerView.Adapter<ItemMainAdapter.PersonalViewHolder> {

    private List<CustomizedProduct> mPersonalList;
    public Context mContext;
    public Activity mActivity;
    private int checkedPosition = 0;
    private boolean mCustomerHome = false;

    public ItemMainAdapter(Activity mActivity, Context context, List<CustomizedProduct> mPersonalList, ItemMainAdapter.PersonalIdCallBack personalIdCallBack, boolean customerHome) {
        this.mContext = context;
        this.personalIdCallBack = personalIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
        this.mCustomerHome = customerHome;
    }

    public interface PersonalIdCallBack {
        void onSelected(CustomizedProduct customizer, int position);
    }

    private ItemMainAdapter.PersonalIdCallBack personalIdCallBack;

    @NonNull
    @Override
    public ItemMainAdapter.PersonalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMenuPrBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_menu_pr, parent, false);
        return new ItemMainAdapter.PersonalViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull ItemMainAdapter.PersonalViewHolder holder, int position) {
        ItemMainAdapter.PersonalViewHolder viewHolder = (ItemMainAdapter.PersonalViewHolder) holder;
        if (checkedPosition == 0) {
            viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
        } else {
            if (checkedPosition == viewHolder.getAdapterPosition()) {
                viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray));
                viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray));
                viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            } else {
                viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext, R.color.colorBlack));
            }
        }

        if (mPersonalList.get(position).getUrlIcon() != null) {
            Picasso.with(mContext).load(mPersonalList.get(position).getUrlIcon()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuPrBinding.imgProd);
        } else {
            viewHolder.itemMenuPrBinding.imgProd.setImageResource(R.drawable.ginc_logo);
        }
        viewHolder.itemMenuPrBinding.txtName.setText(mPersonalList.get(position).getCustomizedName());
        if (mCustomerHome) {
            if (mPersonalList.get(position).getSelected()) {
                viewHolder.itemMenuPrBinding.imgStatus.setVisibility(View.VISIBLE);
                viewHolder.itemMenuPrBinding.imgStatus.setImageResource(R.drawable.ic_baseline_check_black);
            }
        }
        viewHolder.itemMenuPrBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPersonalList.get(position) != null) {
                    viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray));
                    viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorGray));
                    viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                    if (checkedPosition != viewHolder.getAdapterPosition()) {
                        notifyItemChanged(checkedPosition);
                        checkedPosition = viewHolder.getAdapterPosition();
                    }
                    personalIdCallBack.onSelected(mPersonalList.get(position), position);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class PersonalViewHolder extends RecyclerView.ViewHolder {
        final ItemMenuPrBinding itemMenuPrBinding;

        public PersonalViewHolder(@NonNull ItemMenuPrBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuPrBinding = itemView;
        }
    }
}
