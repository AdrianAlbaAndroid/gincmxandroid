package mx.ginc.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import mx.ginc.R;
import mx.ginc.data.entities.Especificaciones;
import mx.ginc.databinding.ItemAtributoBinding;

public class AtributoAdapter extends RecyclerView.Adapter<AtributoAdapter.AtributoViewHolder> {

    private List<Especificaciones> mEspecificacionesList;
    private List<Especificaciones> atributosLista;
    public Context mContext;
    public Activity mActivity;
    private String selected;
    private List<String> mValoresList;

    public AtributoAdapter(Activity mActivity, Context context, List<Especificaciones> mEspecificacionesList, List<Especificaciones> atributosLista) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mEspecificacionesList = mEspecificacionesList;
        this.atributosLista = atributosLista;
    }

    @NonNull
    @Override
    public AtributoAdapter.AtributoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAtributoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_atributo, parent, false);
        return new AtributoAdapter.AtributoViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AtributoAdapter.AtributoViewHolder holder, int position) {
        AtributoAdapter.AtributoViewHolder viewHolder = (AtributoAdapter.AtributoViewHolder) holder;
        viewHolder.itemAtributoBinding.NombreAtributo.setText(mEspecificacionesList.get(position).getEtiqueta().toUpperCase());
        viewHolder.itemAtributoBinding.txtSeleccionaTalla.setText("Seleccione un " + mEspecificacionesList.get(position).getEtiqueta().toLowerCase());
        viewHolder.itemAtributoBinding.txtSeleccionaTalla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionsDialog(viewHolder.itemAtributoBinding.txtSeleccionaTalla,
                        mEspecificacionesList.get(position).getEtiqueta());
            }
        });
    }


    @Override
    public int getItemCount() {
        if (mEspecificacionesList != null)
            return mEspecificacionesList.size();
        else return 0;
    }

    public static class AtributoViewHolder extends RecyclerView.ViewHolder {
        final ItemAtributoBinding itemAtributoBinding;


        public AtributoViewHolder(@NonNull ItemAtributoBinding itemView) {
            super(itemView.getRoot());
            this.itemAtributoBinding = itemView;
        }
    }

    public void showOptionsDialog(TextView txtSeleccionado,String nombreAtributo){
        mValoresList = new ArrayList<>();
        List<String> mAtributosLista =  listaFiltroPorValor(atributosLista, nombreAtributo);

        //Create sequence of items
        final CharSequence[] Atributos = mAtributosLista.toArray(new String[mAtributosLista.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        dialogBuilder.setTitle(nombreAtributo);
        dialogBuilder.setItems(Atributos, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                selected = Atributos[item].toString();  //Selected item in listview
                txtSeleccionado.setText(selected);
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();

    }

    public List<String> listaFiltroPorValor(List<Especificaciones> especificacionesAll, String valor){
        List<String> listaAtributo = new ArrayList<>();
        for(int i = 0; i < especificacionesAll.size(); i++){
            if(especificacionesAll.get(i).getEtiqueta().equals(valor)){
                listaAtributo.add(especificacionesAll.get(i).getValor());
            }
        }
        return listaAtributo;
    }
}