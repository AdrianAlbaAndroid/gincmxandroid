package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Customizer;
import mx.ginc.databinding.ItemMenuSubitemBinding;
import mx.ginc.databinding.ItemTelasBinding;

public class TelasAdapter extends RecyclerView.Adapter<TelasAdapter.PersonalViewHolder> {

    private List<Customizer> mPersonalList;
    public Context mContext;
    public Activity mActivity;
    private int checkedPosition = 0;
    private TelasAdapter.PersonalViewHolder viewHolder;
    private int image;
    private Boolean item;

    public TelasAdapter(Activity mActivity, Context context, List<Customizer> mPersonalList, TelasAdapter.SubItemIdCallBack subItemIdCallBack, Boolean item) {
        this.mContext = context;
        this.subItemIdCallBack = subItemIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
        this.item = item;
    }

    public interface SubItemIdCallBack {
        void onSelectedSubItem(Customizer subItem);
    }

    private TelasAdapter.SubItemIdCallBack subItemIdCallBack;

    @NonNull
    @Override
    public TelasAdapter.PersonalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemTelasBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_telas, parent, false);
        return new TelasAdapter.PersonalViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull PersonalViewHolder personalViewHolder, int i) {
        viewHolder = (TelasAdapter.PersonalViewHolder) personalViewHolder;
        if(mPersonalList.get(i).getUrlIcon() != null){
            Picasso.with(mContext).load(mPersonalList.get(i).getUrlIcon()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuPrBinding.imgPri);
        }else{
            viewHolder.itemMenuPrBinding.imgPri.setImageResource(R.drawable.ginc_logo);
        }
        viewHolder.itemMenuPrBinding.txtNameSubItem.setText(mPersonalList.get(i).getEtiqueta());
        if(item){
            viewHolder.itemMenuPrBinding.crdSubItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mPersonalList.get(i) != null) {
                        subItemIdCallBack.onSelectedSubItem(mPersonalList.get(i));
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class PersonalViewHolder extends RecyclerView.ViewHolder {
        ItemTelasBinding itemMenuPrBinding;

        public PersonalViewHolder(@NonNull ItemTelasBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuPrBinding = itemView;
        }
    }
}