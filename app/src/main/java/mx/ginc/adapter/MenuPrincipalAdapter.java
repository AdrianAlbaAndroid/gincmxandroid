package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Menu;
import mx.ginc.databinding.ItemMenuPrincipalBinding;

public class MenuPrincipalAdapter extends RecyclerView.Adapter<MenuPrincipalAdapter.MenuPrincipalViewHolder> {

    private List<Menu> mPersonalList;
    public Context mContext;
    public Activity mActivity;

    public MenuPrincipalAdapter(Activity mActivity, Context context, List<Menu> mPersonalList, MenuPrincipalAdapter.MenuPrincipalIdCallBack menuIdCallBack) {
        this.mContext = context;
        this.menuIdCallBack = menuIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
    }

    public interface MenuPrincipalIdCallBack {
        void onSelectedMenu(Menu menu);
    }

    private MenuPrincipalAdapter.MenuPrincipalIdCallBack menuIdCallBack;

    @NonNull
    @Override
    public MenuPrincipalAdapter.MenuPrincipalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMenuPrincipalBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_menu_principal, parent, false);
        return new MenuPrincipalAdapter.MenuPrincipalViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull MenuPrincipalViewHolder holder, int position) {
        MenuPrincipalAdapter.MenuPrincipalViewHolder viewHolder = (MenuPrincipalAdapter.MenuPrincipalViewHolder) holder;
        Picasso.with(mContext).load(mPersonalList.get(position).getImage()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuBinding.image1);
        viewHolder.itemMenuBinding.txtMenu.setText(mPersonalList.get(position).getName());
        viewHolder.itemMenuBinding.imgLine.setImageResource(mPersonalList.get(position).getFondo());
        viewHolder.itemMenuBinding.txtMenu.setTextColor(ContextCompat.getColor(this.mContext, mPersonalList.get(position).getColor()));
        viewHolder.itemMenuBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPersonalList.get(position) != null) {
                    menuIdCallBack.onSelectedMenu(mPersonalList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class MenuPrincipalViewHolder extends RecyclerView.ViewHolder {
        final ItemMenuPrincipalBinding itemMenuBinding;

        public MenuPrincipalViewHolder(@NonNull ItemMenuPrincipalBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuBinding = itemView;
        }
    }
}