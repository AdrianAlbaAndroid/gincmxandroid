package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Product;
import mx.ginc.data.entities.ShirtProduct;
import mx.ginc.databinding.ItemShirtBinding;
import mx.ginc.databinding.ItemSubcatalogBinding;

public class SubCatalogAdapter extends RecyclerView.Adapter<SubCatalogAdapter.SubCatalogViewHolder> {

    private List<Product> mShirts;
    public Context mContext;
    public Activity mActivity;
    private String url;

    public SubCatalogAdapter(Activity mActivity, Context context, List<Product> mShirtList) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mShirts = mShirtList;
    }

    public SubCatalogAdapter(Activity mActivity, Context context, List<Product> mShirtList, SubCatalogAdapter.SubCatalogIdCallBack subCatalogoIdCallBack) {
        this.mContext = context;
        this.subCatalogIdCallBack = subCatalogoIdCallBack;
        this.mActivity = mActivity;
        this.mShirts = mShirtList;
    }

    public interface SubCatalogIdCallBack{
        void onSelected(Product note);
    }

    private SubCatalogAdapter.SubCatalogIdCallBack subCatalogIdCallBack;

    @NonNull
    @Override
    public SubCatalogAdapter.SubCatalogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSubcatalogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_subcatalog, parent, false);
        return new SubCatalogAdapter.SubCatalogViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull SubCatalogViewHolder holder, int position) {
        SubCatalogAdapter.SubCatalogViewHolder viewHolder = (SubCatalogAdapter.SubCatalogViewHolder) holder;
        if(!mShirts.get(position).getUrl().equals("")){
            Picasso.with(mContext).load(mShirts.get(position).getUrl()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemCatalogBinding.imgProd);
        }
        viewHolder.itemCatalogBinding.txtProducto.setText(mShirts.get(position).getEtiqueta());
        viewHolder.itemCatalogBinding.btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShirts.get(position) != null) {
                    subCatalogIdCallBack.onSelected(mShirts.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mShirts != null)
            return mShirts.size();
        else return 0;
    }

    public static class SubCatalogViewHolder extends RecyclerView.ViewHolder {
        final ItemSubcatalogBinding itemCatalogBinding;

        public SubCatalogViewHolder(@NonNull ItemSubcatalogBinding itemView) {
            super(itemView.getRoot());
            this.itemCatalogBinding = itemView;
        }
    }

}