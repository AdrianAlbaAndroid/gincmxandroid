package mx.ginc.adapter;

import static com.squareup.picasso.MemoryPolicy.NO_CACHE;
import static com.squareup.picasso.MemoryPolicy.NO_STORE;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.ItemCustomer;
import mx.ginc.databinding.ItemMenuPrBinding;
import mx.ginc.views.CustomerActivity;
import mx.ginc.views.CustomizingSpecificActivity;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.PersonalViewHolder> {

    private List<Customizer> mPersonalList;
    public Context mContext;
    public Activity mActivity;
    private int checkedPosition = 0;
    private boolean mCustomerHome = false;

    public ItemAdapter(Activity mActivity, Context context, List<Customizer> mPersonalList, ItemAdapter.PersonalIdCallBack personalIdCallBack, boolean customerHome) {
        this.mContext = context;
        this.personalIdCallBack = personalIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
        this.mCustomerHome = customerHome;
    }

    public interface PersonalIdCallBack {
            void onSelected(Customizer customizer);
    }

    private ItemAdapter.PersonalIdCallBack personalIdCallBack;

    @NonNull
    @Override
    public ItemAdapter.PersonalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMenuPrBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_menu_pr, parent, false);
        return new ItemAdapter.PersonalViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull PersonalViewHolder holder, int position) {
        ItemAdapter.PersonalViewHolder viewHolder = (ItemAdapter.PersonalViewHolder) holder;
        if(checkedPosition == 0){
            viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));
            viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));
            viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext,R.color.colorBlack));
        }else{
            if(checkedPosition == viewHolder.getAdapterPosition()){
                viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorGray));
                viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorGray));
                viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));
            }else{
                viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));
                viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));
                viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext,R.color.colorBlack));
            }
        }

        if(mPersonalList.get(position).getUrlIcon() != null){
            Picasso.with(mContext).load(mPersonalList.get(position).getUrlIcon())
                    .memoryPolicy(NO_CACHE, NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuPrBinding.imgProd);
        }else{
            viewHolder.itemMenuPrBinding.imgProd.setImageResource(R.drawable.ginc_logo);
        }
        viewHolder.itemMenuPrBinding.txtName.setText(mPersonalList.get(position).getEtiqueta());
        if(mCustomerHome){
            if(mPersonalList.get(position).getSelected()){
                viewHolder.itemMenuPrBinding.imgStatus.setVisibility(View.VISIBLE);
                viewHolder.itemMenuPrBinding.imgStatus.setImageResource(R.drawable.ic_baseline_check_black);
            }
        }
        viewHolder.itemMenuPrBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPersonalList.get(position) != null) {
                    viewHolder.itemMenuPrBinding.imgProd.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorGray));
                    viewHolder.itemMenuPrBinding.txtName.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorGray));
                    viewHolder.itemMenuPrBinding.txtName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));
                    if(checkedPosition != viewHolder.getAdapterPosition()){
                        notifyItemChanged(checkedPosition);
                        checkedPosition = viewHolder.getAdapterPosition();
                    }
                    personalIdCallBack.onSelected(mPersonalList.get(position));
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class PersonalViewHolder extends RecyclerView.ViewHolder {
        final ItemMenuPrBinding itemMenuPrBinding;

        public PersonalViewHolder(@NonNull ItemMenuPrBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuPrBinding = itemView;
        }
    }

    private ArrayList<String> getArrayList(String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
}