package mx.ginc.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.data.local.ProductSection;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ItemAddCartBinding;
import mx.ginc.dialog.NumberPickerDialog;
import mx.ginc.views.CatalogoDetailActivity;
import mx.ginc.views.ShoppingCartActivity;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> implements  NumberPicker.OnValueChangeListener {

    private List<ProductSection> mCartList;
    public Context mContext;
    public Activity mActivity;
    String selectedGender = "";
    private List<String> mAtributos;
    private String selectedSize = "";

    public CartAdapter(Activity mActivity, Context context, List<ProductSection> mCartList, CartAdapter.CartCallBack cartCallBack) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mCartList = mCartList;
        this.cartCallBack = cartCallBack;
    }

    public interface CartCallBack {
        void onSelected(ProductSection product);
        void onUpdate(ProductSection product, int amount);
    }

    public CartAdapter.CartCallBack cartCallBack;

    @NonNull
    @Override
    public CartAdapter.CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemAddCartBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_add_cart, parent, false);
        return new CartAdapter.CartViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        CartAdapter.CartViewHolder viewHolder = (CartAdapter.CartViewHolder) holder;
        viewHolder.itemAddCartBinding.nombreProducto.setText(mCartList.get(position).getDescription());
        viewHolder.itemAddCartBinding.txtCantidadSelected.setText("Cantidad: " + mCartList.get(position).getAmount());
        viewHolder.itemAddCartBinding.lblTalla.setText(mCartList.get(position).getSize());
        if(mCartList.get(position).getTipo().equals(AppUtil.TipoProducto.CATALOGO)){
            if(!mCartList.get(position).getImgUrl().equals("")) {
                Picasso.with(mContext).load(mCartList.get(position).getImgUrl()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemAddCartBinding.imageCart);
                viewHolder.itemAddCartBinding.webviewImage.setVisibility(View.GONE);
                viewHolder.itemAddCartBinding.imageCart.setVisibility(View.VISIBLE);
            }
        }else if(mCartList.get(position).getTipo().equals(AppUtil.TipoProducto.PERSONALIZADO)){
            if(!mCartList.get(position).getObjProduct().equals("")) {
                String url = AppUtil.Urls.URL_IMAGES + mCartList.get(position).getObjProduct();
                viewHolder.itemAddCartBinding.webviewImage.loadUrl(url);
                viewHolder.itemAddCartBinding.webviewImage.clearCache(true);
                viewHolder.itemAddCartBinding.webviewImage.getSettings().setJavaScriptEnabled(true);
                viewHolder.itemAddCartBinding.webviewImage.getSettings().setDomStorageEnabled(true);
                viewHolder.itemAddCartBinding.webviewImage.getSettings().setBlockNetworkImage(true);
                viewHolder.itemAddCartBinding.webviewImage.getSettings().setBuiltInZoomControls(true);
                viewHolder.itemAddCartBinding.webviewImage.getSettings().setDisplayZoomControls(true);
                viewHolder.itemAddCartBinding.webviewImage.setVisibility(View.VISIBLE);
                viewHolder.itemAddCartBinding.imageCart.setVisibility(View.GONE);
            }
        }
        if(mCartList.get(position).getPrice_final() == 0.0){
            viewHolder.itemAddCartBinding.precio.setText("$ 0.00");
        }else{
            viewHolder.itemAddCartBinding.precio.setText(AppUtil.customFormat("$ ###,###.00", mCartList.get(position).getPrice_base()));
        }
        viewHolder.itemAddCartBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartCallBack.onSelected(mCartList.get(position));
            }
        });
        viewHolder.itemAddCartBinding.txtCantidadSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog newFragment = new NumberPickerDialog();
                newFragment.setValueChangeListener(new NumberPicker.OnValueChangeListener() {
                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                        cartCallBack.onUpdate(mCartList.get(position), picker.getValue());
                    }
                });
                newFragment.show(((ShoppingCartActivity)mContext).getSupportFragmentManager(), "time picker");
            }
        });
        viewHolder.itemAddCartBinding.lblTalla.setText(mCartList.get(position).getSize());
    }

    @Override
    public int getItemCount() {
        if (mCartList != null)
            return mCartList.size();
        else return 0;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Toast.makeText(mContext,
                "selected number " + picker.getValue(), Toast.LENGTH_SHORT).show();
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {
        final ItemAddCartBinding itemAddCartBinding;

        public CartViewHolder(@NonNull ItemAddCartBinding itemView) {
            super(itemView.getRoot());
            this.itemAddCartBinding = itemView;
        }
    }
}