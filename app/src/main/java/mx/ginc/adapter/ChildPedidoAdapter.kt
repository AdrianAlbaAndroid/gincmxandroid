package mx.ginc.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import mx.ginc.R
import mx.ginc.adapter.MisPedidosAdapter.MisPedidosViewHolder
import mx.ginc.data.AppUtil
import mx.ginc.data.entities.Pedido
import mx.ginc.databinding.ChildRecyclerViewPedidosBinding
import mx.ginc.databinding.ItemPedidoBinding

class ChildPedidoAdapter(private val list: List<Pedido>, val context: Context,  val pedidoCallBack : ChildPedidoAdapter.PedidoCallBack ) :
    RecyclerView.Adapter<ChildPedidoAdapter.MisPedidosViewHolder>() {

    interface PedidoCallBack {
        fun onSelectedItem(pedido: Pedido?)
    }

    class MisPedidosViewHolder(val itemPedidoBinding: ItemPedidoBinding) : RecyclerView.ViewHolder(
        itemPedidoBinding.root
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MisPedidosViewHolder {
        val binding: ItemPedidoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_pedido, parent, false
        )
        return MisPedidosViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MisPedidosViewHolder, position: Int) {
        if (list.get(position).image != "") {
            Picasso.with(context).load(list.get(position).image).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
                .into(holder.itemPedidoBinding.imageCart)
        }
        holder.itemPedidoBinding.nombreProducto.setText(list.get(position).etiqueta)
        holder.itemPedidoBinding.cantidad.text =
            "Cantidad: " + list.get(position).cantidad
        holder.itemPedidoBinding.precio.text =
            "Precio : " + AppUtil.customFormat(
                "$ ###,###.00",
                list.get(position).costo.toDouble()
            )
        list.get(position).costo.toDouble()
        holder.itemPedidoBinding.crdItem.setOnClickListener {
            pedidoCallBack.onSelectedItem(
                list.get(position)
            )
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}