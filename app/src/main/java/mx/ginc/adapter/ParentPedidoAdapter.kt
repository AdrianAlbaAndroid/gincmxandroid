package mx.ginc.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import mx.ginc.data.AppUtil
import mx.ginc.data.entities.ParentPedido
import mx.ginc.databinding.ParentRecyclerViewPedidosBinding
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ParentPedidoAdapter(private val list: List<ParentPedido>?, val context: Context,  val pedidoCallBack : ChildPedidoAdapter.PedidoCallBack ) :
    RecyclerView.Adapter<ParentPedidoAdapter.PedidoViewHolder>() {

    inner class PedidoViewHolder(val viewDataBinding: ParentRecyclerViewPedidosBinding) : RecyclerView.ViewHolder(viewDataBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PedidoViewHolder {
        val binding = ParentRecyclerViewPedidosBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PedidoViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PedidoViewHolder, position: Int) {
        holder.viewDataBinding.section.text = "No. de pedido: " + list!![position].section
        holder.viewDataBinding.fechaCompra.text = "Fecha de compra: " + AppUtil.convertDate(list!![position].fechaCompra)
        holder.viewDataBinding.recyclerviewPedidos.apply{
            adapter = ChildPedidoAdapter(list[position].list, context, pedidoCallBack)
            setHasFixedSize(true)
            setItemViewCacheSize(20)

        }
    }

    override fun getItemCount(): Int {
       return list!!.size
    }
}