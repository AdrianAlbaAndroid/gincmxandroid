package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Menu;
import mx.ginc.data.entities.Pedido;
import mx.ginc.databinding.ItemPedidoBinding;
import mx.ginc.views.PurchaseDetailActivity;

public class MisPedidosAdapter extends RecyclerView.Adapter<MisPedidosAdapter.MisPedidosViewHolder> {

    private List<Pedido> mPedidoList;
    public Context mContext;
    public Activity mActivity;

    public MisPedidosAdapter(Activity mActivity, Context context, List<Pedido> mPedidoList, MisPedidosAdapter.PedidoCallBack pedidoCallBack) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mPedidoList = mPedidoList;
        this.pedidoCallBack = pedidoCallBack;
    }

    public interface PedidoCallBack {
        void onSelectedItem(Pedido pedido);
    }

    private MisPedidosAdapter.PedidoCallBack pedidoCallBack;

    @NonNull
    @Override
    public MisPedidosAdapter.MisPedidosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPedidoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_pedido, parent, false);
        return new MisPedidosAdapter.MisPedidosViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull MisPedidosAdapter.MisPedidosViewHolder holder, int position) {
        MisPedidosAdapter.MisPedidosViewHolder viewHolder = (MisPedidosAdapter.MisPedidosViewHolder) holder;
        if(!mPedidoList.get(position).getImage().equals("")){
            Picasso.with(mContext).load(mPedidoList.get(position).getImage()).into(viewHolder.itemPedidoBinding.imageCart);
        }

        Picasso.with(mContext).load(mPedidoList.get(position).getImage()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemPedidoBinding.imageCart);
        viewHolder.itemPedidoBinding.nombreProducto.setText(mPedidoList.get(position).getEtiqueta());
        viewHolder.itemPedidoBinding.cantidad.setText("Cantidad: " + mPedidoList.get(position).getCantidad());
        viewHolder.itemPedidoBinding.precio.setText("Precio : " +  AppUtil.customFormat("$ ###,###.00", mPedidoList.get(position).getCosto()));
        viewHolder.itemPedidoBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pedidoCallBack.onSelectedItem(mPedidoList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPedidoList != null)
            return mPedidoList.size();
        else return 0;
    }

    public static class MisPedidosViewHolder extends RecyclerView.ViewHolder {
        final ItemPedidoBinding itemPedidoBinding;

        public MisPedidosViewHolder(@NonNull ItemPedidoBinding itemView) {
            super(itemView.getRoot());
            this.itemPedidoBinding = itemView;
        }
    }
}