package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.entities.Medidas;
import mx.ginc.databinding.ItemDirectionBinding;
import mx.ginc.databinding.ItemMedidasBinding;

public class MedidasAdapter extends RecyclerView.Adapter<MedidasAdapter.MedidasViewHolder> {

    private List<Medidas> mMedidasAll;
    public Context mContext;
    public Activity mActivity;

    public MedidasAdapter(Activity mActivity, Context context, List<Medidas> mMedidasAll, MedidasAdapter.MedidasDeleteCallBack medidasDeleteCallBack) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mMedidasAll = mMedidasAll;
        this.medidasDeleteCallBack = medidasDeleteCallBack;

    }

    private MedidasAdapter.MedidasDeleteCallBack medidasDeleteCallBack;

    public interface MedidasDeleteCallBack {
        void onSelectedDeleted(int id,int position);
    }

    @NonNull
    @Override
    public MedidasAdapter.MedidasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMedidasBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_medidas, parent, false);
        return new MedidasAdapter.MedidasViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull MedidasViewHolder holder, int position) {
        MedidasAdapter.MedidasViewHolder viewHolder = (MedidasAdapter.MedidasViewHolder) holder;
        viewHolder.itemMedidasBinding.Peso.setText(mMedidasAll.get(position).getPeso());
        viewHolder.itemMedidasBinding.Aida.setText(mMedidasAll.get(position).getAida());
        viewHolder.itemMedidasBinding.Altura.setText(mMedidasAll.get(position).getPeso());
        viewHolder.itemMedidasBinding.Biceps.setText(mMedidasAll.get(position).getBiceps());
        viewHolder.itemMedidasBinding.Cadera.setText(mMedidasAll.get(position).getCadera());
        viewHolder.itemMedidasBinding.Cintura.setText(mMedidasAll.get(position).getCintura());
        viewHolder.itemMedidasBinding.Cuello.setText(mMedidasAll.get(position).getCuello());
        viewHolder.itemMedidasBinding.Edad.setText(mMedidasAll.get(position).getEdad());
        viewHolder.itemMedidasBinding.Espalda.setText(mMedidasAll.get(position).getEspalda());
        viewHolder.itemMedidasBinding.Largo.setText(mMedidasAll.get(position).getLargo());
        viewHolder.itemMedidasBinding.LargoManga.setText(mMedidasAll.get(position).getLargoManga());
        viewHolder.itemMedidasBinding.LargoMuneca.setText(mMedidasAll.get(position).getLargoMuneca());
        viewHolder.itemMedidasBinding.TipoCamisa.setText(mMedidasAll.get(position).getTipoCamisaMedida());
        viewHolder.itemMedidasBinding.TipoPecho.setText(mMedidasAll.get(position).getTipoPecho());
        viewHolder.itemMedidasBinding.Vientre.setText(mMedidasAll.get(position).getTipoVientre());
        viewHolder.itemMedidasBinding.BorrarMedida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medidasDeleteCallBack.onSelectedDeleted(Integer.valueOf(mMedidasAll.get(position).getId()), position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mMedidasAll != null)
            return mMedidasAll.size();
        else return 0;
    }

    public static class MedidasViewHolder extends RecyclerView.ViewHolder {
        final ItemMedidasBinding itemMedidasBinding;

        public MedidasViewHolder(@NonNull ItemMedidasBinding itemView) {
            super(itemView.getRoot());
            itemMedidasBinding = itemView;

        }
    }
}