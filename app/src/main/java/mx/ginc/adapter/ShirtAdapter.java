package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.ShirtProduct;
import mx.ginc.databinding.ItemShirtBinding;

public class ShirtAdapter extends RecyclerView.Adapter<ShirtAdapter.ShirtViewHolder> {

    private List<Customizer> mShirts;
    public Context mContext;
    public Activity mActivity;
    private int image;

    public ShirtAdapter(Activity mActivity, Context context, List<Customizer> mShirtList) {
        this.mContext = context;
        this.mActivity = mActivity;
        this.mShirts = mShirtList;
    }

    public ShirtAdapter(Activity mActivity, Context context, List<Customizer> mShirtList, ShirtIdCallBack shirtIdCallBack) {
        this.mContext = context;
        this.shirtIdCallBack = shirtIdCallBack;
        this.mActivity = mActivity;
        this.mShirts = mShirtList;
    }

    public interface ShirtIdCallBack{
        void onSelected(Customizer customizer);
    }

    private ShirtIdCallBack shirtIdCallBack;

    @NonNull
    @Override
    public ShirtViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemShirtBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_shirt, parent, false);
        return new ShirtViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull ShirtViewHolder holder, int position) {
        ShirtViewHolder viewHolder = (ShirtViewHolder) holder;
        switch(mShirts.get(position).getEtiqueta()){
            case "Lisas":
                image = R.drawable.tela01;
                break;
            case "Cuadros":
                image = R.drawable.tela02;
                break;
            case "Rayas":
                image = R.drawable.tela03;
                break;
            case "Especial":
                image = R.drawable.tela04;
                break;
        }
        viewHolder.itemShirtBinding.imgProd.setImageResource(image);
        viewHolder.itemShirtBinding.txtNombreTela.setText(mShirts.get(position).getEtiqueta());
        viewHolder.itemShirtBinding.crdItemCatalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mShirts.get(position) != null) {
                    shirtIdCallBack.onSelected(mShirts.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mShirts != null)
            return mShirts.size();
        else return 0;
    }

    public static class ShirtViewHolder extends RecyclerView.ViewHolder {
        final ItemShirtBinding itemShirtBinding;

        public ShirtViewHolder(@NonNull ItemShirtBinding itemView) {
            super(itemView.getRoot());
            this.itemShirtBinding = itemView;
        }
    }

}
