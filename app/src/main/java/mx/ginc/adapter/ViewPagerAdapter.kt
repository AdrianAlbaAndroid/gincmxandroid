package mx.ginc.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import mx.ginc.R
import mx.ginc.data.entities.Belly

class ViewPagerAdapter(val context: Context, val stringsList : List<Belly>, val selectionCallBack: SelectionCallBack?, val editar : Boolean, val flagsCallBack: FLagsCallBack?) : RecyclerView.Adapter<PagerVH>() {

    interface SelectionCallBack {
        fun onSelected(item: Belly?)
    }

    interface FLagsCallBack {
        fun onSelectedFlag(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH {
        return PagerVH(LayoutInflater.from(parent.context).inflate(R.layout.item_page, parent, false))
    }

    //get the size of color array
    override fun getItemCount(): Int = stringsList.size

    //binding the screen with view
    override fun onBindViewHolder(holder: PagerVH, position: Int) = holder.itemView.run {
        holder.btnPlano.setText(stringsList.get(0).etiqueta)
        holder.btnPromedio.setText(stringsList.get(1).etiqueta)
        holder.btnRedondo.setText(stringsList.get(2).etiqueta)
        if(editar){
            holder.btnContinuar.setText("Guardar")
        }
        if(position == 0) {
            holder.imgIzquierdo.visibility = View.INVISIBLE
            Picasso.with(context).load(stringsList.get(0).image).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(holder.imgPager)
            holder.btnPlano.setText(stringsList.get(0).etiqueta)
            holder.btnPlano.setBackgroundColor(Color.BLACK)
            holder.btnPlano.setTextColor(Color.WHITE)
            holder.imgDerecho.setOnClickListener{
                flagsCallBack!!.onSelectedFlag(1);
            }
        }
        if(position == 1) {
            Picasso.with(context).load(stringsList.get(1).image).into(holder.imgPager)
            holder.btnPromedio.setText(stringsList.get(1).etiqueta)
            holder.btnPromedio.setBackgroundColor(Color.BLACK)
            holder.btnPromedio.setTextColor(Color.WHITE)
            holder.imgIzquierdo.setOnClickListener{
                flagsCallBack!!.onSelectedFlag(0)
            }
            holder.imgDerecho.setOnClickListener{
                flagsCallBack!!.onSelectedFlag(2)
            }
        }
        if(position == 2) {
            holder.imgDerecho.visibility = View.INVISIBLE
            Picasso.with(context).load(stringsList.get(2).image).into(holder.imgPager)
            holder.btnRedondo.setText(stringsList.get(2).etiqueta)
            holder.btnRedondo.setBackgroundColor(Color.BLACK)
            holder.btnRedondo.setTextColor(Color.WHITE)
            holder.imgIzquierdo.setOnClickListener{
                flagsCallBack!!.onSelectedFlag(1)
            }
        }

        holder.btnContinuar.setOnClickListener{
            if (stringsList.get(position) != null) {
                selectionCallBack!!.onSelected(stringsList.get(position))
            }
        }
    }
}

class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView){
    val btnPlano: Button
    val btnPromedio: Button
    val btnRedondo : Button
    val btnContinuar : Button
    val imgPager : ImageView
    val imgIzquierdo : ImageView
    val imgDerecho : ImageView

    init {
        // Define click listener for the ViewHolder's View.
        btnPlano = itemView.findViewById(R.id.btnPlano)
        btnPromedio = itemView.findViewById(R.id.btnPromedio)
        btnRedondo = itemView.findViewById(R.id.btnRedondo)
        btnContinuar = itemView.findViewById(R.id.btnContinuar)
        imgPager = itemView.findViewById(R.id.ivImage)
        imgIzquierdo = itemView.findViewById(R.id.imgFechaIzq)
        imgDerecho = itemView.findViewById(R.id.imgFechaDerecha)
    }
}