package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Item;
import mx.ginc.databinding.ItemMenuBinding;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private List<Item> mPersonalList;
    public Context mContext;
    public Activity mActivity;

    public MenuAdapter(Activity mActivity, Context context, List<Item> mPersonalList, MenuAdapter.MenuIdCallBack menuIdCallBack) {
        this.mContext = context;
        this.menuIdCallBack = menuIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
    }

    public interface MenuIdCallBack {
        void onSelectedMenu(Item personal);
    }

    private MenuAdapter.MenuIdCallBack menuIdCallBack;

    @NonNull
    @Override
    public MenuAdapter.MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMenuBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_menu, parent, false);
        return new MenuAdapter.MenuViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        MenuAdapter.MenuViewHolder viewHolder = (MenuAdapter.MenuViewHolder) holder;
        viewHolder.itemMenuBinding.imgProd.setImageResource(mPersonalList.get(position).getImage());
        viewHolder.itemMenuBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPersonalList.get(position) != null) {
                    menuIdCallBack.onSelectedMenu(mPersonalList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class MenuViewHolder extends RecyclerView.ViewHolder {
        final ItemMenuBinding itemMenuBinding;

        public MenuViewHolder(@NonNull ItemMenuBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuBinding = itemView;
        }
    }
}
