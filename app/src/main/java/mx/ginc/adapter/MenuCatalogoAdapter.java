package mx.ginc.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Menu;
import mx.ginc.databinding.ItemCatalogoBinding;
import mx.ginc.databinding.ItemMenuPrincipalBinding;
import mx.ginc.databinding.ItemMenuSubitemBinding;

public class MenuCatalogoAdapter extends RecyclerView.Adapter<MenuCatalogoAdapter.MenuCatalogoViewHolder> {

    private List<Menu> mPersonalList;
    public Context mContext;
    public Activity mActivity;

    public MenuCatalogoAdapter(Activity mActivity, Context context, List<Menu> mPersonalList, MenuCatalogoAdapter.MenuPrincipalIdCallBack menuIdCallBack) {
        this.mContext = context;
        this.menuIdCallBack = menuIdCallBack;
        this.mActivity = mActivity;
        this.mPersonalList = mPersonalList;
    }

    public interface MenuPrincipalIdCallBack {
        void onSelectedMenu(Menu menu);
    }

    private MenuCatalogoAdapter.MenuPrincipalIdCallBack menuIdCallBack;

    @NonNull
    @Override
    public MenuCatalogoAdapter.MenuCatalogoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCatalogoBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_catalogo, parent, false);
        return new MenuCatalogoAdapter.MenuCatalogoViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull MenuCatalogoViewHolder holder, int position) {
        MenuCatalogoAdapter.MenuCatalogoViewHolder viewHolder = (MenuCatalogoAdapter.MenuCatalogoViewHolder) holder;
        Picasso.with(mContext).load(mPersonalList.get(position).getImage()).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(viewHolder.itemMenuBinding.imageMenu);
        //viewHolder.itemMenuBinding.image1.setImageResource(mPersonalList.get(position).getImage());
        viewHolder.itemMenuBinding.txtMenuSubItem.setText(mPersonalList.get(position).getName());
        viewHolder.itemMenuBinding.imgLineMenu.setImageResource(mPersonalList.get(position).getFondo());
        viewHolder.itemMenuBinding.txtMenuSubItem.setTextColor(ContextCompat.getColor(this.mContext, mPersonalList.get(position).getColor()));
        viewHolder.itemMenuBinding.crdItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPersonalList.get(position) != null) {
                    menuIdCallBack.onSelectedMenu(mPersonalList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPersonalList != null)
            return mPersonalList.size();
        else return 0;
    }

    public static class MenuCatalogoViewHolder extends RecyclerView.ViewHolder {
        final ItemCatalogoBinding itemMenuBinding;

        public MenuCatalogoViewHolder(@NonNull ItemCatalogoBinding itemView) {
            super(itemView.getRoot());
            this.itemMenuBinding = itemView;
        }
    }
}