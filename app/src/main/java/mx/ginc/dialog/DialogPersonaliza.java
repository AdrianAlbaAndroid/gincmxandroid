package mx.ginc.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.ItemAdapter;
import mx.ginc.adapter.SubItemAdapter;
import mx.ginc.data.entities.Customizer;
import mx.ginc.views.CustomerActivity;

public class DialogPersonaliza extends DialogFragment implements SubItemAdapter.SubItemIdCallBack{

    private RecyclerView recyclerPersonaliza;
    private CustomerActivity customerActivity;
    SubItemAdapter subItemAdapter;

    public DialogPersonaliza(CustomerActivity activity, List<Customizer> mListCustomizer){
        this.customerActivity = activity;
        getRecyclerSubItem(mListCustomizer);
    }

    public static DialogPersonaliza newInstance(CustomerActivity activity, List<Customizer> mListCustomizer) {
        DialogPersonaliza frag = new DialogPersonaliza(activity,mListCustomizer);
        return frag;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_personaliza, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        recyclerPersonaliza = (RecyclerView) view.findViewById(R.id.recyclerPersonaliza);
    }

    public void getRecyclerSubItem(List<Customizer> personalList){
        subItemAdapter = new SubItemAdapter( this.customerActivity,  this.customerActivity, personalList,this, true);
        subItemAdapter.notifyDataSetChanged();
        recyclerPersonaliza.setHasFixedSize(true);
        recyclerPersonaliza.setLayoutManager(new LinearLayoutManager( this.customerActivity, LinearLayoutManager.HORIZONTAL, false));
        recyclerPersonaliza.setItemViewCacheSize(20);
        recyclerPersonaliza.setDrawingCacheEnabled(true);
        recyclerPersonaliza.setAdapter(subItemAdapter);
    }

    @Override
    public void onSelectedSubItem(Customizer subItem) {
        Toast.makeText(this.customerActivity,String.valueOf(subItem.getId()),Toast.LENGTH_SHORT).show();
       /* ProductServer.getInstance().getMenuItemFirst(subItem.getId(), new ProductServer.CustomizerCallBack() {
            @Override
            public void onSuccess(List<Customizer> mListCustomizer) {
                if(mListCustomizer.size() != 0){
                    mListCustomizer.add(new Customizer(1,"Atrás",false, R.drawable.ic_arrow));

                    //gettRecyclerSubItem(mListCustomizer);
                    DialogPersonaliza dialogPersonaliza = new DialogPersonaliza(CustomerActivity.this,mListCustomizer);
                    dialogPersonaliza.show(getSupportFragmentManager(), "");
                }
            }
        });*/
    }
}
