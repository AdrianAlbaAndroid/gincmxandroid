package mx.ginc.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import androidx.annotation.IdRes;
import androidx.navigation.Navigation;

public class DialogAlerta {

    private static DialogAlerta instance = null;
    private Context context;

    public static DialogAlerta init(Context context) {
        if (instance == null)
            instance = new DialogAlerta(context);
        return instance;
    }

    public DialogAlerta(Context context) {
        this.context = context;

    }

    public static void createDialogOK(String title, String message, Context context){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    public void createDialogOK(View view, String title, String message, @IdRes int idRes){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Navigation.findNavController(view).navigate(idRes);
            }
        });
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }
}
