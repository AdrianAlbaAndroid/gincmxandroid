package mx.ginc.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import mx.ginc.data.entities.Belly;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.repository.Repository;

public class MeasurementViewModel extends ViewModel {

    private LiveData<List<Belly>> mFitPreferencesList;
    private LiveData<List<Belly>> mBellyMeasurementList;
    private LiveData<List<Belly>> mChestMeasurementList;
    private LiveData<List<Medidas>> mMisMedidassList;

    public LiveData<List<Belly>> getBellyMeasurement(Context context) {
        if (mBellyMeasurementList == null) {
            mBellyMeasurementList = new MutableLiveData<>();
            getBelly(context);
        }
        return mBellyMeasurementList;
    }

    public LiveData<List<Belly>> getChestMeasurement(Context context) {
        if (mChestMeasurementList == null) {
            mChestMeasurementList = new MutableLiveData<>();
            getChest(context);
        }
        return mChestMeasurementList;
    }

    public LiveData<List<Belly>> getFitPreference(Context context) {
        if (mFitPreferencesList == null) {
            mFitPreferencesList = new MutableLiveData<>();
            getFitPref(context);
        }
        return mFitPreferencesList;
    }

    public LiveData<List<Medidas>> getMisMedidas(Context context) {
        if (mMisMedidassList == null) {
            mMisMedidassList = new MutableLiveData<>();
            getMiMedida(context);
        }
        return mMisMedidassList;
    }

    private void getFitPref(Context context) { mFitPreferencesList = Repository.INSTANCE.getFitPref(context); }
    private void getBelly(Context context) { mBellyMeasurementList = Repository.INSTANCE.getBelly(context); }
    private void getChest(Context context) { mChestMeasurementList = Repository.INSTANCE.getChest(context); }
    private void getMiMedida(Context context) { mMisMedidassList = Repository.INSTANCE.getMiMedida(context); }
}