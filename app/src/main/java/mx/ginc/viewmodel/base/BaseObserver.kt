package mx.ginc.viewmodel.base

import androidx.lifecycle.Observer

interface BaseObserver<T> : Observer<T>