package mx.ginc.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;

import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.repository.Repository;

public class DirectionViewModel extends ViewModel {

    public LiveData<List<DirectionAll>> mDirectionList;

    public LiveData<List<DirectionAll>> getDirectionAll(Context context) {
        if (mDirectionList == null) {
            mDirectionList = new MutableLiveData<>();
            getDirection(context);
        }
        return mDirectionList;
    }

    private void getDirection(Context context) { mDirectionList = Repository.INSTANCE.getDirectionAll(context); }
}
