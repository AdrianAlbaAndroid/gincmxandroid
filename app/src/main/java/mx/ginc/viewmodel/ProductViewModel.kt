package mx.ginc.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import mx.ginc.data.local.CustomizedProduct
import mx.ginc.data.local.ProductSection
import mx.ginc.data.repository.RepositoryLocal

class ProductViewModel(application : Application) : AndroidViewModel(application) {
    val repository = RepositoryLocal(this)

    fun insertProduct(productSection: ProductSection){
        repository.insertProduct(productSection)
    }

    fun insertCustomizedProduct(customizedProduct: CustomizedProduct){
        repository.insertCustomizedProduct(customizedProduct)
    }

    fun updateCustomizedProduct(customizedProduct: CustomizedProduct){
        repository.updateCustomizedProduct(customizedProduct)
    }

    fun updateProduct(productSection: ProductSection){
        repository.updateProduct(productSection)
    }

    fun deleteProduct(id: Int){
        repository.deleteProduct(id)
    }

    fun deleteAll(){
        repository.deleteAll()
    }

    fun deleteCustmizedAll(){
        repository.deleteCustmizedAll()
    }

    fun getAllProducts() = repository.getAll()

    fun getCount() = repository.getCount()

    fun updateCustmizedAll(selected : Boolean){
        repository.updateSelectedAll(selected)
    }

}