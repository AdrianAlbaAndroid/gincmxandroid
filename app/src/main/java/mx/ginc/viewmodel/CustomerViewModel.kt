package mx.ginc.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import mx.ginc.data.repository.RepositoryLocal

class CustomerViewModel(application :  Application) : AndroidViewModel(application) {

    val repository = RepositoryLocal(this)

    fun getCustomizedMenu() = repository.getCustomizedMenu()


}