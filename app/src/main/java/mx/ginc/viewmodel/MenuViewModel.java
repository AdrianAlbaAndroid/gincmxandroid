package mx.ginc.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Menu;

public class MenuViewModel extends ViewModel {

    private MutableLiveData<List<Menu>> mMenuCustomer;
    private MutableLiveData<List<Menu>> mMenu;

    public LiveData<List<Menu>> getMenuCustomer() {
        if (mMenuCustomer == null) {
            mMenuCustomer = new MutableLiveData<>();
            getMenuProduct();
        }
        return mMenuCustomer;
    }

    public LiveData<List<Menu>> getMenu() {
        if (mMenu == null) {
            mMenu = new MutableLiveData<>();
            getMenuPrincipal();
        }
        return mMenu;
    }

    public void getMenuProduct() {
        List<Menu> list = new ArrayList<>();
        list.add(new Menu("https://ginc.smart-develop.mx/assets/images/mobile-app/1.jpg", "CAMISAS", R.drawable.linenombre_blanco, R.color.colorWhite));
        list.add(new Menu("https://ginc.smart-develop.mx/assets/images/mobile-app/2.jpg", "BOXERS", R.drawable.linenombre, R.color.colorBlack));
        list.add(new Menu("https://ginc.smart-develop.mx/assets/images/mobile-app/3.jpg", "ACCESORIOS", R.drawable.linenombre_blanco, R.color.colorWhite));
        mMenuCustomer.setValue(list);
    }

    public void getMenuPrincipal(){
        List<Menu> list = new ArrayList<>();
        list.add(new Menu("https://ginc.smart-develop.mx/assets/images/mobile-app/5.png","PERSONALIZA", R.drawable.linenombre_blanco, R.color.colorWhite));
        list.add(new Menu("https://ginc.smart-develop.mx/assets/images/mobile-app/4.png","CATÁLOGO", R.drawable.linenombre, R.color.colorBlack));
        mMenu.setValue(list);
    }


}