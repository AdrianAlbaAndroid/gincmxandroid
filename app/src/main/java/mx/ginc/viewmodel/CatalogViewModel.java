package mx.ginc.viewmodel;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;

import java.util.List;

import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.Especificaciones;
import mx.ginc.data.entities.FinalPrice;
import mx.ginc.data.entities.ParentPedido;
import mx.ginc.data.entities.Pedido;
import mx.ginc.data.entities.Product;
import mx.ginc.data.repository.Repository;

public class CatalogViewModel extends ViewModel {

    private LiveData<List<Customizer>> mCatalogoList;
    private LiveData<List<Product>> mSubCatalogoList;
    private LiveData<List<String>> mImageCatalogList;
    private LiveData<List<ParentPedido>> mMisPedidosList;
    private LiveData<List<Customizer>> mCategoriasList;
    private LiveData<List<Especificaciones>> mEspecificacionesList;

    public LiveData<List<Customizer>> getCatalog(int id) {
        if (mCatalogoList == null) {
            mCatalogoList = new MutableLiveData<>();
            getClothsList(id);
        }
        return mCatalogoList;
    }

    public LiveData<List<Product>> getSubCatalog(JsonObject jsonObject) {
        if (mSubCatalogoList == null) {
            mSubCatalogoList = new MutableLiveData<>();
            getSubCatalogList(jsonObject);
        }
        return mSubCatalogoList;
    }

    public LiveData<List<String>> getImageCatalog(int id) {
        if (mImageCatalogList == null) {
            mImageCatalogList = new MutableLiveData<>();
            getImagesCatalog(id);
        }
        return mImageCatalogList;
    }

    public LiveData<List<ParentPedido>> getMiPedido(Context context, JsonObject jsonObject) {
        if (mMisPedidosList == null) {
            mMisPedidosList = new MutableLiveData<>();
            getMisPedidos(context, jsonObject);
        }
        return mMisPedidosList;
    }

    public LiveData<List<Customizer>> getCategorias() {
        if (mCategoriasList == null) {
            mCategoriasList = new MutableLiveData<>();
            getCategoriasList();
        }
        return mCategoriasList;
    }

    public LiveData<List<Especificaciones>> getEspecificaciones(int idProducto) {
        if (mEspecificacionesList == null) {
            mEspecificacionesList = new MutableLiveData<>();
            getEspecificacionesList(idProducto);
        }
        return mEspecificacionesList;
    }





    private void getClothsList(int id){
        mCatalogoList = Repository.INSTANCE.getCatalog(id);
    }

    public void getSubCatalogList(JsonObject jsonObject) { mSubCatalogoList = Repository.INSTANCE.getSubCatalog(jsonObject); }

    public void getImagesCatalog(int id) { mImageCatalogList = Repository.INSTANCE.getImageCatalog(id); }

    public void getMisPedidos(Context context, JsonObject jsonObject) { mMisPedidosList = Repository.INSTANCE.getMisPedidos(jsonObject, context); }

    public void getCategoriasList() { mCategoriasList = Repository.INSTANCE.getCategorias(); }

    public void getEspecificacionesList(int idProducto) { mEspecificacionesList = Repository.INSTANCE.getEspecificaciones(idProducto); }

}