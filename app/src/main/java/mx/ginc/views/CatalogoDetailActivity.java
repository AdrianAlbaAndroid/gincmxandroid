package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import mx.ginc.R;
import mx.ginc.adapter.AtributoAdapter;
import mx.ginc.adapter.CatalogImageAdapter;
import mx.ginc.adapter.ShirtAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Especificaciones;
import mx.ginc.data.entities.FinalPrice;
import mx.ginc.data.local.ProductSection;
import mx.ginc.data.network.ProductServer;
import mx.ginc.data.repository.RepositoryLocal;
import mx.ginc.databinding.ActivityDetailProductBinding;
import mx.ginc.viewmodel.CatalogViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.fragments.GuiaTallasFragment;

public class CatalogoDetailActivity extends AppCompatActivity implements BaseObserver<List<String>>{

    private ActivityDetailProductBinding mBinding;
    private String name = "";
    private int id = 0;
    private int idCategoria = 0;
    private String imgUrl = "";
    String selectedSize = "";
    private String description = "";
    private String objetoProducto = "";
    private double costoFinal = 0;
    private ProductViewModel mProductBuyViewModel  = null;
    private CatalogImageAdapter adapter;
    private AtributoAdapter atributoAdapter;
    private CatalogViewModel mCatalogViewModel = null;
    private List<Especificaciones> mAtributos;
    private String nombreAtributo = "";
    private List<Especificaciones> mEspecificacionesSelected;
    private String selected = "";
    private List<String> mEspecificacion;
    private List<String> mSelected;
    private JsonArray mArrayAtributo = null;
    private String cadenaAtributos = "";
    private int cantidad = 0;
    private boolean existeProducto = false;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail_product);
        idCategoria = getIntent().getIntExtra("idCategoria", 0);
        id = getIntent().getIntExtra("id", 0);
        name = getIntent().getStringExtra("name");
        description = getIntent().getStringExtra("description");
        objetoProducto =  getIntent().getStringExtra("objetoProducto");
        initToolbar();
        initView();
    }

    public void initView(){
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        pref = new AppPreferenceManager(CatalogoDetailActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(CatalogoDetailActivity.this).get(ProductViewModel.class);
                mCatalogViewModel = ViewModelProviders.of(CatalogoDetailActivity.this).get(CatalogViewModel.class);
            }
        });
        ProductServer.getInstance().getFinalPrice(id, new ProductServer.PriceCallBack() {
            @Override
            public void onSuccess(FinalPrice finalPrice) {
                costoFinal = finalPrice.getCostoFinal();
                mBinding.txtPrecio.setText(AppUtil.customFormat("$ ###,###.00", finalPrice.getCostoFinal()));
            }

            @Override
            public void onFailure(Double price) {
                costoFinal = 0.0;
                mBinding.txtPrecio. setText("$ 0.00");
            }
        });
        mCatalogViewModel.getImageCatalog(id).observe(this,this);
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        if((idCategoria == 1) | (idCategoria == 7)){
            mBinding.algodon.setVisibility(View.VISIBLE);
            mBinding.linea.setVisibility(View.VISIBLE);
            mBinding.GuiaTallas.setVisibility(View.VISIBLE);
        }else if (idCategoria == 8) {
            mBinding.algodon.setVisibility(View.GONE);
            mBinding.linea.setVisibility(View.GONE);
            mBinding.GuiaTallas.setVisibility(View.GONE);
        }
        mCatalogViewModel.getEspecificaciones(id).observe(this, new BaseObserver<List<Especificaciones>>() {
            @Override
            public void onChanged(List<Especificaciones> especificaciones) {
                mAtributos = new ArrayList();
                mEspecificacionesSelected = especificaciones;
                if(especificaciones.size() != 0){
                    mBinding.GuiaTallas.setVisibility(View.VISIBLE);
                    mBinding.linea.setVisibility(View.VISIBLE);
                    Map<String, List<Especificaciones>> valores = getValores(especificaciones);
                    for(Map.Entry m:valores.entrySet()){
                        mAtributos.add(setEspecificaciones(m.getKey().toString(),m.getValue().toString()));
                    }
                    getEspecificaciones(mAtributos,especificaciones);
                }else{
                    mBinding.GuiaTallas.setVisibility(View.INVISIBLE);
                    mBinding.linea.setVisibility(View.INVISIBLE);
                }
            }
        });

        mBinding.titlePrincipal.setText(name);
        mBinding.txtSubPrincipal.setText(description);
        mBinding.btnAddProduct.setOnClickListener(new View.OnClickListener() {
            List<JsonObject> jsonAtributo = new ArrayList<>();
            boolean existe = false;
            @Override
            public void onClick(View v) {
                if(mAtributos.size() != 0) {
                    for (int i = 0; i < mAtributos.size(); i++) {
                        String seleccion = getAtributoSeleccionado(i);
                        existe = (seleccion.contains("Seleccione")) ? true : false;
                    }

                    if(existe){
                        Toast.makeText(CatalogoDetailActivity.this, "Debe seleccionar todos las opciones", Toast.LENGTH_LONG).show();
                    }else{
                        cadenaAtributos = "";
                        for (int i = 0; i < mAtributos.size(); i++) {
                            View row = mBinding.recyclerAtributos.getLayoutManager().findViewByPosition(i);
                            TextView textView = row.findViewById(R.id.txtSeleccionaTalla);
                            String seleccion = textView.getText().toString();
                            for(Especificaciones atributo : mEspecificacionesSelected){
                                if(atributo.getValor().equals(seleccion)){
                                    cadenaAtributos = cadenaAtributos + "\n" + atributo.getEtiqueta() + " : " + atributo.getValor();
                                    JsonObject jsonObject = new JsonParser().parse(atributo.getJsonObject()).getAsJsonObject();
                                    jsonAtributo.add(jsonObject);
                                }
                            }
                        }
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        mArrayAtributo = gson.toJsonTree(jsonAtributo).getAsJsonArray();
                        mProductBuyViewModel.getRepository().getProductsAll(new RepositoryLocal.ProductsAllCallBack() {
                            @Override
                            public void setProducts(@NonNull List<ProductSection> products) {
                                if(products.size() != 0){
                                    cantidad = 0;
                                    for(int i = 0; i < products.size(); i++){
                                         if(products.get(i).getTipo().equals(AppUtil.TipoProducto.CATALOGO)){
                                            if(products.get(i).getIdProducto() == id) {
                                                cantidad = products.get(i).getAmount() + 1;
                                                existeProducto = true;
                                                updateProduct(products.get(i), cantidad);
                                            }
                                        }

                                    }
                                }else{
                                    existeProducto = false;
                                    cantidad = 1;
                                }
                            }
                        });
                        if(existeProducto == false){
                            saveProduct();
                        }
                        Intent intent = new Intent(CatalogoDetailActivity.this, ShoppingCartActivity.class);
                        startActivity(intent);
                    }
                }else{

                    Intent intent = new Intent(CatalogoDetailActivity.this, ShoppingCartActivity.class);
                    startActivity(intent);
                }
            }
        });

        mBinding.btnCompartir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/jpg");
                final File photoFile = new File(getFilesDir(), "foo.jpg");
                shareIntent.putExtra(Intent.EXTRA_STREAM, imgUrl);
                startActivity(Intent.createChooser(shareIntent, "Share image using"));
            }
        });
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CatalogoDetailActivity.this, ShoppingCartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        mBinding.GuiaTallas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuiaTallasFragment dialogFragment = new GuiaTallasFragment();
                dialogFragment.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    private void saveProduct(){
        ProductSection product = new ProductSection();
        product.setIdCart(null);
        product.setIdProducto(id);
        product.setDescription(name);
        product.setTipo(AppUtil.TipoProducto.CATALOGO);
        product.setSexo("");
        product.setPrice_base(costoFinal);
        product.setPrice_final(costoFinal);
        product.setAmount(1);
        product.setIdCategoria(idCategoria);
        product.setSize(cadenaAtributos);
        product.setImgUrl(imgUrl);
        product.setObjProduct(objetoProducto);
        product.setObjEspecificaciones(mArrayAtributo.toString());
        product.setImgCustomizer("");
        mProductBuyViewModel.insertProduct(product);
    }

    private void updateProduct(ProductSection product, int amount){
        ProductSection dbProduct = new ProductSection();
        dbProduct.setIdCart(product.getIdCart());
        dbProduct.setAmount(amount);
        dbProduct.setIdProducto(product.getIdProducto());
        dbProduct.setPrice_base(product.getPrice_base());
        dbProduct.setPrice_final(product.getPrice_base() * amount);
        dbProduct.setSize(product.getSize());
        dbProduct.setTipo(product.getTipo());
        dbProduct.setSexo(product.getSexo());
        dbProduct.setIdCategoria(product.getIdCategoria());
        dbProduct.setImgUrl(product.getImgUrl());
        dbProduct.setDescription(product.getDescription());
        dbProduct.setObjProduct(product.getObjProduct());
        dbProduct.setObjEspecificaciones(product.getObjEspecificaciones());
        dbProduct.setImgCustomizer(product.getImgCustomizer());
        mProductBuyViewModel.updateProduct(dbProduct);
    }

    private String getAtributoSeleccionado(int i){
        View row = mBinding.recyclerAtributos.getLayoutManager().findViewByPosition(i);
        TextView textView = row.findViewById(R.id.txtSeleccionaTalla);
        String seleccion = textView.getText().toString();
        return seleccion;
    }

    public Map<String, List<Especificaciones>> getValores(List<Especificaciones> especificacionesAll){
        return especificacionesAll.stream()
                .collect(Collectors.groupingBy(Especificaciones::getEtiqueta));
    }

    private Especificaciones setEspecificaciones(String etiqueta, String jsonObject){
        return new Especificaciones(0,"", etiqueta,jsonObject);
    }

    public void getImageDetail(List<String> mImageCatalog){
        adapter = new CatalogImageAdapter(this, this, mImageCatalog);
        mBinding.recyclerCatalogDetail.setHasFixedSize(true);
        mBinding.recyclerCatalogDetail.setLayoutManager(new LinearLayoutManager(CatalogoDetailActivity.this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerCatalogDetail.setItemViewCacheSize(20);
        mBinding.recyclerCatalogDetail.setDrawingCacheEnabled(true);
        mBinding.recyclerCatalogDetail.setAdapter(adapter);
    }

    public void getEspecificaciones(List<Especificaciones> especificacionList, List<Especificaciones> atributosLista) {
        atributoAdapter = new AtributoAdapter(this, this, especificacionList, atributosLista);
        mBinding.recyclerAtributos.setHasFixedSize(true);
        mBinding.recyclerAtributos.setLayoutManager(new LinearLayoutManager(CatalogoDetailActivity.this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerAtributos.setItemViewCacheSize(20);
        mBinding.recyclerAtributos.setDrawingCacheEnabled(true);
        mBinding.recyclerAtributos.setAdapter(atributoAdapter);
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onChanged(List<String> strings) {
        if(strings.size() == 0){
            mBinding.recyclerCatalogDetail.setVisibility(View.GONE);
        }else{
            Picasso.with(CatalogoDetailActivity.this).load(strings.get(0)).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgDetail);
            imgUrl = strings.get(0);
            strings.remove(0);
            mBinding.recyclerCatalogDetail.setVisibility(View.VISIBLE);
            getImageDetail(strings);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

}