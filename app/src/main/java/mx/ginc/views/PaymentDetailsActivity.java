package mx.ginc.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import org.json.JSONException;
import org.json.JSONObject;

import mx.ginc.R;
import mx.ginc.databinding.ActivityPaymentDetailsBinding;

public class PaymentDetailsActivity  extends AppCompatActivity  {

    private ActivityPaymentDetailsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment_details);
        Intent intent = getIntent();

        try{
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonObject.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        }catch (JSONException e){

        }
    }

    private void showDetails(JSONObject response, String paymentAmount){
        try{
            mBinding.txtId.setText(response.getString("id"));
            mBinding.txtAmount.setText(response.getString(String.format("$", paymentAmount)));
            mBinding.txtStatus.setText(response.getString("state"));
        }catch (JSONException e){

        }
    }

}
