package mx.ginc.views;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.ShirtAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Customizer;
import mx.ginc.databinding.ActivityCatalogoListBinding;
import mx.ginc.viewmodel.CatalogViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class CatalogActivity extends AppCompatActivity implements ShirtAdapter.ShirtIdCallBack, BaseObserver<List<Customizer>> {

    private ShirtAdapter adapter;
    private ActivityCatalogoListBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private CatalogViewModel mCatalogViewModel = null;
    private ProductViewModel mProductBuyViewModel  = null;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_catalogo_list);
        pref = new AppPreferenceManager(CatalogActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCatalogViewModel = ViewModelProviders.of(CatalogActivity.this).get(CatalogViewModel.class);
                mProductBuyViewModel = ViewModelProviders.of(CatalogActivity.this).get(ProductViewModel.class);
            }
        });
        mCatalogViewModel.getCategorias().observe(this, this);
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CatalogActivity.this, ShoppingCartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void getCatalog(List<Customizer> mShirtList){
        adapter = new ShirtAdapter(this, this, mShirtList,this);
        mBinding.recyclerview.setHasFixedSize(true);
        mBinding.recyclerview.setLayoutManager(new GridLayoutManager(this,2));
        mBinding.recyclerview.setItemViewCacheSize(20);
        mBinding.recyclerview.setDrawingCacheEnabled(true);
        mBinding.recyclerview.setAdapter(adapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onChanged(List<Customizer> customizers) {
        getCatalog(customizers);
    }

    @Override
    public void onSelected(Customizer customizer) {
        Intent intent = new Intent(this, SubCatalogoActivity.class);
        intent.putExtra("idCategoria", 1);
        intent.putExtra("id", customizer.getId());
        startActivity(intent);
    }
}