package mx.ginc.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.databinding.ActivityCatalogMenuBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.mymeasurements.MisMedidasActivity;

public class MenuCatalogoActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActivityCatalogMenuBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog_menu);
        initView();
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_catalog_menu);
        pref = new AppPreferenceManager(MenuCatalogoActivity.this);
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuCatalogoActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });
        initToolbar();
        onMenuClick();
    }

    public void onMenuClick(){
        getImageMenu();
        mBinding.imgCamisas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtil.isOnline(MenuCatalogoActivity.this)) {
                    DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), MenuCatalogoActivity.this);
                }else{
                    Intent intent = new Intent(MenuCatalogoActivity.this, CatalogActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
        mBinding.imgBoxers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtil.isOnline(MenuCatalogoActivity.this)) {
                    DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), MenuCatalogoActivity.this);
                }else{
                    Intent intent = new Intent(MenuCatalogoActivity.this, SubCatalogoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("idCategoria", 7);
                    startActivity(intent);
                }
            }
        });
        mBinding.imgAccesorios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtil.isOnline(MenuCatalogoActivity.this)) {
                    DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), MenuCatalogoActivity.this);
                }else{
                    Intent intent = new Intent(MenuCatalogoActivity.this, SubCatalogoActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("idCategoria", 8);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void getImageMenu(){
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/1.jpg").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgCamisas);
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/2.jpg").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgBoxers);
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/3.jpg").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgAccesorios);
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_menu);
        getNavigationDrawer();
    }

    public void getNavigationDrawer() {
        getHeaderView(pref.getFullName());
        mToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawerLayout.addDrawerListener(mToggle);
        mBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (!AppUtil.isOnline(MenuCatalogoActivity.this)) {
            DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), MenuCatalogoActivity.this);
        }else {
            switch (menuItem.getItemId()) {
                case R.id.nav_inicio:
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.nav_pedidos:
                    setMenu(R.id.nav_pedidos);
                    break;
                case R.id.nav_medidas:
                    Intent intentMedidas = new Intent(this, MisMedidasActivity.class);
                    intentMedidas.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentMedidas);
                    break;
                case R.id.nav_funciona:
                    setMenu(R.id.nav_funciona);
                    break;
                case R.id.nav_medirme:
                    setMenu(R.id.nav_medirme);
                    break;
                case R.id.nav_cuenta:
                    setMenu(R.id.nav_cuenta);
                    break;
                case R.id.nav_terminos:
                    setMenu(R.id.nav_terminos);
                    break;
                case R.id.nav_contacto:
                    setMenu(R.id.nav_contacto);
                    break;
                case R.id.nav_politicas_privacidad:
                    setMenu(R.id.nav_politicas_privacidad);
                    break;
                case R.id.nav_sesion:
                    Intent intentHome = new Intent(this, HomeActivity.class);
                    intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    pref.removeToken();
                    pref.removeFullName();
                    getHeaderView(pref.getFullName());
                    finish();
                    break;
            }
        }
        mBinding.drawerLayout.closeDrawers();
        return true;
    }

    private void setMenu(int option){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("opcion", option);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}