package mx.ginc.views.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import mx.ginc.R
import mx.ginc.data.entities.DirectionAll
import mx.ginc.data.entities.UserDetail
import mx.ginc.data.network.UserServer
import mx.ginc.databinding.FragmentCuentaBinding
import mx.ginc.viewmodel.DirectionViewModel
import mx.ginc.viewmodel.base.BaseObserver
import mx.ginc.views.DirectionActivity
import mx.ginc.views.DirectionRegisterActivity
import mx.ginc.views.LoginActivity
import org.json.JSONObject
import java.util.stream.Collectors

class CuentaFragment : Fragment() {

    private var mBinding: FragmentCuentaBinding? = null
    private var mDirectionViewModel: DirectionViewModel? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentCuentaBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVIew()
    }

    override fun onResume() {
        super.onResume()
        initVIew()
    }

    private fun initVIew() {
        mDirectionViewModel = activity.run { ViewModelProviders.of(activity!!)[DirectionViewModel::class.java] }
        mBinding!!.btnlistadoDirecciones.setOnClickListener{
            val intent = Intent(activity, DirectionActivity::class.java)
            startActivity(intent)
        }
        mBinding!!.btnAddDirection.setOnClickListener{
            val intent = Intent(activity, DirectionRegisterActivity::class.java)
            startActivity(intent)
        }
        getUser()

    }

    fun getUser() {
        UserServer.getInstance(context).getUser(object : UserServer.UserDetailCallBack {
            override fun onSuccess(userDetail: UserDetail?) {
                var nombrePaterno = if (userDetail!!.aPaterno != null) userDetail!!.aPaterno else ""
                var nombreMaterno = if (userDetail!!.aMaterno != null) userDetail!!.aMaterno else ""
                mBinding!!.nombreCuenta.text = userDetail!!.name + " " +  nombrePaterno + " " + nombreMaterno
                mBinding!!.CorreoUsuario.text = userDetail.email
                mBinding!!.FechaAlta.text = userDetail.fechaAlta
                mBinding!!.Usuario.text = userDetail.usuario
                getDirection()
            }

            override fun onFailure(jsonObject: JSONObject?) {
                val http: Int = jsonObject!!.getInt("http")
                val mensaje: String = jsonObject!!.getString("mensaje")
                if (http == 401 || mensaje == "Error al validar el token") {
                    val intent = Intent(context, LoginActivity::class.java)
                    intent.putExtra("activity","CuentaFragment");
                    startActivity(intent)
                } else {
                    createDialog("Alerta", mensaje)
                }

            }
        })
    }

    fun getDirection() {
        mDirectionViewModel!!.getDirectionAll(context).observe(this, object : BaseObserver<List<DirectionAll?>?> {
            override fun onChanged(t: List<DirectionAll?>?) {
                mDirectionViewModel!!.getDirectionAll(context).removeObserver(this)
                if(t!!.size != 0){
                    val directionAll: MutableList<DirectionAll?>? = getDirectionPrincipal(t!!)
                    if (directionAll!!.size != 0) {
                        for (k in directionAll.indices) {
                            mBinding!!.Nombre.text = directionAll[k]!!.quienRecibe
                            mBinding!!.Direccion.text = directionAll[k]!!.callePrincipal + ", " +
                                    directionAll[k]!!.numeroExterior + ", " +
                                    directionAll[k]!!.numeroInteriorDepto + ", " +
                                    directionAll[k]!!.municipio + ", " +
                                    directionAll[k]!!.colonia + ", " +
                                    directionAll[k]!!.codigoPostal + ", " +
                                    directionAll[k]!!.entidad
                        }
                    } else {
                     var direction = t!!.stream().findFirst().get()
                        mBinding!!.Nombre.text = direction!!.quienRecibe
                        mBinding!!.Direccion.text = direction!!.callePrincipal + ", " +
                                direction!!.numeroExterior + ", " +
                                direction!!.numeroInteriorDepto + ", " +
                                direction!!.municipio + ", " +
                                direction!!.colonia + ", " +
                                direction!!.codigoPostal + ", " +
                                direction!!.entidad
                    }
                }else{
                    mBinding!!.PredeterinadoSelect.text = "Para terminar su compra es necesario agregar una dirección"
                    mBinding!!.PredeterinadoSelect.setTextColor(Color.RED)
                    mBinding!!.Nombre.visibility = View.GONE
                    mBinding!!.Direccion.visibility = View.GONE
                }

            }
        })
    }

    fun getDirectionPrincipal(directionAlls: List<DirectionAll?>): MutableList<DirectionAll?>? {
        return directionAlls.stream()
                .filter { it!!.principal == true }
                .collect(Collectors.toList())
    }

    private fun createDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK", null)
        builder.create()
        builder.show()
    }
}