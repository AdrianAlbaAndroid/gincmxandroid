package mx.ginc.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.repository.RepositoryLocal;
import mx.ginc.databinding.FragmentDetailCustomizerBinding;
import mx.ginc.databinding.FragmentVerMedidasBinding;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.AddCartDesignActivity;
import mx.ginc.views.CustomerActivity;

public class DetailCustomizerFragment extends DialogFragment {

    private FragmentDetailCustomizerBinding mBinding;
    private MeasurementViewModel measurementViewModel = null;
    private Context context;
    private AddCartDesignActivity addCartActivity;
    private ProductViewModel mProductBuyViewModel  = null;
    private String cdnCustomized = "";
    private String tipoManga = "";

    public DetailCustomizerFragment(Context context, String tipoManga) {
        this.context = context;
        this.tipoManga = tipoManga;
    }

    public static DetailCustomizerFragment newInstance(Context context, String tipoManga){
        DetailCustomizerFragment fragment = new DetailCustomizerFragment(context, tipoManga);
        return fragment;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mBinding = FragmentDetailCustomizerBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addCartActivity = (AddCartDesignActivity) getActivity();
        addCartActivity.runOnUiThread(() -> mProductBuyViewModel = ViewModelProviders.of(this).get(ProductViewModel.class));
        getCustomizedAll();
        mBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void getCustomizedAll(){
        mProductBuyViewModel.getRepository().getCustomizedAll(new RepositoryLocal.CustomizedAllCallBack() {
            @Override
            public void setCustomized(@NonNull List<CustomizedProduct> products) {
                for(CustomizedProduct producto : sortedListMenu(products, tipoManga)){
                    cdnCustomized = cdnCustomized + "\n\n" + producto.getItemSelected();
                }

                mBinding.txtCustomizer.setText(cdnCustomized);
            }
        });
    }

    public List<CustomizedProduct> sortedListMenu(List<CustomizedProduct>  customizedMenu, String tipoManga){
        return customizedMenu.stream()
                .filter(c -> c.getTipoManga().equals(tipoManga))
                .sorted(Comparator.comparingInt(CustomizedProduct::getIdProduct))
                .collect(Collectors.toList());
    }
}