package mx.ginc.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;

import mx.ginc.data.entities.User;
import mx.ginc.data.network.UserServer;
import mx.ginc.databinding.FragmentOlvideContrasenaBinding;

public class OlvidoContrasenaFragment extends DialogFragment {

    private FragmentOlvideContrasenaBinding mBinding;
    private Context context;

    public OlvidoContrasenaFragment(Context context) {
        this.context = context;
    }

    public static OlvidoContrasenaFragment newInstance(Context context){
        OlvidoContrasenaFragment fragment = new OlvidoContrasenaFragment(context);
        return fragment;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mBinding = FragmentOlvideContrasenaBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mBinding.btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = String.valueOf(mBinding.edtCorreo.getText());
                if(texto.isEmpty()){
                    Toast.makeText(context,"Debe de escribir el correo electrónico",Toast.LENGTH_SHORT).show();
                }else{
                    UserServer.getInstance(context).setUserForgotten(setForgottenEmail(texto), new UserServer.UserCallBack() {
                        @Override
                        public void onSuccess(User user) {
                            Toast.makeText(context,"Se ha enviado un correo con una contraseña temporal",Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(String mensaje) {

                        }
                    });
                }
            }
        });
    }

    private JsonObject setForgottenEmail(String email){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("email", email);
        return requestBody;
    }
}