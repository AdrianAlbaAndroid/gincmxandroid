package mx.ginc.views.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import mx.ginc.databinding.FragmentMiCosturaBinding
import mx.ginc.databinding.FragmentMisMedidasBinding

class MisMedidasFragment(context: Context) : Fragment() {

    private var mBinding: FragmentMisMedidasBinding? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentMisMedidasBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVIew()
    }

    override fun onResume() {
        super.onResume()
        initVIew()
    }

    private fun initVIew() {

    }
}