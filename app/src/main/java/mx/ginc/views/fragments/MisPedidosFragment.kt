package mx.ginc.views.fragments

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import mx.ginc.adapter.ChildPedidoAdapter
import mx.ginc.adapter.MisPedidosAdapter
import mx.ginc.adapter.ParentPedidoAdapter
import mx.ginc.data.entities.ParentPedido
import mx.ginc.data.entities.Pedido
import mx.ginc.data.local.ProductSection
import mx.ginc.databinding.FragmentMisPedidosBinding
import mx.ginc.viewmodel.CatalogViewModel
import mx.ginc.viewmodel.base.BaseObserver
import mx.ginc.views.MenuActivity
import mx.ginc.views.PurchaseDetailActivity
import mx.ginc.views.SubCatalogoActivity
import mx.ginc.views.mymeasurements.MisMedidasActivity

class MisPedidosFragment(context: Context, activity: Activity) : Fragment(), BaseObserver<MutableList<ParentPedido>>, ChildPedidoAdapter.PedidoCallBack {

    private var mBinding: FragmentMisPedidosBinding? = null
    var mMisPedidosAdapter: ParentPedidoAdapter? = null
    private var mMisPedidosViewModel: CatalogViewModel? = null
    private lateinit var mContext : Context

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentMisPedidosBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVIew()
    }

    override fun onResume() {
        super.onResume()
        initVIew()
    }

    override fun onStart() {
        super.onStart()
        initVIew()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context as MenuActivity
    }


    private fun initVIew(){
        mMisPedidosViewModel = activity.run { ViewModelProviders.of(activity!!)[CatalogViewModel::class.java] }
        mMisPedidosViewModel!!.getMiPedido(context,setMisPedidosObject()).observe(viewLifecycleOwner, this)
    }

    fun getMisPedidos(pedidoList: List<ParentPedido?>?) {
        mBinding!!.recyclerviewPedidos.setAdapter(null)
        mMisPedidosAdapter = ParentPedidoAdapter(pedidoList as List<ParentPedido>?, mContext, this)
        mBinding!!.recyclerviewPedidos.setHasFixedSize(true)
        mBinding!!.recyclerviewPedidos.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
        mBinding!!.recyclerviewPedidos.setItemViewCacheSize(20)
        mBinding!!.recyclerviewPedidos.setDrawingCacheEnabled(true)
        mBinding!!.recyclerviewPedidos.setAdapter(mMisPedidosAdapter)
    }

    private fun setMisPedidosObject(): JsonObject? {
        val requestBody = JsonObject()
        requestBody.addProperty("draw", 1)
        requestBody.addProperty("length", 10)
        requestBody.addProperty("start", 0)
        requestBody.addProperty("invoiceNumber", "")
        return requestBody
    }

    override fun onSelectedItem(pedido: Pedido?) {
        val intent = Intent(activity, PurchaseDetailActivity::class.java)
        intent.putExtra("image", pedido!!.image)
        intent.putExtra("etiqueta", pedido!!.etiqueta)
        intent.putExtra("costo", pedido!!.costo)
        intent.putExtra("fechaCompra", pedido!!.fechaCompra)
        intent.putExtra("invoiceNumber", pedido!!.invoiceNumber)
        intent.putExtra("esp", pedido!!.especificaciones)
        intent.putExtra("tipo", pedido!!.tipo)
        startActivity(intent)
    }

    override fun onChanged(t: MutableList<ParentPedido>?) {
        if(t!!.size != 0){
            getMisPedidos(t)
            mMisPedidosViewModel!!.getMiPedido(context,setMisPedidosObject()).removeObserver(this);
        }
    }
}