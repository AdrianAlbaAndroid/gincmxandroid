package mx.ginc.views.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import mx.ginc.R
import mx.ginc.data.network.UserServer
import mx.ginc.databinding.FragmentComoMedirmeBinding
import mx.ginc.databinding.FragmentContactoBinding

class ContactoFragment : Fragment() {

    private var mBinding: FragmentContactoBinding? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentContactoBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVIew()
    }

    override fun onResume() {
        super.onResume()
        initVIew()
    }

    private fun initVIew(){
        mBinding!!.btnAddProduct.setOnClickListener(View.OnClickListener {
            var textArea = mBinding!!.textAreaInformation.text.toString();
            if(!textArea.toString().equals("")){
                UserServer.getInstance(context).setMessageContact(setMessageObject(textArea), object: UserServer.MessageCallBack{
                    override fun onSuccess(mensaje: String?) {
                        if(mensaje.equals("Mensaje enviado correctamente")){
                            createDialog("Alerta", "Mensaje enviado correctamente")
                            mBinding!!.textAreaInformation.text.clear()
                        }
                    }
                })
            }else{
                createDialog("Alerta", "Debe de escribir el mensaje que se necesite enviar")
            }

        })

    }

    private fun setMessageObject(textArea: String): JsonObject? {
        val requestBody = JsonObject()
        requestBody.addProperty("id", 1)
        requestBody.addProperty("mensaje", textArea)
        return requestBody
    }

    private fun createDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setMessage(message)
        builder.setPositiveButton("OK", null)
        builder.create()
        builder.show()
    }
}