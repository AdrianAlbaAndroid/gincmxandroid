package mx.ginc.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.data.entities.Medidas;
import mx.ginc.databinding.FragmentVerMedidasBinding;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.CustomerActivity;

public class MedidasCustomizerFragment extends DialogFragment implements BaseObserver<List<Medidas>> {

    private FragmentVerMedidasBinding mBinding;
    private MeasurementViewModel measurementViewModel = null;
    private Context context;
    private CustomerActivity customerActivity;

    public MedidasCustomizerFragment(Context context) {
        this.context = context;
    }

    public static MedidasCustomizerFragment newInstance(Context context){
        MedidasCustomizerFragment fragment = new MedidasCustomizerFragment(context);
        return fragment;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mBinding = FragmentVerMedidasBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerActivity = (CustomerActivity) getActivity();
        customerActivity.runOnUiThread(() -> measurementViewModel = ViewModelProviders.of(this).get(MeasurementViewModel.class));
        measurementViewModel.getMisMedidas(context).observe(this,this);
        mBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onChanged(List<Medidas> medidas) {
        if(getMedidasPrincipal(medidas).size() != 0) {
            medidas.stream()
                    .filter(c -> c.getPrincipal() == true)
                    .forEach((p)-> {
                        mBinding.Altura.setText(p.getAltura());
                        mBinding.Cuello.setText(p.getCuello());
                        mBinding.Edad.setText(p.getEdad());
                        mBinding.LargoManga.setText(p.getLargoManga());
                        mBinding.Pecho.setText(p.getPecho());
                        mBinding.Peso.setText(p.getPeso());
                        mBinding.TipoCamisa.setText(p.getTipoCamisaMedida());
                        mBinding.TipoPecho.setText(p.getTipoPecho());
                        mBinding.Vientre.setText(p.getTipoVientre());
                    });
        }else{
            mBinding.Altura.setText(medidas.get(0).getAltura());
            mBinding.Cuello.setText(medidas.get(0).getCuello());
            mBinding.Edad.setText(medidas.get(0).getEdad());
            mBinding.LargoManga.setText(medidas.get(0).getLargoManga());
            mBinding.Pecho.setText(medidas.get(0).getPecho());
            mBinding.Peso.setText(medidas.get(0).getPeso());
            mBinding.TipoCamisa.setText(medidas.get(0).getTipoCamisaMedida());
            mBinding.TipoPecho.setText(medidas.get(0).getTipoPecho());
            mBinding.Vientre.setText(medidas.get(0).getTipoVientre());
        }
        measurementViewModel.getMisMedidas(context).removeObservers(this);
    }

    public List<Medidas> getMedidasPrincipal(List<Medidas> medidas){
        return  medidas.stream()
                .filter(c -> c.getPrincipal() == true)
                .collect(Collectors.toList());
    }
}