package mx.ginc.views.fragments

import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.github.barteksc.pdfviewer.PDFView
import mx.ginc.R
import mx.ginc.data.AppUtil
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class ComoFuncionaFragment : Fragment() {

    private lateinit var pdfView: PDFView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_terminos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pdfView = view.findViewById(R.id.pdf_viewer)
        RetrivePDFfromUrl(pdfView).execute(AppUtil.Urls.URL_COMO_FUNCIONA)

    }
    // create an async task class for loading pdf file from URL.
    class  RetrivePDFfromUrl(var pdfView: PDFView) : AsyncTask<String?, Void?, InputStream?>() {
        override fun onPostExecute(inputStream: InputStream?) {
            pdfView.fromStream(inputStream)
                    .password(null)
                    .defaultPage(0)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .enableAnnotationRendering(true)
                    .invalidPageColor(Color.WHITE)
                    .load()
        }

        override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0].toString())
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }

}
