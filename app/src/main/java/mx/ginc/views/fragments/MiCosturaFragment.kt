package mx.ginc.views.fragments

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import mx.ginc.data.entities.UserRegister
import mx.ginc.data.network.UserServer
import mx.ginc.databinding.FragmentCuentaBinding
import mx.ginc.databinding.FragmentMiCosturaBinding
import mx.ginc.views.LoginActivity

class MiCosturaFragment(context: Context) : Fragment() {

    private var mBinding: FragmentMiCosturaBinding? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentMiCosturaBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initVIew()
    }

    override fun onResume() {
        super.onResume()
        initVIew()
    }

    private fun initVIew(){

    }

}