package mx.ginc.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.adapter.ItemAdapter;
import mx.ginc.adapter.SubItemAdapter;
import mx.ginc.adapter.TelasAdapter;
import mx.ginc.data.AppUtil;
import mx.ginc.data.LoadingProgress;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.Medidas;
import mx.ginc.databinding.FragmentTelasBinding;
import mx.ginc.databinding.FragmentVerMedidasBinding;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.AddCartDesignActivity;
import mx.ginc.views.CustomizingSpecificActivity;

public class TelasFragment extends DialogFragment implements TelasAdapter.SubItemIdCallBack{

    public interface SelectedDialogListener {
        void onFinishTextDialog(Customizer customizer);
    }

    private SelectedDialogListener selectedIdListener;

    TelasAdapter adapter;
    private FragmentTelasBinding mBinding;
    private MeasurementViewModel measurementViewModel = null;
    private Context context;
    private CustomizingSpecificActivity customerActivity;
    private List<Customizer> mCustomizers;
    private LoadingProgress loadingProgress = null;
    private String idSelected = "";
    private ArrayList<String> mCustomizerIds = null;

    public TelasFragment(Context context, List<Customizer> customizers, SelectedDialogListener selectedId) {
        this.context = context;
        this.mCustomizers = customizers;
        this.selectedIdListener = selectedId;
    }

    public static TelasFragment newInstance(Context context, List<Customizer> customizers, SelectedDialogListener selectedId){
        TelasFragment fragment = new TelasFragment(context, customizers,selectedId);
        return fragment;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        mBinding = FragmentTelasBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerActivity = (CustomizingSpecificActivity) getActivity();
        loadingProgress = new LoadingProgress(customerActivity, context);
        getRecyclerView(mCustomizers);
    }

    public void getRecyclerView(List<Customizer> personalList){
        adapter = new TelasAdapter(customerActivity, context, personalList,this, true);
        mBinding.recycler.setHasFixedSize(true);
        mBinding.recycler.setLayoutManager(new GridLayoutManager(context, 2));
        mBinding.recycler.setItemViewCacheSize(20);
        mBinding.recycler.setDrawingCacheEnabled(true);
        mBinding.recycler.setAdapter(adapter);
    }


    @Override
    public void onSelectedSubItem(Customizer subItem) {
        selectedIdListener.onFinishTextDialog(subItem);
        this.dismiss();
    }

}