package mx.ginc.views.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.SubItemAdapter;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.FragmentCustomizerBinding;
import mx.ginc.views.CustomerActivity;

public class CustomizerFragment extends DialogFragment {
    private  FragmentCustomizerBinding mBinding;
    private SubItemAdapter subItemAdapter;
    private Context context;
    private Activity activity;
    private List<Customizer> personalList;

    public static CustomizerFragment newInstance(Context context, Activity activity, List<Customizer> personalList){
        CustomizerFragment fragment = new CustomizerFragment(context, activity, personalList);
        return fragment;
    }

    public CustomizerFragment(Context context, Activity activity, List<Customizer> personalList){
        this.context = context;
        this.activity = activity;
        this.personalList = personalList;
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
                mBinding = FragmentCustomizerBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getRecyclerSubItem(personalList);


    }

    public void getRecyclerSubItem(List<Customizer> personalList){
        mBinding.recyclerSubMenu.setAdapter(null);
        subItemAdapter = new SubItemAdapter(activity, context, personalList,null,false);
        subItemAdapter.notifyDataSetChanged();
        mBinding.recyclerSubMenu.setHasFixedSize(true);
        mBinding.recyclerSubMenu.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        mBinding.recyclerSubMenu.setItemViewCacheSize(20);
        mBinding.recyclerSubMenu.setDrawingCacheEnabled(true);
        mBinding.recyclerSubMenu.setAdapter(subItemAdapter);
    }
}