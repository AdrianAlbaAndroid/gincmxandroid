package mx.ginc.views.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import mx.ginc.data.AppUtil
import mx.ginc.databinding.FragmentComoMedirmeBinding


class ComoMedirmeFragment : Fragment() {

    private var mBinding: FragmentComoMedirmeBinding? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentComoMedirmeBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val videoUri: Uri = Uri.parse(AppUtil.Urls.VIDEO_COMO_MEDIRME)
        mBinding!!.videoview.setVideoURI(videoUri);
        mBinding!!.videoview.start();

        val mediaController = MediaController(context)
        mediaController.setAnchorView(mBinding!!.videoview)
        mBinding!!.videoview.setMediaController(mediaController)

    }

}
