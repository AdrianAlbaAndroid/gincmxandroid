package mx.ginc.views.fragments

import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.github.barteksc.pdfviewer.PDFView
import mx.ginc.R
import mx.ginc.data.AppUtil
import mx.ginc.databinding.ActivityDetailProductBinding
import mx.ginc.databinding.FragmentGuiaTallasBinding
import mx.ginc.databinding.FragmentMisPedidosBinding
import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class GuiaTallasFragment : DialogFragment() {

    private var mBinding: FragmentGuiaTallasBinding? = null

    fun newInstance(): GuiaTallasFragment? {
        return GuiaTallasFragment()
    }

    private lateinit var pdfView: PDFView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mBinding = FragmentGuiaTallasBinding.inflate(inflater, container, false)
        return mBinding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pdfView = view.findViewById(R.id.pdf_viewer)
        RetrivePDFfromUrl(pdfView).execute(AppUtil.Urls.URL_GUIA_TALLAS)
        mBinding!!.menuCancel.setOnClickListener{ dismiss() }

    }

    // create an async task class for loading pdf file from URL.
    class  RetrivePDFfromUrl(var pdfView: PDFView) : AsyncTask<String?, Void?, InputStream?>() {
        override fun onPostExecute(inputStream: InputStream?) {
            pdfView.fromStream(inputStream)
                    .password(null)
                    .defaultPage(0)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .enableAnnotationRendering(true)
                    .invalidPageColor(Color.WHITE)
                    .load()
        }

        override fun doInBackground(vararg params: String?): InputStream? {
            var inputStream: InputStream? = null
            try {
                val url = URL(params[0].toString())
                val urlConnection: HttpURLConnection = url.openConnection() as HttpsURLConnection
                if (urlConnection.getResponseCode() === 200) {
                    inputStream = BufferedInputStream(urlConnection.getInputStream())
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return inputStream
        }
    }
}