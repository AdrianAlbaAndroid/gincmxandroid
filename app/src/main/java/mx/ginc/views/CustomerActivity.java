package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.R;
import mx.ginc.adapter.ItemMainAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.repository.RepositoryLocal;
import mx.ginc.databinding.ActivityCustomizerBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.CustomerViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class CustomerActivity extends AppCompatActivity  implements ItemMainAdapter.PersonalIdCallBack, BaseObserver<List<CustomizedProduct>>{

    ItemMainAdapter adapter;
    private ActivityCustomizerBinding mBinding;
    private ProductViewModel mProductBuyViewModel  = null;
    private CustomerViewModel mCustomizerViewModel = null;
    private ActionBarDrawerToggle mToggle;
    private int id;
    private String tipo;
    private String tipoManga;
    private Boolean existeItem = false;
    private ArrayList<String> mCustomizerIds = null;
    private ArrayList<String> mPageLoad = null;
    private List<Medidas> listaMedidas = null;
    private  String urlImage = "";
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customizer);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getImageCustomizer();
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getImageCustomizer();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_customizer);
        pref = new AppPreferenceManager(CustomerActivity.this);
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        id = getIntent().getIntExtra("id",0);
        tipoManga = getIntent().getStringExtra("tipoManga");
        tipo = getIntent().getStringExtra("tipo");
        initViewModels();
        initToolbar();
        getSettingsWebView();
        mBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addCustomizedProduct();
            }
        });
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerActivity.this, ShoppingCartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        mBinding.btnVerMedidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getImageCustomizer();
            }
        });
    }

    private void initViewModels(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(CustomerActivity.this).get(ProductViewModel.class);
                mCustomizerViewModel = ViewModelProviders.of(CustomerActivity.this).get(CustomerViewModel.class);
            }
        });
        mCustomizerViewModel.getCustomizedMenu().observe(this,this);
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
    }

    private void getSettingsWebView(){
        mBinding.webviewImage.getSettings().setJavaScriptEnabled(true);
        mBinding.webviewImage.getSettings().setDomStorageEnabled(true);
        mBinding.webviewImage.getSettings().setBlockNetworkImage(true);
        mBinding.webviewImage.clearCache(true);
    }

    private void addCustomizedProduct(){
        mProductBuyViewModel.getRepository().getCustomizedAll(new RepositoryLocal.CustomizedAllCallBack() {
            @Override
            public void setCustomized(@NonNull List<CustomizedProduct> products) {
                Boolean items = false;
                for(CustomizedProduct customizedProduct :  sortedListMenu(products, tipoManga)){
                    if(!customizedProduct.getSelected()) {
                        items = true;
                        break;
                    }
                }
                if(items){
                    items = false;
                    Toast.makeText(CustomerActivity.this,"Debe de terminar de personalizar la camisa", Toast.LENGTH_LONG).show();
                }else{
                    mCustomizerIds =  pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
                    if(mCustomizerIds != null){
                        Intent intent = new Intent(CustomerActivity.this, AddCartDesignActivity.class);
                        intent.putExtra("tipoManga", tipoManga);
                        intent.putExtra("tipo", tipo);
                        startActivity(intent);
                    }else{
                        Toast.makeText(CustomerActivity.this,"No ha seleccionado ningún item", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void getImageCustomizer(){
        mCustomizerIds = pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
        if(mCustomizerIds != null){
            if (!AppUtil.isOnline(CustomerActivity.this)) {
                mBinding.webviewImage.setVisibility(View.INVISIBLE);
                DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi),CustomerActivity.this);

            }else{
                urlImage = AppUtil.Urls.URL_IMAGES + AppUtil.setIds(mCustomizerIds);
                mBinding.webviewImage.loadUrl(urlImage);
            }
        }
        Log.i("Images_Url", urlImage);
        getWebClient();
    }

    public void getDataRecycler(){
        if(id != 0){
            mProductBuyViewModel.getRepository().getCustomizedAll(new RepositoryLocal.CustomizedAllCallBack() {
                @Override
                public void setCustomized(@NonNull List<CustomizedProduct> products) {
                    if(products.size() !=0){
                        for(CustomizedProduct customized : sortedListMenu(products, tipoManga)){
                            Log.i("items_db", customized.getCustomizedName() + " " + customized.getIdProduct().toString());
                        }
                        getRecyclerView(sortedListMenu(products, tipoManga));
                    }
                }
            });
        }
    }

    public List<CustomizedProduct> sortedListMenu(List<CustomizedProduct>  customizedMenu, String tipoManga){
        return customizedMenu.stream()
                .filter(c -> c.getTipoManga().equals(tipoManga))
                .sorted(Comparator.comparingInt(CustomizedProduct::getIdProduct))
                .collect(Collectors.toList());
    }

    public void getRecyclerView(List<CustomizedProduct> personalList){
        adapter = new ItemMainAdapter(CustomerActivity.this, CustomerActivity.this, personalList,this, true);
        mBinding.recyclerMenu.setHasFixedSize(true);
        mBinding.recyclerMenu.setLayoutManager(new LinearLayoutManager(CustomerActivity.this, LinearLayoutManager.HORIZONTAL, false));
        mBinding.recyclerMenu.setItemViewCacheSize(20);
        mBinding.recyclerMenu.setDrawingCacheEnabled(true);
        mBinding.recyclerMenu.setAdapter(adapter);
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onSelected(CustomizedProduct customizer, int position) {
        Intent intent = new Intent(CustomerActivity.this, CustomizingSpecificActivity.class);
        intent.putExtra("index", position);
        intent.putExtra("idProduct", customizer.getIdProduct());
        intent.putExtra("id", customizer.getId());
        intent.putExtra("url", customizer.getUrlIcon());
        intent.putExtra("nombre", customizer.getCustomizedName());
        intent.putExtra("tipoManga", tipoManga);
        startActivity(intent);
    }

    private void getWebClient(){
        mBinding.webviewImage.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBinding.progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBinding.progressBar.setVisibility(View.GONE);
                mBinding.webviewImage.getSettings().setBlockNetworkImage(false);
            }
        });
    }

    @Override
    public void onChanged(List<CustomizedProduct> customizedProducts) {
        if(customizedProducts.size() !=0){
            for(CustomizedProduct customized : sortedListMenu(customizedProducts, tipoManga)){
                Log.i("items_db", customized.getCustomizedName() + " " + customized.getIdProduct().toString());
            }
            getRecyclerView(sortedListMenu(customizedProducts, tipoManga));
            mCustomizerViewModel.getCustomizedMenu().removeObservers(this);
        }
    }
}