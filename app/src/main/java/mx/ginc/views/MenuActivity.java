package mx.ginc.views;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import mx.ginc.R;
import mx.ginc.databinding.ActivityMenuPrincipalBinding;
import mx.ginc.views.fragments.ComoFuncionaFragment;
import mx.ginc.views.fragments.ComoMedirmeFragment;
import mx.ginc.views.fragments.ContactoFragment;
import mx.ginc.views.fragments.CuentaFragment;
import mx.ginc.views.fragments.MiCosturaFragment;
import mx.ginc.views.fragments.MisPedidosFragment;
import mx.ginc.views.fragments.PoliticasPrivacidadFragment;
import mx.ginc.views.fragments.TerminosFragment;

public class MenuActivity extends AppCompatActivity {

    private ActivityMenuPrincipalBinding mBinding;
    private int opcion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_menu_principal);
        mBinding.menuCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });
        opcion = getIntent().getIntExtra("opcion",0);
        switch (opcion) {
            case R.id.nav_pedidos:
                MisPedidosFragment misPedidosFragment = new MisPedidosFragment(this,this);
                setMyFragment(misPedidosFragment);
                mBinding.title.setText(getString(R.string.mis_pedidos));
                break;
            case R.id.nav_funciona:
                ComoFuncionaFragment funcionaFragment = new ComoFuncionaFragment();
                setMyFragment(funcionaFragment);
                mBinding.title.setText(getString(R.string.como_funciona));
                break;
            case R.id.nav_medirme:
                ComoMedirmeFragment comoMedirmeFragment = new ComoMedirmeFragment();
                setMyFragment(comoMedirmeFragment);
                mBinding.title.setText(getString(R.string.como_medirme));
                break;
            case R.id.nav_cuenta:
                CuentaFragment cuentaFragment = new CuentaFragment();
                setMyFragment(cuentaFragment);
                mBinding.title.setText(getString(R.string.mi_cuenta));
                break;
            case R.id.nav_terminos:
                TerminosFragment terminosFragment = new TerminosFragment();
                setMyFragment(terminosFragment);
                mBinding.title.setText(getString(R.string.terminos_condiciones));
                break;
            case R.id.nav_contacto:
                ContactoFragment contactoFragment = new ContactoFragment();
                setMyFragment(contactoFragment);
                mBinding.title.setText(getString(R.string.contacto));
                break;
            case R.id.nav_politicas_privacidad:
                PoliticasPrivacidadFragment politicasPrivacidadFragment = new PoliticasPrivacidadFragment();
                setMyFragment(politicasPrivacidadFragment);
                mBinding.title.setText(getString(R.string.politicas_privacidad));
                break;
        }


    }

    private void setMyFragment(Fragment fragment)
    {
        //get current fragment manager
        FragmentManager fragmentManager = getSupportFragmentManager();

        //get fragment transaction
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //set new fragment in fragment_container (FrameLayout)
        fragmentTransaction.replace(R.id.flContent, fragment);
        fragmentTransaction.commit();
    }

    private void finishActivity(){
        finish();
    }



}