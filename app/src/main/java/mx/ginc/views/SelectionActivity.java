package mx.ginc.views;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.databinding.ActivityTypeBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.mymeasurements.MisMedidasActivity;

public class SelectionActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private ActivityTypeBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private ProductViewModel mProductBuyViewModel  = null;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_type);
        initView();
        initToolbar();
    }

    public void initView(){
        getImageMenu();
        pref = new AppPreferenceManager(SelectionActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(SelectionActivity.this).get(ProductViewModel.class);
            }
        });
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        mBinding.rltHombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ClassicActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("tipo", "Hombre");
                startActivity(intent);
            }
        });

        mBinding.rltMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SelectionActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });
    }

    public void getImageMenu(){
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "/assets/images/mobile-app/hombre.jpg").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgHombre);
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/mujer.jpg").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgMujer);
    }


    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_menu);
        getNavigationDrawer();
    }
    public void getNavigationDrawer(){
        getHeaderView(pref.getFullName());
        mToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawerLayout.addDrawerListener(mToggle);
        mBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (!AppUtil.isOnline(SelectionActivity.this)) {
            DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), SelectionActivity.this);
        }else {
            switch (menuItem.getItemId()) {
                case R.id.nav_inicio:
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.nav_pedidos:
                    setMenu(R.id.nav_pedidos);
                    break;
                case R.id.nav_medidas:
                    Intent intentMedidas = new Intent(this, MisMedidasActivity.class);
                    intentMedidas.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentMedidas);
                    break;
                case R.id.nav_funciona:
                    setMenu(R.id.nav_funciona);
                    break;
                case R.id.nav_medirme:
                    setMenu(R.id.nav_medirme);
                    break;
                case R.id.nav_cuenta:
                    setMenu(R.id.nav_cuenta);
                    break;
                case R.id.nav_terminos:
                    setMenu(R.id.nav_terminos);
                    break;
                case R.id.nav_contacto:
                    setMenu(R.id.nav_contacto);
                    break;
                case R.id.nav_politicas_privacidad:
                    setMenu(R.id.nav_politicas_privacidad);
                    break;
                case R.id.nav_sesion:
                    Intent intentHome = new Intent(this, HomeActivity.class);
                    intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    pref.removeToken();
                    pref.removeFullName();
                    getHeaderView(pref.getFullName());
                    finish();
                    break;
            }
        }
        mBinding.drawerLayout.closeDrawers();
        return true;
    }

    private void setMenu(int option){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("opcion", option);
        startActivity(intent);
    }

}