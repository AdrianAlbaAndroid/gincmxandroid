package mx.ginc.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import mx.ginc.BaseApplication;
import mx.ginc.R;
import mx.ginc.adapter.CartAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.local.ProductSection;
import mx.ginc.databinding.ActivityShoppingBinding;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class ShoppingCartActivity extends AppCompatActivity implements BaseObserver<List<ProductSection>>, CartAdapter.CartCallBack {

    CartAdapter mCartAdapter;
    private ActivityShoppingBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private ProductViewModel mProductBuyViewModel  = null;
    public static final int PAYPAL_REQUEST_CODE = 7171;
    private String amount = "";
    double amountFinal = 0;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_shopping);
        pref = new AppPreferenceManager(ShoppingCartActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(ShoppingCartActivity.this).get(ProductViewModel.class);
            }
        });
        mProductBuyViewModel.getAllProducts().observe(this,this);
        mProductBuyViewModel.getCount().observe(this, new BaseObserver<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                if(String.valueOf(integer).equals("0")){
                    mBinding.rltMensaje.setVisibility(View.VISIBLE);
                    mBinding.btnComprar.setVisibility(View.GONE);
                    mBinding.rltSubtotal.setVisibility(View.GONE);
                    mBinding.rltEnvio.setVisibility(View.GONE);
                    mBinding.rltTotal.setVisibility(View.GONE);
                }
                else{
                    mBinding.rltMensaje.setVisibility(View.GONE);
                }
            }
        });
        mBinding.btnSeguir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ShoppingCartActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
        mBinding.btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShoppingCartActivity.this, PurchaseConfirmationActivity.class);
                intent.putExtra("activity","ShoppingCartActivity");
                startActivity(intent);
            }
        });
        initToolbar();


    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void getShoppingCart(List<ProductSection> cartList) {
        mCartAdapter = new CartAdapter(ShoppingCartActivity.this, ShoppingCartActivity.this, cartList, this);
        mBinding.recyclerviewMenu.setHasFixedSize(true);
        mBinding.recyclerviewMenu.setLayoutManager(new LinearLayoutManager(ShoppingCartActivity.this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerviewMenu.setItemViewCacheSize(20);
        mBinding.recyclerviewMenu.setDrawingCacheEnabled(true);
        mBinding.recyclerviewMenu.setAdapter(mCartAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onChanged(List<ProductSection> productSections) {
        getShoppingCart(productSections);
        getPriceTotal(productSections);

    }

    private void getPriceTotal(List<ProductSection> productSections){
        int amountTotal = 0;
        double priceProduct = 0;
        double priceTotal = 0;
        for(int i = 0; i < productSections.size(); i++){
            int amount = productSections.get(i).getAmount();
            double price = productSections.get(i).getPrice_base();
            priceProduct= price * amount;
            priceTotal += priceProduct;
            amountTotal += productSections.get(i).getAmount();
        }
        amountFinal = priceTotal;
        if(priceTotal == 0.0){
            mBinding.priceSubtotal.setText("$ 0.00");
            mBinding.Total.setText("$ 0.00");
            mBinding.SubTotalCantidad.setText("SubTotal (0 productos)");
            pref.setProductCount(0);
        }else{
            mBinding.priceSubtotal.setText(AppUtil.customFormat("$ ###,###.00", priceTotal));
            mBinding.Total.setText(AppUtil.customFormat("$ ###,###.00", priceTotal));
            if(amountTotal == 1){
                mBinding.SubTotalCantidad.setText("SubTotal (1 producto)");
            }else{
                mBinding.SubTotalCantidad.setText("SubTotal (" + amountTotal + " productos)");
            }
            pref.setProductCount(amountTotal);
        }

    }

    @Override
    public void onSelected(ProductSection product) {
        mProductBuyViewModel.deleteProduct(product.getIdCart());
    }

    @Override
    public void onUpdate(ProductSection product, int amount) {
        ProductSection dbProduct = new ProductSection();
        dbProduct.setIdCart(product.getIdCart());
        dbProduct.setAmount(amount);
        dbProduct.setPrice_base(product.getPrice_base());
        dbProduct.setPrice_final(product.getPrice_base() * amount);
        dbProduct.setIdProducto(product.getIdProducto());
        dbProduct.setSize(product.getSize());
        dbProduct.setTipo(product.getTipo());
        dbProduct.setSexo(product.getSexo());
        dbProduct.setIdCategoria(product.getIdCategoria());
        dbProduct.setImgUrl(product.getImgUrl());
        dbProduct.setDescription(product.getDescription());
        dbProduct.setObjProduct(product.getObjProduct());
        dbProduct.setObjEspecificaciones(product.getObjEspecificaciones());
        mProductBuyViewModel.updateProduct(dbProduct);
    }
}