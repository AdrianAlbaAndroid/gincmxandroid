package mx.ginc.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.databinding.ActivityDoneBinding;
import mx.ginc.databinding.ActivityPurchaseDetailBinding;
import mx.ginc.viewmodel.ProductViewModel;

public class PurchaseDetailActivity extends AppCompatActivity {

    private ActivityPurchaseDetailBinding mBinding;
    private ProductViewModel mProductBuyViewModel  = null;
    private String invoiceNumber ="";
    private String image ="";
    private int costo = 0;
    private String fechaCompra ="";
    private String etiqueta ="";
    private String esp ="";
    private String tipo ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_detail);
        invoiceNumber = getIntent().getStringExtra("invoiceNumber");
        image = getIntent().getStringExtra("image");
        costo = getIntent().getIntExtra("costo", 0);
        fechaCompra = getIntent().getStringExtra("fechaCompra");
        etiqueta = getIntent().getStringExtra("etiqueta");
        esp = getIntent().getStringExtra("esp");
        tipo = getIntent().getStringExtra("tipo");
        initView();
        getDetail();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_purchase_detail);
        initToolbar();
    }

    private void getDetail(){
        Picasso.with(PurchaseDetailActivity.this).load(image).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgDetail);
        mBinding.NumeroPedido.setText("Número de pedido: " + invoiceNumber);
        mBinding.PagoRealizado.setText("Pago Realizado: " +  AppUtil.customFormat("$ ###,###.00", costo));
        mBinding.FechaCompra.setText("Fecha de compra: " + AppUtil.convertDateTime(fechaCompra));
        mBinding.txtTitle.setText(etiqueta);
        mBinding.Especificaciones.setText(esp);
        if(tipo.equals(AppUtil.TipoProducto.CATALOGO)){
            mBinding.Detalle.setText("Especificaciones");
        }else if(tipo.equals(AppUtil.TipoProducto.PERSONALIZADO)){
            mBinding.Detalle.setText("Detalle de la camisa customizada");
        }

    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}