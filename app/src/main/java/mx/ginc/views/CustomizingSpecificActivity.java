package mx.ginc.views;

import static java.security.AccessController.getContext;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.ItemAdapter;
import mx.ginc.adapter.SubItemAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.LoadingProgress;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.ItemCustomer;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ActivityCustomizerBinding;
import mx.ginc.databinding.ActivityCustomizingSpecificBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.fragments.MedidasCustomizerFragment;
import mx.ginc.views.fragments.TelasFragment;

public class CustomizingSpecificActivity extends AppCompatActivity implements ItemAdapter.PersonalIdCallBack, SubItemAdapter.SubItemIdCallBack {

    ItemAdapter adapter;
    SubItemAdapter subItemAdapter;
    private ActivityCustomizingSpecificBinding mBinding;
    private ProductViewModel mProductBuyViewModel  = null;
    private ActionBarDrawerToggle mToggle;
    private int id;
    private int idProduct;
    private String nombre;
    private String tipoManga;
    private Boolean existeItem = false;
    private ArrayList<String> mListIds = new ArrayList<>();
    private String mIdSelected = "";
    private String mClothSelected = "";
    private String mSubItemSelected = "";
    private int index = 0;
    private String url = "";
    private ArrayList<String> mCustomizerIds = null;
    private ArrayList<String> mPageLoad = null;
    private ArrayList<String> mCustomizerSelected = null;
    public static final int DIALOG_FRAGMENT = 1;
    private  String urlImage = "";
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customizing_specific);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getImageCustomizer();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_customizing_specific);
        pref = new AppPreferenceManager(CustomizingSpecificActivity.this);
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);;
        idProduct = getIntent().getIntExtra("idProduct",0);
        id = getIntent().getIntExtra("id",0);
        index = getIntent().getIntExtra("index",0);
        nombre = getIntent().getStringExtra("nombre");
        url = getIntent().getStringExtra("url");
        tipoManga = getIntent().getStringExtra("tipoManga");
        mBinding.Nombre.setText(nombre);
        mBinding.progressBar.setVisibility(View.GONE);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(CustomizingSpecificActivity.this).get(ProductViewModel.class);
            }
        });
        initToolbar();
        getSettingsWebView();
        getDataRecycler();
        mBinding.btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCustomizerIds != null){
                    if(!mIdSelected.equals("")){
                        pref.saveArrayList(mCustomizerIds, AppUtil.Constants.ID_CUSTOMIZER);
                        updateCustomized();
                        finish();
                    }else{
                        Toast.makeText(CustomizingSpecificActivity.this,"No ha seleccionado ningun item",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void getSettingsWebView(){
        mBinding.webviewImage.getSettings().setJavaScriptEnabled(true);
        mBinding.webviewImage.getSettings().setDomStorageEnabled(true);
        mBinding.webviewImage.getSettings().setBlockNetworkImage(true);
        mBinding.webviewImage.clearCache(true);
    }

    public void getDataRecycler(){
        Log.i("ID_PRODUCTO", String.valueOf(idProduct));
        if(idProduct != 0){
            ProductServer.getInstance().getMenuItemFirst(idProduct, new ProductServer.CustomizerCallBack() {
                @Override
                public void onSuccess(List<Customizer> mListCustomizer) {
                    if(mListCustomizer.size() != 0){
                        if(mListCustomizer.get(0).getUltimo() != null){
                            mBinding.lnrPay2.setVisibility(View.GONE);
                            TelasFragment telasFragment = TelasFragment
                                    .newInstance(CustomizingSpecificActivity.this, mListCustomizer, new TelasFragment.SelectedDialogListener() {
                                        @Override
                                        public void onFinishTextDialog(Customizer customizer) {
                                            mIdSelected = String.valueOf(customizer.getId());
                                            mClothSelected = " -> " + customizer.getEtiqueta();
                                            mCustomizerIds =  pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
                                            if(mCustomizerIds != null) {
                                                mCustomizerIds.get(index);
                                                mCustomizerIds.set(index,String.valueOf(customizer.getId()));
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        getImage(mCustomizerIds);
                                                    }
                                                });
                                            }
                                        }
                                    });
                            telasFragment.show(getSupportFragmentManager(), "dialog");
                        }else{
                            getRecyclerSubItem(mListCustomizer);
                        }
                    }
                }
            });
        }
    }

    private void updateCustomized(){
        String itemSelected = nombre + mSubItemSelected + mClothSelected;
        CustomizedProduct product = new CustomizedProduct();
        product.setId(id);
        product.setIdProduct(idProduct);
        product.setCustomizedName(nombre);
        product.setUrlIcon(url);
        product.setIndexProduct(index);
        product.setSelected(true);
        product.setItemSelected(itemSelected);
        product.setTipoManga(tipoManga);
        mProductBuyViewModel.updateCustomizedProduct(product);
    }

    private void getImageCustomizer(){
        mCustomizerIds =  pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
        mPageLoad = pref.getArrayList(AppUtil.Constants.PAGELOAD);
        if(mCustomizerIds != null){
            if (!AppUtil.isOnline(CustomizingSpecificActivity.this)) {
                mBinding.webviewImage.setVisibility(View.INVISIBLE);
                DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi),CustomizingSpecificActivity.this);
            }else{
                urlImage = AppUtil.Urls.URL_IMAGES + AppUtil.setIds(mCustomizerIds);
                mBinding.webviewImage.loadUrl(urlImage);
            }
        }
        getWebClient();
    }

    public void getRecyclerSubItem(List<Customizer> personalList){
        mBinding.recyclerMenu2.setAdapter(null);
        subItemAdapter = new SubItemAdapter(CustomizingSpecificActivity.this, CustomizingSpecificActivity.this, personalList,this, true);
        subItemAdapter.notifyDataSetChanged();
        mBinding.recyclerMenu2.setHasFixedSize(true);
        mBinding.recyclerMenu2.setLayoutManager(new LinearLayoutManager(CustomizingSpecificActivity.this, LinearLayoutManager.HORIZONTAL, false));
        mBinding.recyclerMenu2.setItemViewCacheSize(20);
        mBinding.recyclerMenu2.setDrawingCacheEnabled(true);
        mBinding.recyclerMenu2.setAdapter(subItemAdapter);
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onSelected(Customizer customizer) {
        mSubItemSelected = " -> " + customizer.getEtiqueta();
        Log.i("PRODUCTO_SELECCIONADO", String.valueOf(mSubItemSelected));
       ProductServer.getInstance().getMenuItemFirst(customizer.getId(), new ProductServer.CustomizerCallBack() {
            @Override
            public void onSuccess(List<Customizer> mListCustomizer) {
                if(mListCustomizer.size() != 0){
                    if(mListCustomizer.get(0).getUltimo()){
                        mBinding.lnrPay2.setVisibility(View.GONE);
                        TelasFragment telasFragment = TelasFragment
                                .newInstance(CustomizingSpecificActivity.this, mListCustomizer, new TelasFragment.SelectedDialogListener() {
                                    @Override
                                    public void onFinishTextDialog(Customizer customizer) {
                                        mIdSelected= String.valueOf(customizer.getId());
                                        mClothSelected = " -> " + customizer.getEtiqueta();
                                        mCustomizerIds =  pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
                                        if(mCustomizerIds != null){
                                            mCustomizerIds.get(index);
                                            mCustomizerIds.set(index, String.valueOf(customizer.getId()));
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getImage(mCustomizerIds);
                                                }
                                            });
                                        }
                                    }
                                });
                        telasFragment.show(getSupportFragmentManager(), "dialog");
                    }else{
                        getRecyclerSubItem(mListCustomizer);
                    }
                }

            }
        });
    }

    @Override
    public void onSelectedSubItem(Customizer subItem) {
        mSubItemSelected = " -> " + subItem.getEtiqueta();
        ProductServer.getInstance().getMenuItemFirst(subItem.getId(), new ProductServer.CustomizerCallBack() {
            @Override
            public void onSuccess(List<Customizer> mListCustomizer) {
                if(mListCustomizer.size() != 0){
                    if(mListCustomizer.get(0).getUltimo()){
                        TelasFragment telasFragment = TelasFragment
                                .newInstance(CustomizingSpecificActivity.this, mListCustomizer, new TelasFragment.SelectedDialogListener() {
                                    @Override
                                    public void onFinishTextDialog(Customizer customizer) {
                                        mIdSelected= String.valueOf(customizer.getId());
                                        mClothSelected = " -> " + customizer.getEtiqueta();
                                        mCustomizerIds =  pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
                                        if(mCustomizerIds != null){
                                            mCustomizerIds.get(index);
                                            mCustomizerIds.set(index,String.valueOf(customizer.getId()));
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getImage(mCustomizerIds);
                                                }
                                            });
                                        }
                                    }
                                });
                        telasFragment.show(getSupportFragmentManager(), "dialog");
                    }else{
                        getRecyclerSubItem(mListCustomizer);
                    }
                }
            }

        });
    }

    private void getImage(ArrayList<String> mCustomizerIds){
        if(mCustomizerIds != null){
            if (!AppUtil.isOnline(CustomizingSpecificActivity.this)) {
                mBinding.webviewImage.setVisibility(View.INVISIBLE);
                DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi),CustomizingSpecificActivity.this);
            }else{
                urlImage = AppUtil.Urls.URL_IMAGES + AppUtil.setIds(mCustomizerIds);
                mBinding.webviewImage.loadUrl(urlImage);
            }
        }
        getWebClient();
    }

    private void getWebClient(){
        mBinding.webviewImage.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBinding.progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBinding.progressBar.setVisibility(View.GONE);
                mBinding.webviewImage.getSettings().setBlockNetworkImage(false);
            }
        });
    }

}

