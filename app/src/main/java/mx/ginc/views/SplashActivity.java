package mx.ginc.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ActivityLoginBinding;
import mx.ginc.databinding.ActivitySplashBinding;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.ProductViewModel;

public class SplashActivity extends AppCompatActivity {

    private ActivitySplashBinding mBinding;
    private ProductViewModel mProductBuyViewModel  = null;
    private final int DURACION_SPLASH = 3500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_splash);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(SplashActivity.this).get(ProductViewModel.class);
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            };
        }, DURACION_SPLASH);
    }
}
