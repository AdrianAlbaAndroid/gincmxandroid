package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.DirectionAdapter;
import mx.ginc.adapter.ShirtAdapter;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.Direccion;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.network.DirectionServer;
import mx.ginc.databinding.ActivityDirectionAllBinding;
import mx.ginc.viewmodel.DirectionViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class DirectionActivity extends AppCompatActivity implements ShirtAdapter.ShirtIdCallBack, BaseObserver<List<DirectionAll>>, DirectionAdapter.DirectionDeleteCallBack, DirectionAdapter.DirectionEditCallBack {

    private DirectionAdapter adapter;
    private ActivityDirectionAllBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private DirectionViewModel mDirectionViewModel = null;
    private List<DirectionAll> listado = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction_all);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_direction_all);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDirectionViewModel = ViewModelProviders.of(DirectionActivity.this).get(DirectionViewModel.class);
            }
        });
        mDirectionViewModel.getDirectionAll(this).observe(this,this);
        mBinding.btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DirectionActivity.this, DirectionRegisterActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        initToolbar();
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void getDirectionAll(List<DirectionAll> mDirectionList){
        adapter = new DirectionAdapter(this, this, mDirectionList, this, this);
        mBinding.recyclerview.setHasFixedSize(true);
        mBinding.recyclerview.setLayoutManager(new LinearLayoutManager(DirectionActivity.this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerview.setItemViewCacheSize(20);
        mBinding.recyclerview.setDrawingCacheEnabled(true);
        mBinding.recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSelected(Customizer customizer) {
        Intent intent = new Intent(this, SubCatalogoActivity.class);
        intent.putExtra("id", customizer.getId());
        startActivity(intent);
    }

    @Override
    public void onChanged(List<DirectionAll> directionAlls) {
        listado = directionAlls;
        getDirectionAll(directionAlls);
        mDirectionViewModel.getDirectionAll(this).removeObservers(this);

    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK",null);
        builder.create();
        builder.show();
    }

    @Override
    public void onSelectedDeleted(int id, int position) {
        DirectionServer.getInstance(this).getDeleteById(id, new DirectionServer.DirectionDeleteCallBack() {
            @Override
            public void isDelete(Boolean isDelete, String message) {
                if(isDelete){
                    createDialog("Alerta", message);
                    listado.remove(position);
                    adapter.notifyItemRemoved(position);
                }
            }
        });
    }

    @Override
    public void onSelected(Direccion direction) {
        Intent intent = new Intent(DirectionActivity.this, DirectionEditActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("value", direction);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}