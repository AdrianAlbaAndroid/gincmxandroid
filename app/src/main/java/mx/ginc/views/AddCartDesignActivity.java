package mx.ginc.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.local.ProductSection;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.ActivityCartDesignBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.views.fragments.DetailCustomizerFragment;
import mx.ginc.views.mymeasurements.MisMedidasActivity;

public class AddCartDesignActivity extends AppCompatActivity {

    private ActivityCartDesignBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private ProductViewModel mProductBuyViewModel  = null;
    private MeasurementViewModel mMeasurementViewModel = null;
    private String tipo;
    private List<Medidas> listado = new ArrayList<>();
    private String mJsonObject = "";
    private String mTipoCustomizacion = "";
    private ArrayList<String> mCustomizerIds = null;
    private String mImageString = "";
    private static final String TAG = "CUSTOMIZED_IDS";
    private String urlImage = "";
    private String tipoManga = "";
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_design);
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getImageCustomizer();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getImageCustomizer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cart_design);
        pref = new AppPreferenceManager(AddCartDesignActivity.this);
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        tipo = getIntent().getStringExtra("tipo");
        tipoManga = getIntent().getStringExtra("tipoManga");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(AddCartDesignActivity.this).get(ProductViewModel.class);
                mMeasurementViewModel = ViewModelProviders.of(AddCartDesignActivity.this).get(MeasurementViewModel.class);
            }
        });
       // mMeasurementViewModel.getMisMedidas(this).observe(this,this);
        mBinding.btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               MeasurementServer.getInstance(AddCartDesignActivity.this).getMedida(new MeasurementServer.MedidasCallBack() {
                    @Override
                    public void onSuccess(Medidas medida) {
                        mProductBuyViewModel.deleteCustmizedAll();
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        mJsonObject =  gson.toJson(medida);
                        saveProduct();
                        Intent intent = new Intent(AddCartDesignActivity.this, ShoppingCartActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                   @Override
                   public void onEmpty() {
                       createDialogOKCancel("Alerta", getString(R.string.crear_medida_dialog));
                   }

                   @Override
                    public void onFailure(JSONObject jsonObject) {
                        try {
                            if(jsonObject.getInt("http") == 401){
                                Intent intent = new Intent(AddCartDesignActivity.this, LoginActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        mBinding.btnDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailCustomizerFragment detailFragment = DetailCustomizerFragment
                        .newInstance(AddCartDesignActivity.this, tipoManga);
                detailFragment.show(getSupportFragmentManager(), "dialog");
            }
        });

        mBinding.btnCancelarCompra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pref.saveArrayList(null, AppUtil.Constants.ID_CUSTOMIZER);
                removeCustomizer();
                Intent intent = new Intent(AddCartDesignActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        mBinding.btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        initToolbar();
        getImageCustomizer();
        getSettingsWebView();
    }

    private void getSettingsWebView(){
        mBinding.webviewImage.getSettings().setJavaScriptEnabled(true);
        mBinding.webviewImage.getSettings().setDomStorageEnabled(true);
        mBinding.webviewImage.getSettings().setBlockNetworkImage(true);
        mBinding.webviewImage.getSettings().setBuiltInZoomControls(true);
        mBinding.webviewImage.getSettings().setDisplayZoomControls(false);
        mBinding.webviewImage.clearCache(true);
    }

    private void getImageCustomizer(){
        mCustomizerIds =   pref.getArrayList(AppUtil.Constants.ID_CUSTOMIZER);
        if(mCustomizerIds != null){
            if (!AppUtil.isOnline(AddCartDesignActivity.this)) {
                mBinding.webviewImage.setVisibility(View.INVISIBLE);
                DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), AddCartDesignActivity.this);

            }else{
                urlImage = AppUtil.Urls.URL_IMAGES + AppUtil.setIds(mCustomizerIds);
                mBinding.webviewImage.loadUrl(urlImage);
            }
        }
        Log.i("Images_Url", urlImage);
        getWebClient();
    }

    private void createDialogOKCancel(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(AddCartDesignActivity.this, MisMedidasActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.create();
        builder.show();
    }

    private void saveProduct(){
        ProductSection product = new ProductSection();
        product.setDescription("CAMISA CUSTOMIZADA");
        product.setTipo(AppUtil.TipoProducto.PERSONALIZADO);
        product.setSexo(tipo);
        product.setPrice_base(Double.valueOf(AppUtil.Constants.PRECIO_CAMISA));
        product.setPrice_final(Double.valueOf(AppUtil.Constants.PRECIO_CAMISA));
        product.setAmount(1);
        product.setObjEspecificaciones(mJsonObject);
        mTipoCustomizacion =  AppUtil.setIds(mCustomizerIds);
        product.setObjProduct(mTipoCustomizacion);
        product.setImgCustomizer(mImageString);
        mProductBuyViewModel.insertProduct(product);
        removeCustomizer();
    }

    private void removeCustomizer(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(AddCartDesignActivity.this);
        prefs.edit().remove(AppUtil.Constants.ID_CUSTOMIZER).commit();
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getWebClient(){
        mBinding.webviewImage.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBinding.progressBar.setVisibility(View.VISIBLE);
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBinding.progressBar.setVisibility(View.GONE);
            }
        });
    }

}