package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthCredential;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.ProviderType;
import mx.ginc.data.entities.User;
import mx.ginc.data.entities.UserRegister;
import mx.ginc.data.network.UserServer;
import mx.ginc.databinding.ActivityAccountBinding;
import mx.ginc.databinding.ActivityLoginBinding;

public class AccountActivity extends AppCompatActivity {
    private ActivityAccountBinding mBinding;
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int GOOGLE_SIGN_IN = 100;
    private int checkTerminos = 0;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        FirebaseApp.initializeApp(this);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        initView();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
    }

    public void initView(){
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        pref = new AppPreferenceManager(AccountActivity.this);
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_account);
        mBinding.btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mBinding.edtEmail.getText().toString();
                if(!validaEmail(email)){
                    createDialog("Mensaje", "El email ingresado es inválido.");
                    return;
                }
                /*if(checkTerminos == 0){
                    createDialog("Mensaje", "Debe de aceptar la política de privacidad.");
                    return;
                }*/
                String name = mBinding.edtNombre.getText().toString();
                String user = mBinding.edtUser.getText().toString();
                String aPaterno = mBinding.edtApellidoPaterno.getText().toString();
                String aMaterno = mBinding.edtApellidoMaterno.getText().toString();
                String password = mBinding.edtContrasena.getText().toString();
                String passwordConfirm = mBinding.edtConfirmar.getText().toString();
                if (!validaDatos(user, name, email, aPaterno, aMaterno, password, passwordConfirm)){
                    JsonObject userObject = setUserRegister(user, name, email, aPaterno, aMaterno, password, passwordConfirm);
                    UserServer.getInstance(AccountActivity.this).setUserRegister(userObject,  new UserServer.UserCallBack() {
                        @Override
                        public void onSuccess(User user) {
                            pref.saveToken(user.getAccess_token());
                            pref.saveFullName(user.getNombre());
                            Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            Toast.makeText(AccountActivity.this,"Registro exitoso",Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(String mensaje) {
                            createDialog("Alerta", mensaje);
                        }
                    });
                }
            }
        });
        mBinding.imgfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbackManager = CallbackManager.Factory.create();
                LoginManager.getInstance().logInWithReadPermissions(AccountActivity.this, Arrays.asList( "email"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if(loginResult != null){
                            AccessToken token = loginResult.getAccessToken();
                            System.out.println("TOken :" + token.getToken());
                            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
                            mAuth.signInWithCredential(credential)
                                    .addOnCompleteListener(AccountActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                FirebaseUser user = mAuth.getCurrentUser();
                                                UserServer.getInstance(AccountActivity.this).setUserProvider(setUserProvider(user), ProviderType.FACEBOOK.name(), new UserServer.UserCallBack() {
                                                    @Override
                                                    public void onSuccess(User user) {
                                                        if(user.getAccess_token() != null){
                                                            pref.saveToken(user.getAccess_token());
                                                            pref.saveFullName(user.getNombre());
                                                            Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                            Toast.makeText(AccountActivity.this,"Registro exitoso",Toast.LENGTH_LONG).show();
                                                        }else{
                                                            createDialog("Alerta", "Usuario o contraseña no validos");
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(String mensaje) {
                                                        createDialog("Alerta", mensaje);
                                                    }
                                                });
                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Toast.makeText(AccountActivity.this, "Authentication failed.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }

                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        System.out.println("Error Facebook " + error.getMessage());
                        createDialog("Mensaje", "Se ha producido un error.");
                    }
                });
            }
        });

        mBinding.imggoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();

                mGoogleSignInClient = GoogleSignIn.getClient(AccountActivity.this, gso);
                mGoogleSignInClient.signOut();
                startActivityForResult(mGoogleSignInClient.getSignInIntent(), GOOGLE_SIGN_IN);
            }
        });

        mBinding.chkPolitica.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    checkTerminos = 1;
                }
                else
                {
                    checkTerminos = 0;
                }
            }
        });

    }

    private boolean validaEmail(String email){
        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        return mather.find();
    }

    private JsonObject setUserProvider(FirebaseUser user){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("id", user.getUid());
        requestBody.addProperty("email", user.getEmail());
        requestBody.addProperty("name", user.getDisplayName());
        return requestBody;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        if(mCallbackManager != null){
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == GOOGLE_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
               if(account !=null){
                   AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                   mAuth.signInWithCredential(credential)
                           .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                               @Override
                               public void onComplete(@NonNull Task<AuthResult> task) {
                                   if (task.isSuccessful()) {
                                       FirebaseUser user = mAuth.getCurrentUser();
                                       UserServer.getInstance(AccountActivity.this).setUserProvider(setUserProvider(user), ProviderType.GOOGLE.name(), new UserServer.UserCallBack() {
                                           @Override
                                           public void onSuccess(User user) {
                                               if(user.getAccess_token() != null){
                                                   pref.saveToken(user.getAccess_token());
                                                   pref.saveFullName(user.getNombre());
                                                   Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
                                                   startActivity(intent);
                                                   finish();
                                                   Toast.makeText(AccountActivity.this,"Registro exitoso",Toast.LENGTH_LONG).show();
                                               }else{
                                                   createDialog("Alerta", "Usuario o contraseña no validos");
                                               }
                                           }

                                           @Override
                                           public void onFailure(String mensaje) {
                                               createDialog("Alerta", mensaje);
                                           }
                                       });
                                   } else {
                                       createDialog("Mensaje", "Se ha producido un error.");
                                   }
                               }
                           });
               }
            } catch (ApiException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    private JsonObject setUserRegister(String user, String name, String email, String aPaterno, String aMaterno, String password, String passwordConfirm){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("usuario", user);
        requestBody.addProperty("password", password);
        requestBody.addProperty("password_confirmation", passwordConfirm);
        requestBody.addProperty("email", email);
        requestBody.addProperty("nombre", name);
        requestBody.addProperty("apellidoPaterno", aPaterno);
        requestBody.addProperty("apellidoMaterno", aMaterno);
        requestBody.addProperty("terminosYCondiciones", 1);
        requestBody.addProperty("deseoRecibirNovedades", 1);
        return requestBody;
    }

    public boolean validaDatos(String user, String name, String email, String aPaterno, String aMaterno, String password, String passwordConfirm){
        boolean contieneError = false;
        if (user.equals("")) {
            createDialog("Mensaje", "Debe de ingresar el usuario.");
            contieneError = true;
        }else if (name.equals("")) {
            createDialog("Mensaje", "Debe de ingresar el nombre.");
            contieneError = true;
        }else if (email.equals("")) {
            createDialog("Mensaje", "Debe de ingresar el email.");
            contieneError = true;
        }else if (aPaterno.equals("")) {
            createDialog("Mensaje", "Debe de ingresar el Apellido Paterno.");
            contieneError = true;
        }else if (aMaterno.equals("")) {
            createDialog("Mensaje", "Debe de ingresar el Apellido Materno.");
            contieneError = true;
        }else if (password.equals("")) {
            createDialog("Mensaje", "Debe de ingresar la Contraseña.");
            contieneError = true;
        }else if (passwordConfirm.equals("")) {
            createDialog("Mensaje", "Debe de ingresar la Confirmación de Contraseña.");
            contieneError = true;
        }
        return contieneError;
    }


    private void createDialogAccept(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(AccountActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
