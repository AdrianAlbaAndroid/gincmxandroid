package mx.ginc.views.mymeasurements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.JsonObject;

import mx.ginc.R;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.FragmentEdadBinding;
import mx.ginc.databinding.FragmentPesoAlturaBinding;
import mx.ginc.dialog.DialogAlerta;

public class EdadFragment extends Fragment {

    private FragmentEdadBinding mBinding;
    private String edad = "";
    private Context mContext;
    private String peso = "";
    private String altura = "";
    private int idTipoVientre = 0;
    private int idTipoPecho = 0;
    private MedidasAll medida;
    private Boolean editar;
    private View mView;
    private MisMedidasActivity activity;

    public EdadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edad, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        mView = view;
        peso = getArguments().getString("peso");
        altura = getArguments().getString("altura");
        idTipoVientre = getArguments().getInt("idTipoVientre");
        idTipoPecho = getArguments().getInt("idTipoPecho");
        editar = getArguments().getBoolean("editar");
        activity.setTitleToolbar(getString(R.string.edad));
        if(editar){
            medida = (MedidasAll) getArguments().getSerializable("medida");
            mBinding.Edad.setText(medida.getEdad());
            mBinding.btnContinuar.setText("Guardar");
        }
        mBinding.btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edad = mBinding.Edad.getText().toString();
                if(validaInfo(edad)){
                    hideKeyboardFrom(mContext, mView);
                    if(editar){
                        actualizarEdad(Integer.valueOf(medida.getId()),setEdad(edad));
                    }else{
                        Navigation.findNavController(mView).navigate(R.id.tipoCamisaFragment, setArgs(edad));
                    }

                }
            }
        });
    }

    private void actualizarEdad(int id, JsonObject jsonObject){
        MeasurementServer.getInstance(mContext).actualizarMedidas(id, jsonObject, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView, "Alerta", message,R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                DialogAlerta.createDialogOK("Alerta", message, mContext);
            }
        });
    }

    private JsonObject setEdad(String edad){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("edad",edad);
        return requestBody;
    }

    private Bundle setArgs(String edad){
        Bundle bundle = new Bundle();
        bundle.putString("peso", peso);
        bundle.putString("altura", altura);
        bundle.putInt("idTipoVientre", idTipoVientre);
        bundle.putInt("idTipoPecho", idTipoPecho);
        bundle.putString("edad", edad);
        return bundle;
    }

    private boolean validaInfo(String edad){
        if(edad.equals("")){
            DialogAlerta.createDialogOK("Alerta","El edad esta vacio", mContext);
            return false;
        }
        return true;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}