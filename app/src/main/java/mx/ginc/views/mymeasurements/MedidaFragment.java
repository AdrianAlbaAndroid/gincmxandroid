package mx.ginc.views.mymeasurements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import mx.ginc.R;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.views.LoginActivity;

public class MedidaFragment extends Fragment {

    private MisMedidasActivity activity;
    private NavController navController;
    private MeasurementViewModel measurementViewModel = null;
    private Context mContext;
    private MedidasAll medidasAll;
    private Button btnCrearMedida;
    private TextView lblPeso, lblAltura, lblCuello, lblPecho, lblEdad, lblLargoManga, lblTipoCamisa, lblTipoPecho, lblTipoVientre;
    private RelativeLayout rtlCards;
    private CardView cardView1, cardView2,cardView3,cardView4,cardView5,cardView6;

    public MedidaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_medida, container, false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getMedida();
    }

    @Override
    public void onResume() {
        super.onResume();
        getMedida();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        navController = Navigation.findNavController(view);
        activity.setTitleToolbar(getString(R.string.mis_medidas));
        initViewModel();
        addComponents(view);
        getMedida();
        btnCrearMedida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(btnCrearMedida.getText().toString()){
                    case "Crear medida":
                        Navigation.findNavController(view).navigate(R.id.pesoAlturaFragment);
                        break;
                    case "Borrar medida":
                        borrarMedida();
                        break;
                }

            }
        });
        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.pesoAlturaFragment,setArgs());
            }
        });
        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.tipoVientreFragment,setArgs());
            }
        });
        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.tipoPechoFragment,setArgs());
            }
        });
        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.edadFragment,setArgs());
            }
        });
        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.tipoCamisaFragment,setArgs());
            }
        });
        cardView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.cuelloPechoFragment,setArgs());
            }
        });
    }

    private Bundle setArgs(){
        Bundle bundle = new Bundle();
        bundle.putSerializable("medida", medidasAll);
        bundle.putBoolean("editar", true);
        return bundle;
    }

    private void addComponents(View view) {
        btnCrearMedida = view.findViewById(R.id.btnCrearMedida);
        rtlCards = view.findViewById(R.id.rtlCards);
        lblLargoManga = view.findViewById(R.id.Manga);
        lblAltura = view.findViewById(R.id.Altura);
        lblPeso = view.findViewById(R.id.Peso);
        lblEdad = view.findViewById(R.id.Edad);
        lblTipoVientre = view.findViewById(R.id.Vientre);
        lblTipoPecho = view.findViewById(R.id.TipoPecho);
        lblTipoCamisa = view.findViewById(R.id.TipoCamisa);
        lblCuello = view.findViewById(R.id.Cuello);
        lblPecho = view.findViewById(R.id.Pecho);
        cardView1 = view.findViewById(R.id.crdItem1);
        cardView2 = view.findViewById(R.id.crdItem2);
        cardView3 = view.findViewById(R.id.crdItem3);
        cardView4 = view.findViewById(R.id.crdItem4);
        cardView5 = view.findViewById(R.id.crdItem5);
        cardView6 = view.findViewById(R.id.crdItem6);
    }

    private void initViewModel() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                measurementViewModel = ViewModelProviders.of(activity).get(MeasurementViewModel.class);
            }
        });
    }

    private void getMedida(){
        MeasurementServer.getInstance(mContext).getMedida(new MeasurementServer.MedidasCallBack() {
            @Override
            public void onSuccess(Medidas medida) {
                rtlCards.setVisibility(View.VISIBLE);
                lblPeso.setText(medida.getPeso() + " kg");
                lblAltura.setText(medida.getAltura() + " cm");
                lblCuello.setText(medida.getCuello() + " cm");
                lblPecho.setText(medida.getPecho() + " cm");
                lblEdad.setText(medida.getEdad() + " años");
                lblLargoManga.setText(medida.getLargoManga() + " cm");
                lblTipoCamisa.setText(medida.getTipoCamisaMedida());
                lblTipoPecho.setText(medida.getTipoPecho());
                lblTipoVientre.setText(medida.getTipoVientre());
                btnCrearMedida.setText(getString(R.string.borrar_medida));
                medidasAll = setMedidas(medida);
            }

            @Override
            public void onEmpty() {
                btnCrearMedida.setText(getString(R.string.crear_medida));
            }

            @Override
            public void onFailure(JSONObject jsonObject) {
                try {
                    if (jsonObject.getInt("http") == 401) {
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void borrarMedida(){
        MeasurementServer.getInstance(mContext).deleteMedida(Integer.parseInt(medidasAll.getId()),new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                createDialog("Alerta", message);
                limpiarMedidas();
                btnCrearMedida.setText(getString(R.string.crear_medida));
            }

            @Override
            public void onFailure(String message) {
                createDialog("Alerta", message);
            }
        });
    }

    private void limpiarMedidas(){
        rtlCards.setVisibility(View.GONE);
    }


    private MedidasAll setMedidas(Medidas medidas) {
        return new MedidasAll(medidas.getId(),
                medidas.getNombre(),
                medidas.getAltura(),
                medidas.getPeso(),
                medidas.getTipoVientre(),
                medidas.getTipoPecho(),
                medidas.getEdad(),
                medidas.getTipoCamisaMedida(),
                medidas.getCuello(),
                medidas.getPecho(),
                medidas.getCintura(),
                medidas.getCadera(),
                medidas.getLargoManga(),
                medidas.getLargoMuneca(),
                medidas.getMunecaIzquierda(),
                medidas.getMunecaDerecha(),
                medidas.getBiceps(),
                medidas.getEspalda(),
                medidas.getAida(),
                medidas.getLargo(),
                medidas.getPrincipal());
    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", null);
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }
}