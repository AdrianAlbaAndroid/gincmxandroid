package mx.ginc.views.mymeasurements;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import mx.ginc.R;

public class MisMedidasActivity extends AppCompatActivity {

    private NavController navController;
    private Toolbar toolbar;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_medida);
        initView();
    }

    public void initView(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ImageView imgCancel = (ImageView) findViewById(R.id.menuCancel);
        textView = (TextView) findViewById(R.id.title);
        NavHostFragment navHostFragment =
                (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainerView);
        navController = navHostFragment.getNavController();
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.medidaFragment, R.id.pesoAlturaFragment, R.id.tipoVientreFragment, R.id.tipoPechoFragment, R.id.edadFragment)
                .build();
        setSupportActionBar(toolbar);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(toolbar, navController);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void setTitleToolbar(String toolbarTitle){
        textView.setText(toolbarTitle);
    }






}