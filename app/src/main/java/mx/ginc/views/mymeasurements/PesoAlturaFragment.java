package mx.ginc.views.mymeasurements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.JsonObject;

import mx.ginc.R;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.FragmentPesoAlturaBinding;
import mx.ginc.databinding.FragmentTipoVientreBinding;
import mx.ginc.dialog.DialogAlerta;

public class PesoAlturaFragment extends Fragment {

    private FragmentPesoAlturaBinding mBinding;
    private Context mContext;
    private MisMedidasActivity activity;
    private View mView;
    private MedidasAll medida;
    private Boolean editar;

    public PesoAlturaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_peso_altura, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        mView = view;
        activity.setTitleToolbar(getString(R.string.peso_altura));
        editar = getArguments().getBoolean("editar");
        if(editar){
            medida = (MedidasAll) getArguments().getSerializable("medida");
            mBinding.Peso.setText(medida.getPeso());
            mBinding.Altura.setText(medida.getAltura());
            mBinding.btnContinuar.setText("Guardar");
        }
        mBinding.btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String peso = mBinding.Peso.getText().toString();
                String altura = mBinding.Altura.getText().toString();
                if(validaInfo(peso, altura)){
                    hideKeyboardFrom(mContext, mView);
                    if(editar){
                        actualizarPesoAltura(Integer.valueOf(medida.getId()), setPesoAltura(peso, altura));
                    }else{
                        Navigation.findNavController(view).navigate(R.id.tipoVientreFragment, setArgs(peso, altura));
                    }
                }
            }
        });
    }

    private void actualizarPesoAltura(int id, JsonObject jsonObject){
        MeasurementServer.getInstance(mContext).actualizarMedidas(id, jsonObject, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView,"Alerta", message, R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                DialogAlerta.createDialogOK("Alerta", message, mContext);
            }
        });
    }

    private JsonObject setPesoAltura(String peso, String altura){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("peso",peso);
        requestBody.addProperty("altura",altura);
        return requestBody;
    }

    private Bundle setArgs(String peso,  String altura){
        Bundle bundle = new Bundle();
        bundle.putString("peso", peso);
        bundle.putString("altura", altura);
        return bundle;
    }

    private boolean validaInfo(String peso, String altura){
        if(peso.equals("")){
            DialogAlerta.createDialogOK("Alerta","El campo peso esta vacio", mContext);
            return false;
        }
        if(altura.equals("")){
            DialogAlerta.createDialogOK("Alerta","El campo altura esta vacio", mContext);
            return false;
        }
        return true;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}