package mx.ginc.views.mymeasurements;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.entities.Belly;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.FragmentTipoCamisaBinding;
import mx.ginc.databinding.FragmentTipoPechoBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class TipoCamisaFragment extends Fragment {

    private MeasurementViewModel mMeasurementViewModel = null;
    private Context mContext;
    private MisMedidasActivity activity;
    private FragmentTipoCamisaBinding mBinding;
    private View mView;
    RadioGroup rg;
    private int fit = 0;
    private String peso = "";
    private String altura = "";
    private int idTipoVientre = 0;
    private int idTipoPecho = 0;
    private String edad  = "";
    private MedidasAll medida;
    private Boolean editar;

    public TipoCamisaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tipo_camisa, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        mView = view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        activity.setTitleToolbar(getString(R.string.tipo_camisa));
        peso = getArguments().getString("peso");
        altura = getArguments().getString("altura");
        idTipoVientre = getArguments().getInt("idTipoVientre");
        idTipoPecho = getArguments().getInt("idTipoPecho");
        edad = getArguments().getString("edad");
        editar = getArguments().getBoolean("editar");
        medida = (MedidasAll) getArguments().getSerializable("medida");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMeasurementViewModel = ViewModelProviders.of(activity).get(MeasurementViewModel.class);
            }
        });
        mMeasurementViewModel.getFitPreference(mContext).observe(activity, new BaseObserver<List<Belly>>() {
            @Override
            public void onChanged(List<Belly> bellies) {
                mMeasurementViewModel.getFitPreference(mContext).removeObservers(activity);
                List<Belly> listado = new ArrayList<>();
                rg = new RadioGroup(mContext);
                for(int i = 0; i < bellies.size(); i++){
                    listado.add(bellies.get(i));
                    RadioButton rb1 = new RadioButton(mContext);
                    rb1.setText(bellies.get(i).getEtiqueta());
                    rb1.setTextColor(Color.BLACK);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        rb1.setButtonTintList(ContextCompat.getColorStateList(mContext,R.color.colorBlack));
                    }
                    rg.addView(rb1);

                }
                mBinding.rtlFit.addView(rg);
                rg.setOrientation(RadioGroup.VERTICAL);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int) RadioGroup.LayoutParams.WRAP_CONTENT,(int) RadioGroup.LayoutParams.WRAP_CONTENT);
                params.leftMargin =150;
                params.topMargin = 100;

                rg.setLayoutParams(params);
                if(rg.getParent() != null) {
                    ((ViewGroup)rg.getParent()).removeView(rg); // <- fix
                }
                mBinding.rtlFit.addView(rg);
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton radioButton = (RadioButton) mView.findViewById(checkedId);
                        for(int i = 0; i < bellies.size(); i++){
                            if(radioButton.getText().equals(bellies.get(i).getEtiqueta())){
                                fit = bellies.get(i).getId();
                            }
                        }
                    }
                });
            }
        });
        mBinding.btnContinuar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(fit == 0) {
                    Toast.makeText(mContext, "Debe de seleccionar una opción", Toast.LENGTH_LONG).show();
                }else{
                    if(editar){
                        actualizarTipoCamisa(Integer.valueOf(medida.getId()), setTipoCamisa(fit));
                    }else {
                        Navigation.findNavController(mView).navigate(R.id.cuelloPechoFragment, setArgs());
                    }

                }
            }
        });
    }

    private Bundle setArgs(){
        Bundle bundle = new Bundle();
        bundle.putString("peso", peso);
        bundle.putString("altura", altura);
        bundle.putInt("idTipoVientre", idTipoVientre);
        bundle.putInt("idTipoPecho", idTipoPecho);
        bundle.putString("edad", edad);
        bundle.putInt("idTipoCamisa", fit);
        return bundle;
    }

    private JsonObject setTipoCamisa(int tipoCamisa){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("tipoCamisaMedidaId",tipoCamisa);
        return requestBody;
    }


    private void actualizarTipoCamisa(int id, JsonObject jsonObject){
        MeasurementServer.getInstance(mContext).actualizarMedidas(id, jsonObject, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView,"Alerta", message, R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                DialogAlerta.createDialogOK("Alerta", message, mContext);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }
}