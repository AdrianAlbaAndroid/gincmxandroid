package mx.ginc.views.mymeasurements;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.JsonObject;

import mx.ginc.R;
import mx.ginc.data.entities.Medidas;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.FragmentCuelloPechoBinding;
import mx.ginc.databinding.FragmentTipoCamisaBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.views.LoginActivity;

public class CuelloPechoFragment extends Fragment {

    private MeasurementViewModel mMeasurementViewModel = null;
    private Context mContext;
    private MisMedidasActivity activity;
    private FragmentCuelloPechoBinding mBinding;
    private View mView;
    private String peso = "";
    private String altura = "";
    private int idTipoVientre = 0;
    private int idTipoPecho = 0;
    private int idTipoCamisa = 0;
    private String edad  = "";
    private JsonObject medidasJson = null;
    private MedidasAll medida;
    private Boolean editar;

    public CuelloPechoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_cuello_pecho, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        peso = getArguments().getString("peso");
        altura = getArguments().getString("altura");
        idTipoVientre = getArguments().getInt("idTipoVientre");
        idTipoPecho = getArguments().getInt("idTipoPecho");
        idTipoCamisa = getArguments().getInt("idTipoCamisa");
        edad = getArguments().getString("edad");
        editar = getArguments().getBoolean("editar");
        mView = view;
        initView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    private void initView(){
        activity.setTitleToolbar(getString(R.string.cuello_pecho));
        if(editar){
            medida = (MedidasAll) getArguments().getSerializable("medida");
            mBinding.Cuello.setText(medida.getCuello());
            mBinding.Pecho.setText(medida.getPecho());
            mBinding.LargaManga.setText(medida.getLargoManga());
            mBinding.btnGuardar.setText("Guardar");
        }
        mBinding.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cuello = mBinding.Cuello.getText().toString();
                String pecho = mBinding.Pecho.getText().toString();
                String largaManga = mBinding.LargaManga.getText().toString();
                if(validaInfo(cuello,pecho,largaManga)){
                    hideKeyboardFrom(mContext, mView);
                    medidasJson =  setMedidasJson( MisMedidasObj("1", "Mis Medidas", altura, peso, idTipoVientre,
                            idTipoPecho,edad, idTipoCamisa, cuello, pecho, "1", "1", largaManga, "1", "1", "1", "1", "1", "1", "1", true));

                    if(editar){
                        actualizarMedidas(Integer.valueOf(medida.getId()), setMisMedidas(cuello, pecho, "1", "1", largaManga, "1", "1", "1", "1", "1", "1", "1", true));
                    }else{
                        insertarMedida();
                    }
                }
            }
        });
    }

    private void actualizarMedidas(int id, JsonObject jsonObject){
        MeasurementServer.getInstance(mContext).actualizarMedidas(id, jsonObject, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView,"Alerta", message, R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                DialogAlerta.createDialogOK("Alerta", message, mContext);
            }
        });
    }


    private Medidas MisMedidasObj(String id, String nombre, String altura, String peso, int tipoVientre,
                                  int tipoPecho, String edad, int tipoCamisaMedida, String cuello, String pecho,
                                  String cintura, String cadera, String largoManga, String largoMuneca, String munecaIzquierda, String munecaDerecha, String biceps,
                                  String espalda, String aida, String largo, Boolean preferente){
        return new Medidas(id,  nombre,  altura,  peso,  String.valueOf(tipoVientre),  String.valueOf(tipoPecho),  edad,   String.valueOf(tipoCamisaMedida),  cuello,  pecho, cintura,  cadera,  largoManga,  largoMuneca, munecaIzquierda, munecaDerecha, biceps, espalda,  aida,  largo, preferente);
    }

    private JsonObject setMisMedidas(String cuello, String pecho,
                                     String cintura, String cadera, String largoManga, String largoMuneca,
                                     String munecaIzquierda, String munecaDerecha, String biceps,
                                     String espalda, String aida, String largo, Boolean preferente){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("cuello", cuello);
        requestBody.addProperty("pecho",  pecho);
        requestBody.addProperty("cintura",  cintura);
        requestBody.addProperty("cadera",  cadera);
        requestBody.addProperty("largoManga", largoManga);
        requestBody.addProperty("largoMuneca",  largoMuneca);
        requestBody.addProperty("munecaIzquierda", munecaIzquierda);
        requestBody.addProperty("munecaDerecha",  munecaDerecha);
        requestBody.addProperty("biceps",  biceps);
        requestBody.addProperty("espalda",  espalda);
        requestBody.addProperty("aida",  aida);
        requestBody.addProperty("largo",  largo);
        requestBody.addProperty("principal",  preferente);
        return requestBody;
    }

    private JsonObject setMedidasJson(Medidas medidasEntry){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("id", medidasEntry.getId());
        requestBody.addProperty("nombre",  medidasEntry.getNombre());
        requestBody.addProperty("altura",  medidasEntry.getAltura());
        requestBody.addProperty("peso",  medidasEntry.getPeso());
        requestBody.addProperty("edad",  medidasEntry.getEdad());
        requestBody.addProperty("tipoVientreId", Integer.valueOf(medidasEntry.getTipoVientre()));
        requestBody.addProperty("tipoPechoId",  Integer.valueOf(medidasEntry.getTipoPecho()));
        requestBody.addProperty("tipoCamisaMedidaId", Integer.valueOf( medidasEntry.getTipoCamisaMedida()));
        requestBody.addProperty("cuello",  medidasEntry.getCuello());
        requestBody.addProperty("pecho",  medidasEntry.getPecho());
        requestBody.addProperty("cintura",  medidasEntry.getCintura());
        requestBody.addProperty("cadera",  medidasEntry.getCadera());
        requestBody.addProperty("largoManga",  medidasEntry.getLargoManga());
        requestBody.addProperty("largoMuneca",  medidasEntry.getLargoMuneca());
        requestBody.addProperty("munecaIzquierda",  medidasEntry.getMunecaIzquierda());
        requestBody.addProperty("munecaDerecha",  medidasEntry.getMunecaDerecha());
        requestBody.addProperty("biceps",  medidasEntry.getBiceps());
        requestBody.addProperty("espalda",  medidasEntry.getEspalda());
        requestBody.addProperty("aida",  medidasEntry.getAida());
        requestBody.addProperty("largo",  medidasEntry.getLargo());
        requestBody.addProperty("principal",  medidasEntry.getPrincipal());
        return requestBody;
    }

    private static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void insertarMedida(){
        MeasurementServer.getInstance(mContext).addMedida(medidasJson, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView,"Alerta", message, R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                if (message.equals(getString(R.string.token_requerido)) || message.equals(getString(R.string.token_expirado))) {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("activity","MyMeasurementActivity");
                    startActivity(intent);
                } else {
                    DialogAlerta.createDialogOK("Alerta", message, mContext);
                }
            }
        });
    }


    private boolean validaInfo(String cuello, String pecho, String largoManga){
        if(cuello.equals("")){
            DialogAlerta.createDialogOK("Alerta","El campo cuello esta vacio", mContext);
            return false;
        }
        if(pecho.equals("")){
            DialogAlerta.createDialogOK("Alerta","El campo pecho esta vacio", mContext);
            return false;
        }
        if(largoManga.equals("")){
            DialogAlerta.createDialogOK("Alerta","El campo largo de manga esta vacio",mContext);
            return false;
        }
        return true;
    }
}