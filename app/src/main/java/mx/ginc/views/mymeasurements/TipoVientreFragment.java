package mx.ginc.views.mymeasurements;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;

import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.ViewPagerAdapter;
import mx.ginc.data.entities.Belly;
import mx.ginc.data.entities.MedidasAll;
import mx.ginc.data.network.MeasurementServer;
import mx.ginc.databinding.FragmentTipoVientreBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class TipoVientreFragment extends Fragment {

    private ViewPagerAdapter mAdapter;
    private MeasurementViewModel mMeasurementViewModel = null;
    private Context mContext;
    private MisMedidasActivity activity;
    private View mView;
    private FragmentTipoVientreBinding mBinding;
    private String peso = "";
    private String altura = "";
    private MedidasAll medida;
    private Boolean editar;

    public TipoVientreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = (MisMedidasActivity) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_tipo_vientre, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
        mView = view;
        activity.setTitleToolbar(getString(R.string.tipo_vientre));
        editar = getArguments().getBoolean("editar");
        medida = (MedidasAll) getArguments().getSerializable("medida");
        peso = getArguments().getString("peso");
        altura = getArguments().getString("altura");
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMeasurementViewModel = ViewModelProviders.of(activity).get(MeasurementViewModel.class);
            }
        });
        getTipoVientre();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MisMedidasActivity) getActivity();
    }

    private void getTipoVientre(){
        mMeasurementViewModel.getBellyMeasurement(mContext).observe(activity, new BaseObserver<List<Belly>>() {
            @Override
            public void onChanged(List<Belly> bellies) {
                mAdapter = new ViewPagerAdapter(mContext, bellies, new ViewPagerAdapter.SelectionCallBack() {
                    @Override
                    public void onSelected(@Nullable Belly item) {
                        if(editar){
                            actualizarTipoVientre(Integer.valueOf(medida.getId()), setTipoVientre(item.getId()));
                        }else{
                            Navigation.findNavController(mView).navigate(R.id.tipoPechoFragment, setArgs(item.getId()));
                        }

                    }
                }, false, new ViewPagerAdapter.FLagsCallBack() {
                    @Override
                    public void onSelectedFlag(int position) {
                        mBinding.viewPager2.setCurrentItem(position);
                    }
                });
                mBinding.viewPager2.setAdapter(mAdapter);
                mMeasurementViewModel.getBellyMeasurement(mContext).removeObservers(activity);
            }
        });
    }

    private JsonObject setTipoVientre(int tipoVientre){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("tipoVientreId",tipoVientre);
        return requestBody;
    }

    private Bundle setArgs(int idTipoVientre){
        Bundle bundle = new Bundle();
        bundle.putString("peso", peso);
        bundle.putString("altura", altura);
        bundle.putInt("idTipoVientre", idTipoVientre);
        return bundle;
    }


    private void actualizarTipoVientre(int id, JsonObject jsonObject){
        MeasurementServer.getInstance(mContext).actualizarMedidas(id, jsonObject, new MeasurementServer.MeasurementCallBack() {
            @Override
            public void onSuccess(String message) {
                DialogAlerta.init(mContext).createDialogOK(mView, "Alerta", message, R.id.medidaFragment);
            }

            @Override
            public void onFailure(String message) {
                DialogAlerta.createDialogOK("Alerta", message, mContext);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinding = null;
    }
}