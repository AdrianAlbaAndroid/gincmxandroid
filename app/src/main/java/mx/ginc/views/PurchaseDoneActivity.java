package mx.ginc.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.databinding.ActivityDoneBinding;
import mx.ginc.viewmodel.ProductViewModel;

public class PurchaseDoneActivity extends AppCompatActivity {

    private ActivityDoneBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private String invoiceNumber = "";
    private String status = "";
    private String directioSend = "";
    private String personReceiver = "";
    private String priceFinal = "";
    private ProductViewModel mProductBuyViewModel  = null;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);
        invoiceNumber = getIntent().getStringExtra("invoiceNumber");
        status = getIntent().getStringExtra("statusPedido");
        directioSend = getIntent().getStringExtra("directioSend");
        personReceiver = getIntent().getStringExtra("personReceiver");
        priceFinal = getIntent().getStringExtra("priceFinal");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(PurchaseDoneActivity.this).get(ProductViewModel.class);
            }
        });
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_done);
        pref = new AppPreferenceManager(PurchaseDoneActivity.this);
        mBinding.txtPedido.setText(status);
        if (status.equals("Pedido Confirmado")) {
            mBinding.imgOK.setImageResource(R.drawable.ok);
            mBinding.NoPedido.setText("Número de pedido: " + invoiceNumber);
            mBinding.QuienRecibe.setText(personReceiver);
            mBinding.Direccion.setText(directioSend);
            mBinding.PagoRealizado.setText("Pago realizado: " + priceFinal);
            mBinding.btnRegresarCarrito.setVisibility(View.GONE);
            pref.setProductCount(0);
            mProductBuyViewModel.deleteAll();
        }else if(status.equals("Pago cancelado")){
            mBinding.imgOK.setImageResource(R.drawable.cancel);
            mBinding.NoPedido.setVisibility(View.GONE);
            mBinding.QuienRecibe.setVisibility(View.GONE);
            mBinding.Direccion.setVisibility(View.GONE);
            mBinding.datosEnvio.setVisibility(View.GONE);
            mBinding.PagoRealizado.setVisibility(View.GONE);
            mBinding.btnRegresarCarrito.setVisibility(View.VISIBLE);
        }else{
            mBinding.imgOK.setVisibility(View.GONE);
            mBinding.NoPedido.setVisibility(View.GONE);
            mBinding.QuienRecibe.setVisibility(View.GONE);
            mBinding.Direccion.setVisibility(View.GONE);
            mBinding.datosEnvio.setVisibility(View.GONE);
            mBinding.PagoRealizado.setVisibility(View.GONE);
            mBinding.btnRegresarCarrito.setVisibility(View.VISIBLE);
        }
        mBinding.btnRegresarCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PurchaseDoneActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mBinding.btnIrMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PurchaseDoneActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
        initToolbar();
    }


    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
    }
}