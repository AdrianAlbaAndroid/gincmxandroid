package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.gson.JsonObject;

import java.util.Arrays;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.ProviderType;
import mx.ginc.data.entities.User;
import mx.ginc.data.network.UserServer;
import mx.ginc.databinding.ActivityLoginBinding;
import mx.ginc.views.fragments.OlvidoContrasenaFragment;

public class LoginActivity extends AppCompatActivity {

    private ActivityLoginBinding mLoginBinding;
    private  String usuario = "";
    private  String password = "";
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private static final int GOOGLE_SIGN_IN = 100;
    private static final String TAG = "FacebookLogin";
    private String nameActivity;
    private AccessTokenTracker accessTokenTracker;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        FirebaseApp.initializeApp(this);
        FacebookSdk.sdkInitialize(this);
    }

    private void initView(){
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        mLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login);
        pref = new AppPreferenceManager(LoginActivity.this);
        nameActivity = getIntent().getStringExtra("activity");
        mLoginBinding.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usuario = mLoginBinding.edtusuario.getText().toString();
                password = mLoginBinding.edtContrasena.getText().toString();
                if(!isValid(usuario,password)){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            createDialog("Alerta", "Llenar todos los campos vacios");
                        }
                    });

                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            UserServer.getInstance(LoginActivity.this).setUserLogin(setUserObject(usuario, password), new UserServer.UserCallBack() {
                                @Override
                                public void onSuccess(User user) {
                                    Log.i("ACCESS_TOKEN", user.getAccess_token());
                                    Log.i("FULL_NAME", user.getNombre());
                                    pref.saveToken(user.getAccess_token());
                                    pref.saveFullName(user.getNombre());
                                    pref.saveProfile(ProviderType.BASIC.name());
                                    finish();
                                }

                                @Override
                                public void onFailure(String mensaje) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            createDialog("Alerta", mensaje);
                                        }
                                    });
                                }
                            });
                        }
                    });

                }
            }
        });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    mAuth.signOut();
                }
            }
        };

        mLoginBinding.imgfacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList( "email", "public_profile"));
                LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if(loginResult != null){
                            AccessToken token = loginResult.getAccessToken();
                            Log.i("TOken :", pref.getToken());
                            AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
                            mAuth.signInWithCredential(credential)
                                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                // Sign in success, update UI with the signed-in user's information
                                                FirebaseUser user = mAuth.getCurrentUser();
                                                UserServer.getInstance(LoginActivity.this).setUserProvider(setUserProvider(user), ProviderType.FACEBOOK.name(), new UserServer.UserCallBack() {
                                                    @Override
                                                    public void onSuccess(User user) {
                                                        if(user.getAccess_token() != null){
                                                            Log.i("ACCESS_TOKEN", user.getAccess_token());
                                                            Log.i("FULL_NAME", user.getNombre());
                                                            pref.saveToken(user.getAccess_token());
                                                            pref.saveFullName(user.getNombre());
                                                            pref.saveProfile(ProviderType.FACEBOOK.name());
                                                            Log.i("TOKEN_SAVE", pref.getToken());
                                                            finish();
                                                        }else{
                                                            createDialog("Alerta", "Usuario o contraseña no validos");
                                                        }
                                                    }

                                                    @Override
                                                    public void onFailure(String mensaje) {
                                                        createDialog("Alerta", mensaje);
                                                    }
                                                });
                                            } else {
                                                // If sign in fails, display a message to the user.
                                                Toast.makeText(LoginActivity.this, "Authentication failed.",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }

                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException error) {
                        System.out.println("Error Facebook " + error.getMessage());
                        createDialog("Mensaje", "Se ha producido un error.");
                    }
                });
            }
        });

        mLoginBinding.imggoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();

                mGoogleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);
                mGoogleSignInClient.signOut();
                FirebaseAuth.getInstance().signOut();
                startActivityForResult(mGoogleSignInClient.getSignInIntent(), GOOGLE_SIGN_IN);
            }
        });

        mLoginBinding.btnNuevoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, AccountActivity.class);
                startActivity(intent);
                finish();
            }
        });

        mLoginBinding.olvideContraseA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OlvidoContrasenaFragment dialogFragment = OlvidoContrasenaFragment
                        .newInstance(LoginActivity.this);
                dialogFragment.show(getSupportFragmentManager(), "dialog");
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, UI will update with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(LoginActivity.this, "Authentication Succeeded.", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign-in fails, a message will display to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        if(mCallbackManager != null){
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if(requestCode == GOOGLE_SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                if(account !=null){
                    AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                    mAuth.signInWithCredential(credential)
                            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        UserServer.getInstance(LoginActivity.this).setUserProvider(setUserProvider(user), ProviderType.GOOGLE.name(), new UserServer.UserCallBack() {
                                            @Override
                                            public void onSuccess(User user) {
                                                if(user.getAccess_token() != null){
                                                    Log.i("ACCESS_TOKEN", user.getAccess_token());
                                                    Log.i("FULL_NAME", user.getNombre());
                                                    pref.saveToken(user.getAccess_token());
                                                    pref.saveFullName(user.getNombre());
                                                    pref.saveProfile(ProviderType.GOOGLE.name());
                                                    finish();
                                                }else{
                                                    createDialog("Alerta", "Usuario o contraseña no validos");
                                                }
                                            }

                                            @Override
                                            public void onFailure(String mensaje) {
                                                createDialog("Alerta", mensaje);
                                            }
                                        });
                                    } else {
                                        createDialog("Mensaje", "Se ha producido un error.");
                                    }
                                }
                            });
                }
            } catch (ApiException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private boolean isValid(String usuario, String password){
        boolean valid;
        if(usuario.isEmpty() && password.isEmpty()){
             valid = false;
        }else if(usuario.isEmpty()){
            valid = false;
        }
        else if(password.isEmpty()){
            valid = false;
        }else{
            valid = true;
        }
        return valid;
    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK",null);
        builder.create();
        builder.show();
    }

    private JsonObject setUserObject(String user, String password){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("usuario", user);
        requestBody.addProperty("password", password);
        return requestBody;
    }

    private JsonObject setUserProvider(FirebaseUser user){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("id", user.getUid());
        requestBody.addProperty("email", user.getEmail());
        requestBody.addProperty("name", user.getDisplayName());
        return requestBody;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*if(nameActivity != null){
            if(nameActivity.equals("CuentaFragment")){
                Intent intentMedidas = new Intent(this, HomeActivity.class);
                intentMedidas.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentMedidas);
                finish();
            }else{
                finish();
            }
        }else{
            finish();
        }*/
        Intent intentMedidas = new Intent(this, HomeActivity.class);
        intentMedidas.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentMedidas);
        finish();

    }

}

