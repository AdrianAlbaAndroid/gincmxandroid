package mx.ginc.views;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import mx.ginc.R;
import mx.ginc.adapter.ConfirmationAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.LoadingProgress;
import mx.ginc.data.entities.Checkout;
import mx.ginc.data.entities.DirectionAll;
import mx.ginc.data.local.ProductSection;
import mx.ginc.data.network.DirectionServer;
import mx.ginc.data.network.PurchaseServer;
import mx.ginc.databinding.ActivityConfimationBinding;
import mx.ginc.viewmodel.DirectionViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class PurchaseConfirmationActivity  extends AppCompatActivity implements BaseObserver<List<ProductSection>> {

    private static final String TAG = "CONFIRMACION";
    private ConfirmationAdapter adapter;
    private ActivityConfimationBinding mBinding;
    private ProductViewModel mProductBuyViewModel = null;
    private DirectionViewModel mDirectionViewModel = null;
    double amountFinal = 0;
    private String directionObject = "";
    private String priceFinal = "";
    private List<ProductSection> listadoProductos = null;
    private String directioSend = "";
    private String personReceiver = "";
    private final LoadingProgress loadingProgress = new LoadingProgress(PurchaseConfirmationActivity.this, PurchaseConfirmationActivity.this);
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confimation);
        pref = new AppPreferenceManager(PurchaseConfirmationActivity.this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(PurchaseConfirmationActivity.this).get(ProductViewModel.class);
                mDirectionViewModel = ViewModelProviders.of(PurchaseConfirmationActivity.this).get(DirectionViewModel.class);
            }
        });
        mProductBuyViewModel.getAllProducts().observe(this,this);
        mBinding.crdDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PurchaseConfirmationActivity.this, DirectionActivity.class);
                startActivity(intent);
            }
        });
        mBinding.btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckoutProduct();
            }
        });
        mBinding.btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PurchaseConfirmationActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        getDirection();
        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDirection();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getDirection();
    }

    public void getDirection(){
        DirectionServer.getInstance(this).getDirectionFirst(new DirectionServer.DirectionAllCallBack() {
            @Override
            public void onSuccess(List<DirectionAll> listaDireccion) {
                List<DirectionAll> directionAll = getDirectionPrincipal(listaDireccion);
                for(DirectionAll d : directionAll){
                    if(d.getPrincipal()){
                        directioSend = d.getCallePrincipal() + ", " +
                                d.getNumeroExterior() + ", " +
                                d.getNumeroInteriorDepto() + ", " +
                                d.getMunicipio() + ", " +
                                d.getColonia() + ", " +
                                d.getCodigoPostal() + ", " +
                                d.getEntidad();
                        personReceiver = d.getQuienRecibe();
                        directionObject = d.getJsonObject();
                    }
                }
                if(listaDireccion.size() != 0){
                    if(getDirectionPrincipal(listaDireccion).size() != 0){
                        getDirection(getDirectionPrincipal(listaDireccion));
                    }else{
                        getDirection(listaDireccion.stream().findFirst().get());
                        directioSend = listaDireccion.stream().findFirst().get().getCallePrincipal() + ", " +
                                listaDireccion.stream().findFirst().get().getNumeroExterior() + ", " +
                                listaDireccion.stream().findFirst().get().getNumeroInteriorDepto() + ", " +
                                listaDireccion.stream().findFirst().get().getMunicipio() + ", " +
                                listaDireccion.stream().findFirst().get().getColonia() + ", " +
                                listaDireccion.stream().findFirst().get().getCodigoPostal() + ", " +
                                listaDireccion.stream().findFirst().get().getEntidad();
                        personReceiver =  listaDireccion.stream().findFirst().get().getQuienRecibe();
                        directionObject = listaDireccion.stream().findFirst().get().getJsonObject();
                    }
                }else{
                    mBinding.PredeterinadoSelect.setText("Para terminar su compra es necesario agregar una dirección");
                    mBinding.PredeterinadoSelect.setTextColor(Color.RED);
                    mBinding.Nombre.setVisibility(View.GONE);
                    mBinding.Direccion.setVisibility(View.GONE);
                }
            }
        });
    }

    public void CheckoutProduct(){
        if(!directionObject.equals("")){
            showProgressBar();
            JsonObject jsonObject = setConfirmation(directionObject,listadoProductos);
            String jsonString =  jsonObject.toString().replace("\\", "");
            JsonObject jsonObjectFinal = new JsonParser().parse(jsonString).getAsJsonObject();
            Log.i(TAG, jsonObjectFinal.toString());
            PurchaseServer.getInstance(PurchaseConfirmationActivity.this).setCheckout(jsonObjectFinal, new PurchaseServer.PurchaseCallBack() {
                @Override
                public void onSuccess(String paymentUrl, String invoiceNumber) {
                    Intent intent = new Intent(PurchaseConfirmationActivity.this, PayPalActivity.class);
                    intent.putExtra("paymentUrl", paymentUrl);
                    intent.putExtra("invoiceNumber", invoiceNumber);
                    intent.putExtra("directioSend", directioSend);
                    intent.putExtra("personReceiver", personReceiver);
                    intent.putExtra("priceFinal", priceFinal);
                    startActivity(intent);
                    hideProgressBar();
                }

                @Override
                public void onFailure(JSONObject jsonObject) {
                    try {
                        hideProgressBar();
                        if(jsonObject.getInt("http") == 401) {
                            Intent intent = new Intent(PurchaseConfirmationActivity.this, LoginActivity.class);
                            intent.putExtra("activity","PurchaseConfirmationActivity");
                            startActivity(intent);
                        }else{
                            if(directionObject == null){
                                createDialog("Alerta", "Falta agregar la dirección de envio para poder finalizar la compra");
                            }else{
                                createDialog("Alerta", jsonObject.getString("mensaje"));
                            }
                        }
                    } catch (JSONException e) {
                        createDialog("Alerta", "Error al traer los datos");
                    }

                }
            });
        }else{
            createDialog("Alerta", "Falta agregar la dirección para el envio");
        }

    }

    private void showProgressBar(){
        loadingProgress.showDialog(getString(R.string.message_loading));
    }

    private void hideProgressBar(){
        loadingProgress.dimissbar();
    }

    public List<DirectionAll> getDirectionPrincipal(List<DirectionAll> directionAlls){
        return directionAlls.stream()
                .filter(x -> x.getPrincipal()== true)
                .collect(Collectors.toList());
    }

    public void getDirection(List<DirectionAll> directionAll){
        if(directionAll.size() != 0){
            for(int k = 0; k < directionAll.size(); k++){
                mBinding.Nombre.setText(directionAll.get(k).getQuienRecibe());
                mBinding.Direccion.setText(directionAll.get(k).getCallePrincipal() + ", " +
                        directionAll.get(k).getNumeroExterior() + ", " +
                        directionAll.get(k).getNumeroInteriorDepto() + ", " +
                        directionAll.get(k).getMunicipio() + ", " +
                        directionAll.get(k).getColonia() + ", " +
                        directionAll.get(k).getCodigoPostal() + ", " +
                        directionAll.get(k).getEntidad());
            }
        }else{
            mBinding.PredeterinadoSelect.setText("Para terminar su compra es necesario agregar una dirección");
            mBinding.PredeterinadoSelect.setTextColor(Color.RED);
            mBinding.Nombre.setVisibility(View.GONE);
            mBinding.Direccion.setVisibility(View.GONE);
        }

    }

    public void getDirection(DirectionAll directionAll){
        if(directionAll != null){
            mBinding.Nombre.setText(directionAll.getQuienRecibe());
            mBinding.Direccion.setText(directionAll.getCallePrincipal() + ", " +
                    directionAll.getNumeroExterior() + ", " +
                    directionAll.getNumeroInteriorDepto() + ", " +
                    directionAll.getMunicipio() + ", " +
                    directionAll.getColonia() + ", " +
                    directionAll.getCodigoPostal() + ", " +
                    directionAll.getEntidad());
        }else{
            mBinding.PredeterinadoSelect.setText("Para terminar su compra es necesario agregar una dirección");
            mBinding.PredeterinadoSelect.setTextColor(Color.RED);
            mBinding.Nombre.setVisibility(View.GONE);
            mBinding.Direccion.setVisibility(View.GONE);
        }

    }

    private void createDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK",null);
        builder.create();
        builder.show();
    }


    private void getPriceTotal(List<ProductSection> productSections){
        double priceProduct = 0;
        double priceTotal = 0;
        for(int i = 0; i < productSections.size(); i++){
            int amount = productSections.get(i).getAmount();
            double price = productSections.get(i).getPrice_base();
            priceProduct= price * amount;
            priceTotal += priceProduct;
        }
        amountFinal = priceTotal;
        priceFinal = AppUtil.customFormat("$ ###,###.00", priceTotal);
        mBinding.priceSubtotal.setText(AppUtil.customFormat("$ ###,###.00", priceTotal));
        mBinding.Total.setText(AppUtil.customFormat("$ ###,###.00", priceTotal));
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void getShoppingCart(List<ProductSection> cartList) {
        adapter = new ConfirmationAdapter(PurchaseConfirmationActivity.this, PurchaseConfirmationActivity.this, cartList);
        mBinding.recyclerviewCompra.setHasFixedSize(true);
        mBinding.recyclerviewCompra.setLayoutManager(new LinearLayoutManager(PurchaseConfirmationActivity.this, LinearLayoutManager.VERTICAL, false));
        mBinding.recyclerviewCompra.setItemViewCacheSize(20);
        mBinding.recyclerviewCompra.setDrawingCacheEnabled(true);
        mBinding.recyclerviewCompra.setAdapter(adapter);
    }

    @Override
    public void onChanged(List<ProductSection> productSections) {
        mBinding.txtProductos.setText("Listado de productos (" + String.valueOf(pref.getProductCount()) + ")");
        getShoppingCart(productSections);
        getPriceTotal(productSections);
        listadoProductos = productSections;
        mProductBuyViewModel.getAllProducts().removeObservers(this);
    }

    private JsonObject setConfirmation(String jsonDirection, List<ProductSection> items){
        JsonObject jsonObject = new JsonParser().parse(jsonDirection).getAsJsonObject();
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("id",1);
        requestBody.add("direccionEntrega",jsonObject);
        requestBody.add("itemsDeCarritoCatalogo", setArrayCatalogo(items));
        requestBody.add("itemsDeCarritoPersonaliza", setArrayPersonaliza(items));
        requestBody.addProperty("cantidadTotalDeItems", items.size());
        return requestBody;
    }

    private JsonObject setProductCatalog(ProductSection productSection){
        JsonObject jsonObject = new JsonParser().parse(productSection.getObjProduct()).getAsJsonObject();
        JsonArray jsonArray = new JsonParser().parse(productSection.getObjEspecificaciones()).getAsJsonArray();
        JsonObject requestBody = new JsonObject();
        requestBody.add("producto", jsonObject);
        requestBody.add("especificaciones", jsonArray);
        requestBody.addProperty("cantidad",  productSection.getAmount());
        requestBody.addProperty("costo",  productSection.getPrice_final());
        return requestBody;
    }

    private JsonObject setCustomizer(ProductSection productSection){
        String[] parts = productSection.getObjProduct().split(",");
        Gson gson = new GsonBuilder().serializeNulls().create();
        String mJsonObject =  gson.toJson(Arrays.asList(parts));
        JsonObject jsonObjectMedida = new JsonParser().parse(productSection.getObjEspecificaciones()).getAsJsonObject();
        JsonArray jsonArrayIdsCustomer = new JsonParser().parse(mJsonObject).getAsJsonArray();
        JsonObject requestBody = new JsonObject();
        requestBody.add("tipoCustomizaciones", jsonArrayIdsCustomer);
        requestBody.add("misMedidas", jsonObjectMedida);
        requestBody.addProperty("cantidad",  productSection.getAmount());
        return requestBody;
    }

    private JsonArray setArrayCatalogo(List<ProductSection> productSections) {
        List<JsonObject> itemsCatalogo = new ArrayList<>();
        for(ProductSection p : productSections){
            if(p.getTipo().equals(AppUtil.TipoProducto.CATALOGO)){
                itemsCatalogo.add(setProductCatalog(p));
            }
        }
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonArray myCustomArray = gson.toJsonTree(itemsCatalogo).getAsJsonArray();
        return myCustomArray;
    }

    private JsonArray setArrayPersonaliza(List<ProductSection> productSections) {
        List<JsonObject> itemsCatalogo = new ArrayList<>();
        for(ProductSection p : productSections){
            if(p.getTipo().equals(AppUtil.TipoProducto.PERSONALIZADO)){
                itemsCatalogo.add(setCustomizer(p));
            }
        }
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonArray myCustomArray = gson.toJsonTree(itemsCatalogo).getAsJsonArray();
        return myCustomArray;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();  return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

}