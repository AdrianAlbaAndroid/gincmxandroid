package mx.ginc.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.LoadingProgress;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ActivityClassicMenuBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MeasurementViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.views.mymeasurements.MisMedidasActivity;

public class ClassicActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ActivityClassicMenuBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private String tipo;
    private ProductViewModel mProductBuyViewModel  = null;
    private int id = 0;
    private final LoadingProgress loadingProgress = new LoadingProgress(ClassicActivity.this,ClassicActivity.this);
    private  int idProduct = 0;
    private ArrayList<String> mCustomizerIds = null;
    private MeasurementViewModel mMeasurementViewModel = null;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_classic_menu);
        pref = new AppPreferenceManager(ClassicActivity.this);
        tipo = getIntent().getStringExtra("tipo");
        mBinding.progressBar.setVisibility(View.GONE);
        initToolbar();
        onMenuClick();
    }

    private void saveIds(String mangas){
        if(mangas.equals("Con mangas")){
            pref.saveArrayList(getPayLoadConMangas(),AppUtil.Constants.ID_CUSTOMIZER);
        }else if(mangas.equals("Sin mangas")){
            pref.saveArrayList(getPayLoadSinMangas(),AppUtil.Constants.ID_CUSTOMIZER);
        }
    }

    private ArrayList<String> getPayLoadConMangas(){
        ArrayList<String> mCustomizerIds = new ArrayList<>();
        String payload = "159,223,273,474,526,728,815,632,843,774,909";
        String[] parts = payload.split(",");
        for (int i = 0; i < parts.length; i++) {
            mCustomizerIds.add(parts[i]);
        }
        return mCustomizerIds;
    }

    private ArrayList<String> getPayLoadSinMangas(){
        ArrayList<String> mCustomizerIds = new ArrayList<>();
        String payload = "962,1062,1010,1262,1312,1425,1329,1357,1343";
        String[] parts = payload.split(",");
        for (int i = 0; i < parts.length; i++) {
            mCustomizerIds.add(parts[i]);
        }
        return mCustomizerIds;
    }

    public void onMenuClick(){
        getImageMenu();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProductBuyViewModel = ViewModelProviders.of(ClassicActivity.this).get(ProductViewModel.class);
                mMeasurementViewModel = ViewModelProviders.of(ClassicActivity.this).get(MeasurementViewModel.class);
            }
        });
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        mBinding.rltClasica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtil.isOnline(ClassicActivity.this)) {
                    DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi),ClassicActivity.this);
                }else{
                    openCustomizer(3,"Con mangas");
                }
            }
        });
        mBinding.rltGinc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppUtil.isOnline(ClassicActivity.this)) {
                    DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi),ClassicActivity.this);
                }else{
                    openCustomizer(36,"Sin mangas");
                }
            }
        });
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ClassicActivity.this, ShoppingCartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }
        private void insertarMangaLarga(int id, String tipoManga) {
            ProductServer.getInstance().getMenuItemFirst(id, new ProductServer.CustomizerCallBack() {
                @Override
                public void onSuccess(List<Customizer> mListCustomizer) {
                    for(int i = 0; i < mListCustomizer.size(); i++) {
                        CustomizedProduct product = new CustomizedProduct();
                        product.setIdProduct(mListCustomizer.get(i).getId());
                        product.setCustomizedName(mListCustomizer.get(i).getEtiqueta());
                        product.setUrlIcon(mListCustomizer.get(i).getUrlIcon());
                        product.setIndexProduct(mListCustomizer.get(i).getIndex());
                        product.setSelected(false);
                        product.setItemSelected("");
                        product.setTipoManga(tipoManga);
                        mProductBuyViewModel.insertCustomizedProduct(product);
                    }
                }
            });
        }


    private void openCustomizer(int id, String mangas){
        pref.saveArrayList(null, AppUtil.Constants.ID_CUSTOMIZER);
        saveIds(mangas);
        mBinding.progressBar.setVisibility(View.GONE);
        Intent intent = new Intent(getApplicationContext(), CustomerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", id);
        intent.putExtra("tipoManga",mangas);
        intent.putExtra("tipo",tipo);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void getImageMenu(){
        mBinding.imgConMangas.setImageResource(R.drawable.camisa_con_mangas);
        mBinding.imgSinMangas.setImageResource(R.drawable.camisa_sin_mangas);
       // Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/camisa1.jpg").into(mBinding.imgGinc);
        //Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/camisa2.jpg").into(mBinding.imgClasica);
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_menu);
        getNavigationDrawer();
    }

    public void getNavigationDrawer(){
        getHeaderView(pref.getFullName());
        mToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawerLayout.addDrawerListener(mToggle);
        mBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (!AppUtil.isOnline(ClassicActivity.this)) {
            DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), ClassicActivity.this);
        }else{
            switch (menuItem.getItemId()) {
                case R.id.nav_inicio:
                    Intent intent = new Intent(this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                    break;
                case R.id.nav_pedidos:
                    setMenu(R.id.nav_pedidos);
                    break;
                case R.id.nav_medidas:
                    Intent intentMedidas = new Intent(this, MisMedidasActivity.class);
                    intentMedidas.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentMedidas);
                    break;
                case R.id.nav_funciona:
                    setMenu(R.id.nav_funciona);
                    break;
                case R.id.nav_medirme:
                    setMenu(R.id.nav_medirme);
                    break;
                case R.id.nav_cuenta:
                    setMenu(R.id.nav_cuenta);
                    break;
                case R.id.nav_terminos:
                    setMenu(R.id.nav_terminos);
                    break;
                case R.id.nav_contacto:
                    setMenu(R.id.nav_contacto);
                    break;
                case R.id.nav_politicas_privacidad:
                    setMenu(R.id.nav_politicas_privacidad);
                    break;
                case R.id.nav_sesion:
                    Intent intentHome = new Intent(this, HomeActivity.class);
                    intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentHome);
                    pref.removeToken();
                    pref.removeFullName();
                    getHeaderView(pref.getFullName());
                    finish();
                    break;
            }
        }
        mBinding.drawerLayout.closeDrawers();
        return true;
    }

    private void setMenu(int option){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("opcion", option);
        startActivity(intent);
    }
}