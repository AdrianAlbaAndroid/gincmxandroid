package mx.ginc.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

import mx.ginc.R;
import mx.ginc.data.network.PurchaseServer;
import mx.ginc.databinding.ActivityPaymentBinding;

public class PayPalActivity extends AppCompatActivity  {

    private static final String TAG = "PAGO";
    private ActivityPaymentBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private String paymentUrl = "";
    private String invoiceNumber = "";
    private String directioSend = "";
    private String personReceiver = "";
    private String priceFinal = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        paymentUrl = getIntent().getStringExtra("paymentUrl");
        invoiceNumber = getIntent().getStringExtra("invoiceNumber");
        directioSend = getIntent().getStringExtra("directioSend");
        personReceiver = getIntent().getStringExtra("personReceiver");
        priceFinal = getIntent().getStringExtra("priceFinal");
        initView();
        getPayPalView(paymentUrl);
    }

    public void initView(){
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        mBinding.progressBar.setVisibility(View.GONE);
        initToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mBinding.progressBar.setVisibility(View.GONE);
    }

    private void getPayPalView(String paymentUrl){
        Log.i(TAG, paymentUrl);
        if(!paymentUrl.equals("")){
            mBinding.webviewPay.loadUrl(paymentUrl);
            WebSettings webSettings = mBinding.webviewPay.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webSettings.setDomStorageEnabled(true);
            mBinding.webviewPay.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    Log.i(TAG, url);
                    if(url.contains("NOT_YOU") || url.contains("/checkout/review")){
                        mBinding.webviewPay.loadUrl(url);
                        WebSettings webSettings = mBinding.webviewPay.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        webSettings.setDomStorageEnabled(true);
                    }else{
                        if(url.contains("/solicitud/compra/pay/") || url.contains("/solicitud/compra/cancel/")){
                            mBinding.progressBar.setVisibility(View.VISIBLE);
                            mBinding.webviewPay.setVisibility(View.GONE);
                            PurchaseServer.getInstance(PayPalActivity.this).setUrlPayment(url, new PurchaseServer.PayCallBack() {
                                @Override
                                public void onSuccess(String mensaje) {
                                    Log.i(TAG, mensaje);
                                    mBinding.progressBar.setVisibility(View.GONE);
                                    if(mensaje.equals(getString(R.string.pago_confirmado))){
                                        Intent intentPagado = new Intent(PayPalActivity.this, PurchaseDoneActivity.class);
                                        intentPagado.putExtra("invoiceNumber", invoiceNumber);
                                        intentPagado.putExtra("statusPedido", "Pedido Confirmado");
                                        intentPagado.putExtra("directioSend", directioSend);
                                        intentPagado.putExtra("personReceiver", personReceiver);
                                        intentPagado.putExtra("priceFinal", priceFinal);
                                        startActivity(intentPagado);
                                        finish();
                                    }else{
                                        Intent intentMensaje = new Intent(PayPalActivity.this, PurchaseDoneActivity.class);
                                        intentMensaje.putExtra("statusPedido", mensaje);
                                        startActivity(intentMensaje);
                                        finish();
                                    }
                                }
                            });
                        }

                    }
                    return false;
                }
            });
        }
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}