package mx.ginc.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;

import java.util.List;

import mx.ginc.R;
import mx.ginc.adapter.SubCatalogAdapter;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Product;
import mx.ginc.databinding.ActivitySubcategoriaBinding;
import mx.ginc.viewmodel.CatalogViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;

public class SubCatalogoActivity extends AppCompatActivity implements SubCatalogAdapter.SubCatalogIdCallBack, BaseObserver<List<Product>> {

    SubCatalogAdapter adapter;
    private ActivitySubcategoriaBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private CatalogViewModel mCatalogViewModel = null;
    private ProductViewModel mProductBuyViewModel  = null;
    private int id;
    private int idCategoria;
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategoria);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView(){
        pref = new AppPreferenceManager(SubCatalogoActivity.this);
        id = getIntent().getIntExtra("id", 0);
        idCategoria = getIntent().getIntExtra("idCategoria", 0);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_subcategoria);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCatalogViewModel = ViewModelProviders.of(SubCatalogoActivity.this).get(CatalogViewModel.class);
                mProductBuyViewModel = ViewModelProviders.of(SubCatalogoActivity.this).get(ProductViewModel.class);
            }
        });
        if(idCategoria == 1){
            mCatalogViewModel.getSubCatalog(setCatalogProduct(id)).observe(this, this);
        }else if ((idCategoria == 7) || (idCategoria == 8)){
            mCatalogViewModel.getSubCatalog(setCatalogProduct(idCategoria)).observe(this, this);
        }
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END);
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
        mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCatalogoActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });
        if((idCategoria == 7) || (idCategoria == 8)){
            mBinding.imgTelas.setVisibility(View.GONE);
        }else if(idCategoria == 1) {
            mBinding.imgTelas.setVisibility(View.VISIBLE);
        }
        initToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
    }

    private void getHeaderView(String nombreCompleto){
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void getSubCatalog(List<Product> mShirtList){
        adapter = new SubCatalogAdapter(this, this, mShirtList, this);
        mBinding.recyclerview.setHasFixedSize(true);
        mBinding.recyclerview.setLayoutManager(new GridLayoutManager(this, 2));
        mBinding.recyclerview.setItemViewCacheSize(20);
        mBinding.recyclerview.setDrawingCacheEnabled(true);
        mBinding.recyclerview.setAdapter(adapter);
    }

    private JsonObject setCatalogProduct(int categoria){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("length", 10);
        requestBody.addProperty("start", 0);
        requestBody.addProperty("etiqueta", "");
        requestBody.addProperty("categorias", String.valueOf(categoria));
        return requestBody;
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onChanged(List<Product> products) {
        getSubCatalog(products);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    @Override
    public void onSelected(Product note) {
        Intent intent = new Intent(this, CatalogoDetailActivity.class);
        intent.putExtra("id", note.getId());
        intent.putExtra("name", note.getEtiqueta());
        intent.putExtra("idCategoria",idCategoria);
        intent.putExtra("description",note.getDescripcion());
        intent.putExtra("objetoProducto",note.getProduct());
        startActivity(intent);
    }
}