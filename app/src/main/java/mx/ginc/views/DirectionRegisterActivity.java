package mx.ginc.views;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppUtil;
import mx.ginc.data.entities.Colonia;
import mx.ginc.data.entities.DirectionEntry;
import mx.ginc.data.entities.Especificaciones;
import mx.ginc.data.network.DirectionServer;
import mx.ginc.databinding.ActivityRegisterDirectionBinding;
import mx.ginc.dialog.DialogAlerta;

public class DirectionRegisterActivity extends AppCompatActivity {

    private ActivityRegisterDirectionBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private JsonObject tipoColoniaObject;
    private Boolean preferencia = false;
    private String selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_direction);
        initView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void initView() {
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_register_direction);
        mBinding.Colonia.setEnabled(false);
        mBinding.Estado.setEnabled(false);
        mBinding.Municipio.setEnabled(false);
        mBinding.CodigoPostal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mBinding.txtMessage.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    DirectionServer.getInstance(DirectionRegisterActivity.this).getColonia(s.toString(), new DirectionServer.ColoniaCallBack() {
                        @Override
                        public void onSuccess(List<Colonia> estado) {
                            showOptionsDialog(estado);
                        }

                        @Override
                        public void onFailure(String message) {
                            mBinding.txtMessage.setVisibility(View.VISIBLE);
                            mBinding.txtMessage.setText(message);
                        }
                    });
                } else {
                    mBinding.Colonia.setText("");
                    mBinding.Estado.setText("");
                    mBinding.Municipio.setText("");
                    mBinding.txtMessage.setVisibility(View.GONE);
                }
            }
        });
        initToolbar();
        mBinding.btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!mBinding.CodigoPostal.getText().toString().equals("")){
                     if(tipoColoniaObject != null){
                         DirectionEntry directionEntry = new DirectionEntry(0,
                                 mBinding.Nombre.getText().toString(),
                                 mBinding.Telefono.getText().toString(),
                                 tipoColoniaObject,
                                 mBinding.Calle.getText().toString(),
                                 mBinding.Calle1.getText().toString(),
                                 mBinding.Calle2.getText().toString(),
                                 mBinding.NumeroExterior.getText().toString(),
                                 mBinding.NumeroInterior.getText().toString(),
                                 mBinding.indicaciones.getText().toString(),
                                 preferencia);
                         JsonObject jsonObject = setDireccionEntrega(directionEntry);
                         DirectionServer.getInstance(DirectionRegisterActivity.this).setDirection(jsonObject, new DirectionServer.DirectionCallBack() {
                             @Override
                             public void onSuccess(String message) {
                                 createDialogAccept("Mensaje", message);
                             }

                             @Override
                             public void onFailure(String message) {
                                 DialogAlerta.createDialogOK("Mensaje", message,DirectionRegisterActivity.this);
                             }
                         });
                     }else{
                         DialogAlerta.createDialogOK("Mensaje", "Favor de validar la información ingresada",DirectionRegisterActivity.this);
                     }
                }
                else{
                    DialogAlerta.createDialogOK("Mensaje", "Favor de ingresar el código postal",DirectionRegisterActivity.this);
                }
            }
        });
        mBinding.switchPreferencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mBinding.switchPreferencia.isChecked()){
                    preferencia = true;
                }else{
                    preferencia = false;
                }
            }
        });
    }

    public void showOptionsDialog(List<Colonia> estado){
        //Create sequence of items
        final CharSequence[] colonias = AppUtil.getListColonia(estado).toArray(new String[estado.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DirectionRegisterActivity.this);
        dialogBuilder.setTitle("Colonias Encontradas");
        dialogBuilder.setItems(colonias, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                filtrarColonia(estado, colonias[item].toString());
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();

    }

    public void filtrarColonia(List<Colonia> coloniaAll, String colonia){
        coloniaAll.stream()
                .filter(c -> c.getNombreColonia().equals(colonia))
                .forEach((p)-> {
                    tipoColoniaObject = p.getJsonObject();
                    mBinding.Colonia.setText(p.getNombreColonia());
                    mBinding.Estado.setText(p.getEntidad());
                    mBinding.Municipio.setText(p.getMunicipio());
                    mBinding.txtMessage.setVisibility(View.GONE);
                    mBinding.Colonia.setEnabled(false);
                    mBinding.Estado.setEnabled(false);
                    mBinding.Municipio.setEnabled(false);
                });
    }

    private JsonObject setDireccionEntrega(DirectionEntry directionEntry){
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("quienRecibe", directionEntry.getQuienRecibe());
        requestBody.addProperty("telefono",  directionEntry.getTelefono());
        requestBody.add("tipoColonia",  directionEntry.getTipoColonia());
        requestBody.addProperty("callePrincipal",  directionEntry.getCallePrincipal());
        requestBody.addProperty("calle1",  directionEntry.getCalle1());
        requestBody.addProperty("calle2",  directionEntry.getCalle2());
        requestBody.addProperty("numeroExterior",  directionEntry.getNumeroExterior());
        requestBody.addProperty("numeroInteriorDepto",  directionEntry.getNumeroInteriorDepto());
        requestBody.addProperty("indicacionesAdicionales",  directionEntry.getIndicacionesAdicionales());
        requestBody.addProperty("principal",  directionEntry.getPrincipal());
        return requestBody;
    }
    private void createDialogAccept(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(DirectionRegisterActivity.this, DirectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        builder.create();
        builder.setCancelable(false);
        builder.show();
    }

    public void initToolbar() {
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}