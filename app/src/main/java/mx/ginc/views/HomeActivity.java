package mx.ginc.views;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.facebook.login.LoginManager;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import mx.ginc.R;
import mx.ginc.data.AppPreferenceManager;
import mx.ginc.data.AppUtil;
import mx.ginc.data.ProviderType;
import mx.ginc.data.entities.Customizer;
import mx.ginc.data.entities.Menu;
import mx.ginc.data.local.CustomizedProduct;
import mx.ginc.data.network.ProductServer;
import mx.ginc.databinding.ActivityMenuHomeBinding;
import mx.ginc.dialog.DialogAlerta;
import mx.ginc.viewmodel.MenuViewModel;
import mx.ginc.viewmodel.ProductViewModel;
import mx.ginc.viewmodel.base.BaseObserver;
import mx.ginc.views.mymeasurements.MisMedidasActivity;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BaseObserver<List<Menu>> {

    private ActivityMenuHomeBinding mBinding;
    private ActionBarDrawerToggle mToggle;
    private MenuViewModel mMenuViewModel = null;
    private ProductViewModel mProductBuyViewModel  = null;
    private String opcion = "";
    private AppPreferenceManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_home);
        initView();
    }

    public void initView(){
        try{
            pref = new AppPreferenceManager(HomeActivity.this);
            mBinding = DataBindingUtil.setContentView(this, R.layout.activity_menu_home);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mMenuViewModel = ViewModelProviders.of(HomeActivity.this).get(MenuViewModel.class);
                    mProductBuyViewModel = ViewModelProviders.of(HomeActivity.this).get(ProductViewModel.class);
                }
            });
            mProductBuyViewModel.deleteCustmizedAll();
            insertarMangaLarga();
            insertarMangaCorta();
            mMenuViewModel.getMenu().observe(this, this);
            getImageMenu();
            mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
            mBinding.frmCarrito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this, ShoppingCartActivity.class);
                    startActivity(intent);
                }
            });
            initToolbar();
            onClickMenuImage();

        }catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }



    @Override
    protected void onResume() {
        super.onResume();
        getHeaderView(pref.getFullName());
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mBinding.tvCounter.setText(String.valueOf(pref.getProductCount()));
    }

    private void getHeaderView(String nombreCompleto){
        System.out.println(nombreCompleto);
        View headerLayout = mBinding.navigationView.getHeaderView(0);
        TextView textView = headerLayout.findViewById(R.id.NombreUsuario);
        textView.setText(nombreCompleto);
    }

    public void onClickMenuImage(){

        mBinding.gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this, SelectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        mBinding.imgCatalogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MenuCatalogoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    public void getImageMenu(){
       // Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/5.png").into(mBinding.imgPersonliza);
        Picasso.with(this).load(AppUtil.Urls.BASE_URL + "assets/images/mobile-app/4.png").networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(mBinding.imgCatalogo);
    }

    public void initToolbar(){
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_baseline_menu);
        getNavigationDrawer();
    }
    public void getNavigationDrawer(){
        getHeaderView(pref.getFullName());
        mToggle = new ActionBarDrawerToggle(this, mBinding.drawerLayout, mBinding.toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mBinding.drawerLayout.addDrawerListener(mToggle);
        mBinding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (!AppUtil.isOnline(HomeActivity.this)) {
            DialogAlerta.createDialogOK("Alerta", getString(R.string.no_conexion_wifi), HomeActivity.this);
        }else{
            switch (menuItem.getItemId()) {
                case R.id.nav_inicio:
                    mBinding.lnrMenu.setVisibility(View.VISIBLE);
                    break;
                case R.id.nav_pedidos:
                    setMenu(R.id.nav_pedidos);
                    break;
                case R.id.nav_medidas:
                    Intent intent = new Intent(this, MisMedidasActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                case R.id.nav_funciona:
                    setMenu(R.id.nav_funciona);
                    break;
                case R.id.nav_medirme:
                    setMenu(R.id.nav_medirme);
                    break;
                case R.id.nav_cuenta:
                    setMenu(R.id.nav_cuenta);
                    break;
                case R.id.nav_terminos:
                    setMenu(R.id.nav_terminos);
                    break;
                case R.id.nav_contacto:
                    setMenu(R.id.nav_contacto);
                    break;
                case R.id.nav_politicas_privacidad:
                    setMenu(R.id.nav_politicas_privacidad);
                    break;
                case R.id.nav_sesion:
                    logOut();
                    break;
            }
        }
        mBinding.drawerLayout.closeDrawers();
        return true;
    }

    private void setMenu(int option){
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("opcion", option);
        startActivity(intent);
    }

    private void logOut(){
        if(pref.getProfile().equals(ProviderType.FACEBOOK.name())){
            FirebaseAuth.getInstance().signOut();
            LoginManager.getInstance().logOut();
        }
        pref.removeToken();
        pref.removeFullName();
        getHeaderView(pref.getFullName());
        //getHeaderView(BaseApplication.getInstance().getPreferenceManager().getFullName());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed(); }

    @Override
    public void onChanged(List<Menu> menus) {
        //getRecyclerPersonalView(menus);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //mBinding.imgPersonliza.setImageDrawable(null);
        mBinding.imgCatalogo.setImageDrawable(null);
    }

    private void insertarMangaLarga() {
        ProductServer.getInstance().getCustomizerMenu(AppUtil.Constants.ID_MANGA_LARGA, AppUtil.Constants.MANGA_LARGA, new ProductServer.CustomizerMenuCallBack() {
            @Override
            public void onSuccess(List<Customizer> mListCustomizer, String tipoManga) {
                for(int i = 0; i < mListCustomizer.size(); i++) {
                    CustomizedProduct product = new CustomizedProduct();
                    product.setIdProduct(mListCustomizer.get(i).getId());
                    product.setCustomizedName(mListCustomizer.get(i).getEtiqueta());
                    product.setUrlIcon(mListCustomizer.get(i).getUrlIcon());
                    product.setIndexProduct(mListCustomizer.get(i).getIndex());
                    product.setSelected(false);
                    product.setItemSelected("");
                    product.setTipoManga(tipoManga);
                    mProductBuyViewModel.insertCustomizedProduct(product);
                }
            }
        });
    }

    private void insertarMangaCorta() {
        ProductServer.getInstance().getCustomizerMenu(AppUtil.Constants.ID_MANGA_CORTA, AppUtil.Constants.MANGA_CORTA, new ProductServer.CustomizerMenuCallBack() {
            @Override
            public void onSuccess(List<Customizer> mListCustomizer, String tipoManga) {
                for(int i = 0; i < mListCustomizer.size(); i++) {
                    CustomizedProduct product = new CustomizedProduct();
                    product.setIdProduct(mListCustomizer.get(i).getId());
                    product.setCustomizedName(mListCustomizer.get(i).getEtiqueta());
                    product.setUrlIcon(mListCustomizer.get(i).getUrlIcon());
                    product.setIndexProduct(mListCustomizer.get(i).getIndex());
                    product.setSelected(false);
                    product.setItemSelected("");
                    product.setTipoManga(tipoManga);
                    mProductBuyViewModel.insertCustomizedProduct(product);
                }
            }
        });
    }
}